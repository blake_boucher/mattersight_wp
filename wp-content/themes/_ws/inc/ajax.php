<?php
// Post selector post types
function _ws_post_selector_types() {
  if ($_REQUEST['post_type']) {
    $postTypes = array($_REQUEST['post_type'] => get_post_type_object($_REQUEST['post_type']));
  }
  else {
    $postTypes = get_post_types(array('public' => true));
    unset($postTypes['attachment']);
    $postTypes = array_map(function($v) {
      return get_post_type_object($v);
    }, $postTypes);
  }
  if ($_REQUEST['value']) {
    $postType = get_post_type_object(get_post_type($_REQUEST['value']));
  }
  else {
    $postType = reset($postTypes);
  }
  $output = '';
  if ($_REQUEST['multiple']) {
    $output .= '<ul class="chips">';
      if ($vals = $_REQUEST['value']) {
        $vals = explode(',', $vals);
        foreach ($vals as $v) {
          $output .= '<li pid="' . $v . '"><span>' . get_post($v)->post_title . '</span><button><span class="dashicons dashicons-no-alt"></span></button></li>';
        }
      }
    $output .= '</ul>';
  }
  $output .= '<div class="post-selector-container">';
    if (count($postTypes) > 1) {
      $output .= '<div class="post-selector-type">
        <ul>';
          foreach ($postTypes as $type) {
            $output .= '<li><input id="' . $_REQUEST['id'] . '-type-' . $type->name . '" name="' . $_REQUEST['id'] . '-type" type="radio" value="' . $type->name . '" ' . ($postType->name==$type->name ? 'checked' : '') . ' /><label for="' . $_REQUEST['id'] . '-type-' . $type->name . '">' . $type->labels->singular_name . '</label></li>';
          }
        $output .= '</ul>
      </div>';
    }
    $output .= '<div class="post-selector-id">
      <input class="post-selector-filter" type="text" placeholder="Filter..." />
      <ul>';
        $posts = get_posts(array('post_type'=>$postType->name, 'post_status'=>'publish', 'posts_per_page'=>-1));
        foreach ($posts as $post) {
          $output .= '<li>
            <input id="' . $_REQUEST['id'] . '-id-' . $post->ID . '" name="' . $_REQUEST['id'] . '-id" type="radio" value="' . $post->ID . '" ' . ($post->ID==$_REQUEST['value'] ? 'checked' : '') . ' />
            <label for="' . $_REQUEST['id'] . '-id-' . $post->ID . '">' . $post->post_title . '</label>
          </li>';
        }
      $output .= '</ul>
    </div>
  </div>';
  echo $output;
  wp_die();
}
add_action('wp_ajax__ws_post_selector_types', '_ws_post_selector_types');
add_action('wp_ajax_nopriv__ws_post_selector_types', '_ws_post_selector_types');

// Post selector posts
function _ws_post_selector_posts() {
  $posts = get_posts(array('post_type'=>$_REQUEST['post_type'], 'post_status'=>'publish', 'posts_per_page'=>-1));
  $output = '<input class="post-selector-filter" type="text" placeholder="Filter..." /><ul>';
  if ($posts) {
    foreach ($posts as $post) {
      $output .= '<li><input id="' . $_REQUEST['id'] . '-id-' . $post->ID . '" name="' . $_REQUEST['id'] . '-id" type="radio" value="' . $post->ID . '" ' . ($_REQUEST['value']==$post->ID ? 'checked' : '') . ' /><label for="' . $_REQUEST['id'] . '-id-' . $post->ID . '">' . $post->post_title . '</label></li>';
    }
  }
  else {
    $output .= '<li><i>No posts found</i></li>';
  }
  $output .= '</ul>';
  echo $output;
  wp_die();
}
add_action('wp_ajax__ws_post_selector_posts', '_ws_post_selector_posts');
add_action('wp_ajax_nopriv__ws_post_selector_posts', '_ws_post_selector_posts');

// SVG selector
function _ws_svg_selector() {
  $svgs = get_option('svg');
  $output = '<div class="svg-selector-container"><div class="svg-selection"><svg><use xlink:href="/wp-content/themes/_ws/template-parts/sprites.svg#' . ($_REQUEST['value'] ?: '') . '"></use></svg><button><span class="dashicons dashicons-arrow-down"></span></button></div><ul><li><input id="' . $_REQUEST['id'] . '-id-empty" name="' . $_REQUEST['id'] . '-id" type="radio" value="" ' . (!$_REQUEST['value'] ? 'checked' : '') . ' /><label for="' . $_REQUEST['id'] . '-id-empty">No Icon</label></li>';
  if ($svgs) {
    foreach ($svgs as $svg) {
      $output .= '<li><input id="' . $_REQUEST['id'] . '-id-' . $svg['id'] . '" name="' . $_REQUEST['id'] . '-id" type="radio" value="' . $svg['id'] . '" ' . ($_REQUEST['value']==$svg['id'] ? 'checked' : '') . ' /><label for="' . $_REQUEST['id'] . '-id-' . $svg['id'] . '"><svg width="20" height="20" viewBox="' . $svg['viewbox'] . '">' . $svg['path'] . '</svg></label></li>';
    }
  }
  else {
    $output .= '<li><i>No svgs found</i></li></div>';
  }
  $output .= '</ul>';
  echo $output;
  wp_die();
}
add_action('wp_ajax__ws_svg_selector', '_ws_svg_selector');
add_action('wp_ajax_nopriv__ws_svg_selector', '_ws_svg_selector');

// Infinite scroll posts
function _ws_get_more_posts() {
  ob_start();
  $loop = json_decode(stripslashes($_REQUEST['loop']), true);
  $args = $loop['query'];
  $args['paged'] = $_REQUEST['page'];
  $loop = new WP_Query($args);
  $posts = 0;
  if ($loop->have_posts()) : while ($loop->have_posts()) : $loop->the_post();
    if (isset($_REQUEST['type'])) {
      get_template_part('template-parts/archive', $_REQUEST['type']);
    }
    else {
      get_template_part('template-parts/archive', get_post_type());
    }
    $posts++;
  endwhile; endif; wp_reset_postdata();
  $output = ob_get_clean();
  $more = true;
  if ($loop->query['paged'] >= $loop->max_num_pages) {
    $more = false;
  }
  wp_send_json_success(array('output'=>$output, 'more'=>$more));
  wp_die();
}
add_action('wp_ajax__ws_get_more_posts', '_ws_get_more_posts');
add_action('wp_ajax_nopriv__ws_get_more_posts', '_ws_get_more_posts');

// Send Email
function _ws_send_email() {
  $res = false;
  if ($_REQUEST['apiaryProductContainer'] == '' && $_REQUEST['reniatnoCtcudorPyraipa'] == 'Pooh Bear' && $_REQUEST['name'] != '' && $_REQUEST['email'] != '' && $_REQUEST['message'] != '') {
    $to = $_REQUEST['to'];
    $subject = 'New Message from ' . $_REQUEST['name'] . ' via ' . get_site_url();
    $body = '<p>A user has filled out the ' . $_REQUEST['form'] . ' form on ' . site_url() . '.</p>';
    foreach ($_REQUEST as $key => $r) {
      if ($key == 'apiaryProductContainer' || $key == 'reniatnoCtcudorPyraipa' || $key == 'to' || $key == 'form' || $key == 'action') {
        continue;
      }
      else {
        $body .= '<p><b>' . ucwords($key) . ': </b>' . $r . '</p>';
      }
    }
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $res = wp_mail($to, $subject, $body, $headers);
  }
  if ($res) {
    wp_send_json_success('<p>Your email was sent successfully.</p>');
  }
  else {
    wp_send_json_error('<p>There was an error sending your request. Please confirm that all information is valid, and try again.</p>');
  }
  wp_die();
}
add_action('wp_ajax__ws_send_email', '_ws_send_email');
add_action('wp_ajax_nopriv__ws_send_email', '_ws_send_email');

// Generate Sitemap
function _ws_generate_sitemap() {
  if (_ws_sitemap()) {
    wp_send_json_success(date("F j, Y g:i A"));
  }
  else {
    wp_send_json_error('N/A');
  }
  wp_die();
}
add_action('wp_ajax__ws_generate_sitemap', '_ws_generate_sitemap');
add_action('wp_ajax_nopriv__ws_generate_sitemap', '_ws_generate_sitemap');

// Return csv of pages for bulk edit
function _ws_get_bulk() {
  if ($types = $_REQUEST['postTypes']) {
    $postTypes = explode(',', $types);
  }
  else {
    $postTypes = get_post_types(array('public'=>true));
    unset($postTypes['attachment']);
  }
  $ps = get_posts(array(
    'post_type' => $postTypes,
    'post_status' => 'publish',
    'posts_per_page' => -1
  ));
  $csv = "url,id\n";
  foreach ($ps as $p) {
    if (get_post_meta($p->ID, '_banner-external', true)) {
      continue;
    }
    $csv .= get_permalink($p->ID) . "," . $p->ID . "\n";
  }
  wp_send_json_success($csv);
  wp_die();
}
add_action('wp_ajax__ws_get_bulk', '_ws_get_bulk');
add_action('wp_ajax_nopriv__ws_get_bulk', '_ws_get_bulk');

// Update meta data from uploaded bulk edit csv
function _ws_set_bulk() {
  $rows = str_getcsv($_REQUEST['csv'], "\n");
  $cols = str_getcsv($rows[0]);
  for ($i = 1; $i < count($rows); $i++) {
    $row = str_getcsv($rows[$i], ",", '', '');
    for ($ii = 1; $ii < count($row); $ii++) {
      if ($row[$ii]) {
        $val = str_replace("%%%%%", ",", trim($row[$ii]));
        $val = str_replace("~~~~~", '"', $val);
        update_post_meta($row[0], trim($cols[$ii]), $val);
      }
    }
  }
  wp_send_json_success();
  wp_die();
}
add_action('wp_ajax__ws_set_bulk', '_ws_set_bulk');
add_action('wp_ajax_nopriv__ws_set_bulk', '_ws_set_bulk');

// Generate Calculator PDF
function _ws_generatePDF() {
  $key = callPardotApi('https://pi.pardot.com/api/login/version/3',
    array(
      'email' => 'mattersight@walkersands.com',
      'password' => 'Sandwalk7!',
      'user_key' => '0b3b50de18a68014bf94a12982f922a9'
    )
  );
  $prospect = callPardotApi('https://pi.pardot.com/api/prospect/version/3/do/read/email/' . $_REQUEST['email'],
    array(
      'api_key' => simplexml_load_string($key)->api_key,
      'user_key' => '0b3b50de18a68014bf94a12982f922a9',
      'output' => 'simple'
    )
  );
  $p_id = simplexml_load_string($prospect)->prospect->id;
  $prospect2 = callPardotApi('https://pi.pardot.com/api/prospect/version/3/do/upsert/email/' . $_REQUEST['email'],
    array(
      'api_key' => simplexml_load_string($key)->api_key,
      'user_key' => '0b3b50de18a68014bf94a12982f922a9',
      'output' => 'simple',
      'industry' => $_REQUEST['industry'],
      'Number_of_Seats' => $_REQUEST['seatCount'],
      'Benefit_Calculator_Report' => wp_upload_dir()['baseurl'] . '/pdf/' . date('Y') . '/' . $p_id . '.pdf',
      'Latest_Content_Download_URL' => wp_upload_dir()['baseurl'] . '/pdf/' . date('Y') . '/' . $p_id . '.pdf'
    )
  );
  $prospect4 = callPardotApi('https://pi.pardot.com/api/email/version/3/do/send/prospect_email/' . $_REQUEST['email'],
    array(
      'api_key' => simplexml_load_string($key)->api_key,
      'user_key' => '0b3b50de18a68014bf94a12982f922a9',
      'output' => 'simple',
      'campaign_id' => 264,
      'email_template_id' => 38515
    )
  );

  $pdfDir = wp_upload_dir()['basedir'] . '/pdf/';
  if (!file_exists($pdfDir)) {
    mkdir($pdfDir);
  }
  if (!file_exists($pdfDir . date('Y') . '/')) {
    mkdir($pdfDir . date('Y') . '/');
  }

  $logo = wp_upload_dir()['basedir'] . '/2018/04/mattersight_logo.png';
  $icon1 = wp_upload_dir()['basedir'] . '/2018/06/report_icon_1.png';
  $icon2 = wp_upload_dir()['basedir'] . '/2018/06/report_icon_2.png';
  $icon3 = wp_upload_dir()['basedir'] . '/2018/06/report_icon_3.png';
  $dots = wp_upload_dir()['basedir'] . '/2018/06/dots_big.jpg';

  $graph = $_REQUEST['graph'];
  $graph = str_replace('data:image/png;base64,', '', $graph);
  $graph = str_replace(' ', '+', $graph);
  $graph = base64_decode($graph);
  file_put_contents($pdfDir . $p_id . '.png', $graph);

  require(get_template_directory() . '/lib/fpdf181/fpdf.php');
  $pdf = new FPDF();
  // Page width approx 210
  $pdf->AddFont('Open Sans', '', 'opensans.php');
  $pdf->AddFont('Open Sans', 'B', 'opensansb.php');
  $pdf->SetMargins(10, 10, 10);
  $pdf->AddPage();
  $pdf->Image($logo, 10, 10, 40);
  $pdf->SetTextColor(60, 80, 100);
  $pdf->Ln(18);
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, 'Benefit Report for ' . simplexml_load_string($prospect)->prospect->first_name . ':');
  $pdf->Ln();
  $pdf->Ln();
  $pdf->Write(6, 'Based on the information you provided, we calculated a ');
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->Write(6, 'potential cost savings of $' . number_format($_REQUEST['year3']));
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, ' over a three-year period while delivering a best-in-class customer experience. The following chart shows your annual benefit.');

  $pdf->SetFillColor(250, 250, 250);
  $pdf->Rect(0, 65, 120, 60, 'F');
  $pdf->SetY(75);
  $pdf->Write(6, 'Projected Outcomes Based on These Assumptions:');
  $pdf->Ln();
  $pdf->Ln();
  $pdf->Write(6, 'Seat Count');
  $pdf->Ln();
  $pdf->SetFont('Open Sans', 'B', 12);
  $pdf->Write(6, number_format($_REQUEST['seatCount']));
  $pdf->Ln();
  $pdf->Ln();
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, 'Average Handle Time');
  $pdf->Ln();
  $pdf->SetFont('Open Sans', 'B', 12);
  $pdf->Write(6, $_REQUEST['averageHandleTime'] . ' seconds');
  $pdf->SetY(75);
  $pdf->Ln();
  $pdf->Ln();
  $pdf->SetX(70);
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, 'Annual Calls Handled');
  $pdf->Ln();
  $pdf->SetX(70);
  $pdf->SetFont('Open Sans', 'B', 12);
  $pdf->Write(6, number_format($_REQUEST['annualCalls']));
  $pdf->Ln();
  $pdf->Ln();
  $pdf->SetX(70);
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, 'Cost Per Minute');
  $pdf->Ln();
  $pdf->SetX(70);
  $pdf->SetFont('Open Sans', 'B', 12);
  $pdf->Write(6, '$' . number_format($_REQUEST['costPerMinute'], 2));

  $pdf->SetFillColor(240, 240, 240);
  $pdf->Rect(120, 65, 90, 95, 'F');
  $pdf->SetY(75);
  $pdf->SetX(130);
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, 'Cumulative Savings');
  $pdf->Image($pdfDir . $p_id . '.png', 127, 82, 54);
  $pdf->SetY(135);
  $pdf->SetX(130);
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->SetTextColor(215, 65, 35);
  $pdf->Write(6, 'Three-year Total Impact');
  $pdf->Ln();
  $pdf->SetX(130);
  $pdf->SetFont('Open Sans', 'B', 16);
  $pdf->Write(10, '$' . number_format($_REQUEST['year3']));

  $pdf->SetFillColor(60, 80, 100);
  $pdf->Rect(0, 125, 120, 35, 'F');
  $pdf->SetTextColor(255, 255, 255);
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->SetY(135);
  $pdf->Write(6, 'Annual Minutes Saved');
  $pdf->Ln();
  $pdf->SetFont('Open Sans', 'B', 16);
  $pdf->Write(10, number_format($_REQUEST['annualMinutesSaved']));
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->SetY(135);
  $pdf->SetX(70);
  $pdf->Write(6, 'Total Annual Impact');
  $pdf->Ln();
  $pdf->SetX(70);
  $pdf->SetFont('Open Sans', 'B', 16);
  $pdf->Write(10, '$' . number_format($_REQUEST['year1']));

  $pdf->SetFont('Open Sans', 'B', 12);
  $pdf->SetTextColor(215, 65, 35);
  $pdf->SetY(170);
  $pdf->Write(6, 'Personality Plus Analytics Leads to Better Business Outcomes');
  $pdf->Ln();
  $pdf->Ln();
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->SetTextColor(60, 80, 100);
  $pdf->Write(6, 'Mattersight\'s technology empowers businesses to improve customer conversations and contact center performance by pairing customers with agents best suited to their personality. Our proven SaaS-based personality model combined with robust data analytics powered by machine learning and artificial intelligence, have enabled similar enterprise contact centers to:');
  $pdf->Image($icon1, 11, 218, 8);
  $pdf->SetY(230);
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->MultiCell(45, 6, 'Decrease average handle and talk times', 0, 'L');
  $pdf->Image($icon2, 71, 218, 8);
  $pdf->SetY(230);
  $pdf->SetX(70);
  $pdf->MultiCell(45, 6, 'Measurably improve enterprise profitability', 0, 'L');
  $pdf->Image($icon3, 141, 218, 8);
  $pdf->SetY(230);
  $pdf->SetX(140);
  $pdf->MultiCell(45, 6, 'Raise customer satisfaction', 0, 'L');
  $pdf->SetY(260);
  $pdf->SetFont('Open Sans', 'B', 12);
  $pdf->SetTextColor(215, 65, 35);
  $pdf->Image($dots, -4, 262, 220);
  $pdf->Write(6, 'Ready to shift your contact center into high gear?');
  $pdf->Ln();
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, 'Request a demo at ');
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->Write(6, 'mattersight.com/demo', 'https://www.mattersight.com/demo/');
  $pdf->SetFont('Open Sans', '', 10);
  $pdf->Write(6, ' or email ');
  $pdf->SetFont('Open Sans', 'B', 10);
  $pdf->Write(6, 'info@mattersight.com', 'mailto:info@mattersight.com');

  $pdf->Output('F', $pdfDir . date('Y') . '/' . simplexml_load_string($prospect)->prospect->id . '.pdf');

  unlink($pdfDir . $p_id . '.png');
  wp_send_json_success();
  wp_die();
}
add_action('wp_ajax__ws_generatePDF', '_ws_generatePDF');
add_action('wp_ajax_nopriv__ws_generatePDF', '_ws_generatePDF');

function callPardotApi($url, $data, $method = 'GET') {
  $queryString = http_build_query($data, null, '&');
  if (strpos($url, '?') !== false) {
    $url = $url . '&' . $queryString;
  } else {
    $url = $url . '?' . $queryString;
  }
  $curl_handle = curl_init($url);
  curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($curl_handle, CURLOPT_TIMEOUT, 30);
  curl_setopt($curl_handle, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
  curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, 2);
  curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);

  if (strcasecmp($method, 'POST') === 0) {
    curl_setopt($curl_handle, CURLOPT_POST, true);
  } else if (strcasecmp($method, 'GET') !== 0) {
    curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, strtoupper($method));
  }

  $pardotApiResponse = curl_exec($curl_handle);
  if ($pardotApiResponse === false) {
    $humanReadableError = curl_error($curl_handle);
    $httpResponseCode = curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
    curl_close($curl_handle);
    throw new Exception("Unable to successfully complete Pardot API call to $url -- curl error: \"" . "$humanReadableError\", HTTP response code was: $httpResponseCode");
  }
  curl_close($curl_handle);
  return $pardotApiResponse;
}
