<?php
// Pagebuilder modules
$modules = array(
  'clients' => array(
    'title' => 'Client Logos',
    'desc' => 'Grid of client logos',
    'attrs' => array(
      'heading' => '',
      'max' => '8'
    ),
    'frontend' => function($content, $heading, $max) {
      $clients = get_posts(array('post_type'=>'client', 'post_status'=>'publish', 'posts_per_page'=>$max));
      $output = '<div class="container row">';
      $output .= $heading ? '<h2>' . $heading . '</h2>' : '';
      foreach ($clients as $client) {
        $output .= '<div class="col-md-3 col-sm-6"><img class="lazy-load" data-src="' . (get_the_post_thumbnail_url($client->ID, 'medium') ?: get_the_post_thumbnail_url($client->ID, 'full')) . '" alt="' . $client->post_title . '" /></div>';
      }
      $output .= '</div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-6">
          <label for="client-heading-00">Heading</label>
          <input id="client-heading-00" v-model="section.options.heading" type="text" min="1" />
        </div>
        <div class="col-xs-6">
          <label for="client-max-00">Number of Clients <small>-1 means "All"</small></label>
          <input id="client-max-00" v-model="section.options.max" type="number" min="1" />
        </div>
      </li>';
      return $output;
    }
  ),
  'cta' => array(
    'title' => 'Call to Action (Text/Button)',
    'desc' => 'Large centered text with button',
    'attrs' => array(
      'heading' => '',
      'btn_text' => '',
      'btn_link' => ''
    ),
    'frontend' => function($content, $heading, $btn_text, $btn_link) {
      return '<div class="container row">
        <div class="col-xl-6 col-xl-offset-3 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">' .
          ($heading ? '<h2 class="h3">' . $heading . '</h2>' : '') .
          ($content ?: '') .
          ($btn_text && $btn_link ? '<a href="' . $btn_link . '" class="btn">' . $btn_text . '</a>' : '') .
        '</div>
      </div>';
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="cta-heading-00">Heading</label>
          <input id="cta-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="cta-text-00">Text</label>
          <textarea id="cta-text-00" class="text-editor" v-model="section.content"></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-6">
          <label for="cta-btn-text-00">Button Text</label>
          <input id="cta-btn-text-00" type="text" v-model="section.options.btn_text" />
        </div>
        <div class="col-xs-6">
          <label for="cta-btn-link-00">Button URL</label>
          <input id="cta-btn-link-00" type="text" v-model="section.options.btn_link" />
        </div>
      </li>';
      return $output;
    }
  ),
  'cta_form' => array(
    'title' => 'Call to Action (Text/Form)',
    'desc' => 'Text on the left and accompanying form on the right',
    'attrs' => array(
      'heading' => '',
      'form' => ''
    ),
    'frontend' => function($content, $heading, $form) {
      $output = '<div class="container row">
        <div class="col-xl-5 col-lg-6 col-md-6 text">' .
          ($heading ? '<h3>' . $heading . '</h3>' : '') .
          ($content ?: '') .
        '</div>
        <div class="col-xl-5 col-xl-offset-2 col-lg-5 col-lg-offset-1 col-md-6">
          <div class="form-card-container">
            <div class="form-card">
              <div class="form-card-content">' .
                htmlspecialchars_decode($form) .
              '</div>
            </div>
          </div>
        </div>
      </div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="cta-form-heading-00">Heading</label>
          <input id="cta-form-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>
      <li>
        <div class="col-xs-12">
          <label for="cta-form-text-00">Text</label>
          <textarea id="cta-form-text-00" class="text-editor" v-model="section.content"></textarea>
        </div>
      </li>
      <li>
        <div class="col-xs-12">
          <label for="cta-form-00">Form</label>
          <textarea id="cta-form-00" v-model="section.options.form"></textarea>
        </div>
      </li>';
      return $output;
    }
  ),
  'featured_jobs' => array(
    'title' => 'Featured Jobs',
    'desc' => '3 cards featuring open positions',
    'attrs' => array(
      'heading' => '',
      'job_1' => '',
      'job_2' => '',
      'job_3' => ''
    ),
    'frontend' => function($content, $heading, $job_1, $job_2, $job_3) {
      return '<div class="container row">' .
        ($heading ? '<div class="col-xs-12"><h2 class="heading">' . $heading . '</h2></div>' : '') . '
        <ul class="row">
          <li class="col-lg-4 col-md-6 card link-card">
            <div>
              <h3>' . get_the_title($job_1) . '</h3>
              <p>' . get_post_meta($job_1, '_job-field', true) . '</p>
            </div>
            <div>
              <p>' . get_post_meta($job_1, '_job-location', true) . '</p>
              <a href="' . get_post_meta($job_1, '_job-link', true) . '" target="_blank" class="arrow">learn more</a>
            </div>
          </li>
          <li class="col-lg-4 col-md-6 card link-card">
            <div>
              <h3>' . get_the_title($job_2) . '</h3>
              <p>' . get_post_meta($job_2, '_job-field', true) . '</p>
            </div>
            <div>
              <p>' . get_post_meta($job_2, '_job-location', true) . '</p>
              <a href="' . get_post_meta($job_2, '_job-link', true) . '" target="_blank" class="arrow">learn more</a>
            </div>
          </li>
          <li class="col-lg-4 col-md-6 card link-card">
            <div>
              <h3>' . get_the_title($job_3) . '</h3>
              <p>' . get_post_meta($job_3, '_job-field', true) . '</p>
            </div>
            <div>
              <p>' . get_post_meta($job_3, '_job-location', true) . '</p>
              <a href="' . get_post_meta($job_3, '_job-link', true) . '" target="_blank" class="arrow">learn more</a>
            </div>
          </li>
        </ul>
        <a class="btn" href="https://careers-mattersight.icims.com/" target="_blank">view all open positions</a>
      </div>';
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="jobs-heading-00">Heading</label>
          <input id="jobs-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-4">
          <label for="jobs-one-00">Job 1</label>
          <input id="jobs-one-00" class="post-selector" post-type="job" v-model="section.options.job_1">
          </select>
        </div>
        <div class="col-xs-4">
          <label for="jobs-two-00">Job 2</label>
          <input id="jobs-two-00" class="post-selector" post-type="job" v-model="section.options.job_2">
          </select>
        </div>
        <div class="col-xs-4">
          <label for="jobs-three-00">Job 3</label>
          <input id="jobs-three-00" class="post-selector" post-type="job" v-model="section.options.job_3">
          </select>
        </div>
      </li>';
      return $output;
    }
  ),
  'featured_resource' => array(
    'title' => 'Featured Resource',
    'desc' => 'Feature a resource or page in this centered section',
    'attrs' => array(
      'heading' => '',
      'r_id' => ''
    ),
    'frontend' => function($content, $heading, $r_id) {
      return '<div class="container row">' .
        ($heading ? '<h2 class="title">' . $heading . '</h2>' : '') . '
        <div class="col-xl-10 col-xl-offset-1 col-sm-12">
          <div class="card row link-card">
            <div class="col-md-6 col-sm-12" style="height:100%;">
              <div class="featured-img">
                ' . _ws_thumbnail($r_id, 'standard', true) . '
              </div>
            </div>
            <div class="col-md-6 col-sm-12">
              <h3>' . get_the_title($r_id) . '</h3>
              <p>' . _ws_excerpt($r_id) . '</p>
              <a href="' . get_permalink($r_id) . '" class="arrow">learn more</a>
            </div>
          </div>
        </div>
      </div>';
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="featured-heading-00">Heading</label>
          <input id="featured-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="featured-resource-00">Resource</label>
          <input id="featured-resource-00" class="post-selector" type="text" v-model="section.options.r_id" />
        </div>
      </li>';
      return $output;
    }
  ),
  'kpis' => array(
    'title' => 'KPI\'s',
    'desc' => '3 Key Performance Indicators',
    'attrs' => array(
      'heading' => '',
      'svg_1' => '',
      'title_1' => '',
      'text_1' => '',
      'svg_2' => '',
      'title_2' => '',
      'text_2' => '',
      'svg_3' => '',
      'title_3' => '',
      'text_3' => '',
      'btn_text' => '',
      'btn_link' => ''
    ),
    'frontend' => function($content, $heading, $svg_1, $title_1, $text_1, $svg_2, $title_2, $text_2, $svg_3, $title_3, $text_3, $btn_text, $btn_link) {
      $output = '<div class="container row">' .
        ($heading ? '<div class="col-xs-12"><h2>' . $heading . '</h2></div>' : '') .
        '<div class="col-md-4 kpi">' .
          ($svg_1 ? do_shortcode('[svg id="' . $svg_1 . '"]') : '') .
          ($title_1 ? '<h3>' . $title_1 . '</h3>' : '') .
          ($text_1 ? '<p>' . $text_1 . '</p>' : '') .
        '</div>
        <div class="col-md-4 kpi">' .
          ($svg_2 ? do_shortcode('[svg id="' . $svg_2 . '"]') : '') .
          ($title_2 ? '<h3>' . $title_2 . '</h3>' : '') .
          ($text_2 ? '<p>' . $text_2 . '</p>' : '') .
        '</div>
        <div class="col-md-4 kpi">' .
          ($svg_3 ? do_shortcode('[svg id="' . $svg_3 . '"]') : '') .
          ($title_3 ? '<h3>' . $title_3 . '</h3>' : '') .
          ($text_3 ? '<p>' . $text_3 . '</p>' : '') .
        '</div>';
        if ($btn_text && $btn_link) {
          $output .= '<div class="col-xs-12 center">
            <a class="btn" href="' . $btn_link . '">' . $btn_text . '</a>
          </div>';
        }
      $output .= '</div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="kpi-heading-00">Heading</label>
          <input id="kpi-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-4">
          <input id="kpi-one-svg-00" class="svg-selector" v-model="section.options.svg_1" />
          <textarea id="kpi-one-title-00" v-model="section.options.title_1"></textarea>
          <textarea id="kpi-one-text-00" v-model="section.options.text_1"></textarea>
        </div>
        <div class="col-xs-4">
          <input id="kpi-two-svg-00" class="svg-selector" v-model="section.options.svg_2" />
          <textarea id="kpi-two-title-00" v-model="section.options.title_2"></textarea>
          <textarea id="kpi-two-text-00" v-model="section.options.text_2"></textarea>
        </div>
        <div class="col-xs-4">
          <input id="kpi-three-svg-00" class="svg-selector" v-model="section.options.svg_3" />
          <textarea id="kpi-three-title-00" v-model="section.options.title_3"></textarea>
          <textarea id="kpi-three-text-00" v-model="section.options.text_3"></textarea>
        </div>
      </li>';
      return $output;
    }
  ),
  'locations' => array(
    'title' => 'Locations',
    'desc' => 'Cards of locations',
    'attrs' => array(
      'heading' => ''
    ),
    'frontend' => function($content, $heading) {
      $output = '<div class="container row">' .
        ($heading ? '<h2>' . $heading . '</h2>' : '');
        $locs = get_posts(array('post_type'=>'location', 'post_status'=>'publish', 'posts_per_page'=>-1));
        foreach ($locs as $i=>$loc) {
          $output .= '<div class="col-lg-4 col-md-6 location-card">';
            $output .= '<div class="featured-img">' .
              _ws_thumbnail($loc->ID, 'standard', true) .
            '</div>
            <div class="location-info">
              <button class="location-heading toggle" target="#location-' . $i . '" aria-label="Expand Location Information">
                <div class="button-flex">
                  <h3>' . $loc->post_title . '</h3>' .
                  do_shortcode('[svg id="plus"]') .
                '</div>
              </button>
              <div id="location-' . $i . '" class="location-extra">
                <div>' . $loc->post_content . '</div>
              </div>
            </div>
          </div>';
        }
      $output .= '</div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="locations-heading-00">Heading</label>
          <input id="locations-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>';
      return $output;
    }
  ),
  'overview' => array(
    'title' => 'Overview Grid',
    'desc' => 'Grid of pages',
    'attrs' => array(
      'heading' => '',
      'page_ids' => array()
    ),
    'frontend' => function($content, $heading, $page_ids) {
      if (!$page_ids) {
        return '';
      }
      $p_ids = explode(',', $page_ids);
      $output = '<div class="container row">
        <div class="col-xl-8 col-lg-10 no-padding">' .
          ($heading ? '<h2>' . $heading . '</h2>' : '') .
          ($content ? $content : '') .
        '</div>';
        foreach ($p_ids as $p_id) {
          $p_id = trim($p_id);
          $output .= '<div class="col-lg-6 card link-card">
            <div class="card-content">
              <div>
                <h3>' . get_the_title($p_id) . '</h3>
                <p>' . _ws_excerpt($p_id) . '</p>
              </div>
              <a href="' . get_permalink($p_id) . '" class="arrow">learn more</a>
            </div>';
            if ($img = _ws_thumbnail($p_id, 'standard')) {
              $output .= '<div class="featured-img">' . $img . '</div>';
            }
          $output .= '</div>';
        }
      $output .= '</div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="overview-heading-00">Heading</label>
          <input id="overview-heading-00" type="text" v-model="section.options.heading" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="overview-subheading-00">Subheading</label>
          <textarea id="overview-subheading-00" class="text-editor" v-model="section.content"></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="overview-pages-00">Pages</label>
          <input id="overview-pages-00" class="post-selector" multiple type="text" v-model="section.options.page_ids" />
        </div>
      </li>';
      return $output;
    }
  ),
  'overview_list' => array(
    'title' => 'Overview List',
    'desc' => 'Full width list of pages',
    'attrs' => array(
      'heading' => '',
      'p_id' => ''
    ),
    'frontend' => function($content, $heading, $p_id) {
      $ps = get_posts(array('post_type'=>'page', 'post_parent'=>$p_id, 'post_status'=>'publish', 'posts_per_page'=>-1)) ?: array();
      $output = '';
      if ($heading) {
        $output .= '<div class="container row">
          <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 center">
            <h2>' . $heading . '</h2>
          </div>
        </div>';
      }
      $output .= '<div class="container-fluid row">';
      foreach ($ps as $p) {
        $output .= '<div class="col-xs-12">
          <div class="card link-card">
            <div class="col-md-6">
              <div class="featured-img">' . _ws_thumbnail($p->ID, 'large', true) . '</div>
            </div>
            <div class="col-md-6 card-content">
              <h3>' . $p->post_title . '</h3>
              <p>' . $p->post_excerpt . '</p>
              <a href="' . get_permalink($p->ID) . '" class="arrow">learn more</a>
            </div>
          </div>
        </div>';
      }
      $output .= '</div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="overview-list-heading-00">Heading</label>
          <input id="overview-list-heading-00" type="text" v-model="section.options.heading"></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="overview-list-heading-00">Parent</label>
          <input id="overview-list-heading-00" class="post-selector" type="text" v-model="section.options.p_id"></textarea>
        </div>
      </li>';
      return $output;
    }
  ),
  'related' => array(
    'title' => 'Related Content',
    'desc' => 'Feature 3 posts from a post type, related content tag, or simply by page ID',
    'attrs' => array(
      'heading' => 'Related Content',
      'type' => '',
      'post_1' => '',
      'post_2' => '',
      'post_3' => '',
      'post_type' => '',
      'tax' => ''
    ),
    'frontend' => function($content, $heading, $type, $post_1, $post_2, $post_3, $post_type, $tax) {
      $output = '<div class="container row type-' . $type . '">';
        $output .= '<div class="col-xs-12 no-padding">' .
          ($heading ? '<h2>' . $heading . '</h2>' : '') .
          // ($type == 'post-type' ? '<a class="arrow" href="#">view all</a>' : '') .
        '</div>';
        $ps = array();
        if ($type == 'posts') {
          $ps = get_posts(array('post_type' => 'any', 'include' => array($post_1, $post_2, $post_3)));
        }
        else if ($type == 'post-type') {
          $ps = get_posts(array('post_type' => $post_type, 'post_status' => 'publish', 'posts_per_page' => 3));
        }
        else if ($type == 'tax') {
          $ps = get_posts(array('post_type' => 'any', 'post_status' => 'publish', 'posts_per_page' => 3, 'tax_query' => array(
            array(
              'taxonomy' => 'related_content',
              'field' => 'slug',
              'terms' => $tax
            )
          )));
        }
        global $post;
        foreach ($ps as $post) {
          setup_postdata($post);
          ob_start();
          get_template_part('template-parts/archive', get_post_type());
          $output .= ob_get_contents();
          ob_end_clean();
        }
        wp_reset_postdata();
      $output .= '</div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="related-heading-00">Heading</label>
          <input id="related-heading-00" type="text" v-model="section.options.heading">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <input id="related-type-post-00" name="related-type-00" type="radio" value="posts" v-model="section.options.type" />
          <label for="related-type-post-00">Specific Posts</label>
          <div class="row">
            <div class="col-xs-4">
              <input id="related-post-one-00" class="post-selector" type="text" v-model="section.options.post_1" />
            </div>
            <div class="col-xs-4">
              <input id="related-post-two-00" class="post-selector" type="text" v-model="section.options.post_2" />
            </div>
            <div class="col-xs-4">
              <input id="related-post-three-00" class="post-selector" type="text" v-model="section.options.post_3" />
            </div>
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <input id="related-type-post-type-00" name="related-type-00" type="radio" value="post-type" v-model="section.options.type" />
          <label for="related-type-post-type-00">Post Type</label>
          <div>
            <select id="related-post-type-00" v-model="section.options.post_type">';
              $types = get_post_types(array('public'=>true));
              foreach ($types as $type) {
                $t = get_post_type_object($type);
                $output .= '<option value="' . $t->name . '">' . $t->labels->name . '</option>';
              }
            $output .= '</select>
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <input id="related-type-tax-00" name="related-type-00" type="radio" value="tax" v-model="section.options.type" />
          <label for="related-type-tax-00">Taxonomy</label>
          <div>
            <select id="related-tax-00" v-model="section.options.tax">';
              $terms = get_terms('related_content');
              foreach ($terms as $term) {
                $output .= '<option value="' . $term->slug . '">' . $term->name . ' (' . $term->count . ')</option>';
              }
            $output .= '</select>
          </div>
        </div>
      </li>';
      return $output;
    }
  ),
  'testimonial' => array(
    'title' => 'Testimonial',
    'desc' => 'Feature a testimonial',
    'attrs' => array(
      't_id' => ''
    ),
    'frontend' => function($content, $t_id) {
      $output = '<div class="container row">
        <div class="col-xl-8 col-xl-offset-2 col-lg-10 col-lg-offset-1 center">
          <blockquote>
            <p class="h3">' . get_post($t_id)->post_content . '</p>
          </blockquote>
          <div class="attestant">';
            if ($img = _ws_thumbnail($t_id, 'thumbnail', true)) {
              $output .= '<div class="featured-img">' . ($img ?: '') . '</div>';
            }
            $output .= '<div>
              <p class="name">' . (get_post_meta($t_id, '_testimonial-name', true) ?: get_the_title($t_id)) . '</p>';
              if ($title = get_post_meta($t_id, '_testimonial-title', true)) {
                $output .= '<p class="title">' . $title . '</p>';
              }
              if ($linkedin = get_post_meta($t_id, '_testimonial-linkedin', true)) {
                $output .= '<a class="arrow" href="' . $linkedin . '" target="_blank">view linkedin</a>';
              }
            $output .= '</div>
          </div>';
          if ($video = get_post_meta($t_id, '_testimonial-video', true)) {
            $output .= '<button class="lightbox-button">watch video</button>[wistia]' . $video . '[/wistia]';
          }
        $output .= '</div>
      </div>';
      return $output;
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <label for="testimonial-id-00">Testimonial</label>
          <input id="testimonial-id-00" class="post-selector" post-type="testimonial" v-model="section.options.t_id" />
        </div>
      </li>';
      return $output;
    }
  ),
  'text' => array(
    'title' => 'Text',
    'desc' => 'Text section has a shorter width so that text lines are easier to read',
    'attrs' => array(
    ),
    'frontend' => function($content) {
      return '<div class="container row">
        <div class="col-xl-8 col-xl-offset-2 col-lg-10 col-lg-offset-1">' . $content . '</div>
      </div>';
    },
    'backend' => function() {
      $output = '<li class="row">
        <div class="col-xs-12">
          <textarea id="text-00" class="text-editor" v-model="section.content"></textarea>
        </div>
      </li>';
      return $output;
    }
  )
);

foreach ($modules as $name=>$module) {
  add_shortcode($name, 'pb_shortcode');
}
function pb_shortcode($atts, $content = null, $shortcode = '') {
  global $modules;
  $module = $modules[$shortcode];
  $standard_attrs = array(
    'id' => '',
    'class' => '',
    'bg_color' => '',
    'bg_img' => '',
    'bg_pos' => ''
  );
  $attrs = array_merge($standard_attrs, $module['attrs']);
  $a = shortcode_atts($attrs, $atts);
  $output = '';
  $output .= '<section ' . ($a['id'] ? 'id="' . $a['id'] . '" ' : '') . 'class="pb-item pb-' . $shortcode . ($a['class'] ? ' ' . $a['class'] : '') . '" ' . 'style="' . ($a['bg_color'] ? 'background-color:' . $a['bg_color'] . ';' : '') . ($a['bg_img'] ? 'background-image:url(' . $a['bg_img'] . ');' : '') . ($a['bg_pos'] ? 'background-position: ' . $a['bg_pos'] . ';' : '') . '">';

  $custom_attrs = array_intersect_key($a, $module['attrs']);
  array_unshift($custom_attrs, $content);
  $output .= do_shortcode(call_user_func_array($module['frontend'], $custom_attrs));

  $output .= '</section>';
  return $output;
}

// Build pagebuilder
function _ws_pagebuilder($post) {
  if (get_post_meta($post->ID, '_wp_page_template', true) != 'templates/pagebuilder.php') {
    return;
  }
  wp_nonce_field(basename(__FILE__), 'pagebuilder-nonce'); ?>
  <div id="page-builder" v-show="pb">
    <h1>Page Builder</h1>
    <button id="add-section-top" class="button" @click.prevent="showSectionsFunc('top')"><span class="dashicons dashicons-plus"></span>Add Section</button>
    <div v-if="showSections" class="lightbox">
      <div class="lightbox-bg" @click="showSections = false"></div>
      <div class="lightbox-header sortable-header">
        <h4 class="sortable-title">Add Section</h4>
        <span class="dashicons dashicons-no-alt lightbox-close" @click="showSections = false"></span>
      </div>
      <div class="lightbox-content">
        <ul class="section-selections">
          <li v-for="(s, k, i) in sections">
            <button class="button" @click.prevent="addSection(k)">
              <h4>{{s.title}}</h4>
              <p>{{s.desc}}</p>
            </button>
          </li>
        </ul>
      </div>
    </div>
    <!-- <button id="load-section" class="button"><span class="dashicons dashicons-category"></span>Load Section</button> -->
    <ul id="sections" class="sortable-container">
      <li v-for="(section, i) in pbData" :key="section.u_id" :class="'sortable-item section postbox pb-' + section.slug">
        <div class="sortable-header">
          <h4 class="sortable-title">{{sections ? sections[section.slug].title : ''}}</h4>
          <span class="dashicons dashicons-move sortable-handle"></span>
          <span class="dashicons dashicons-admin-generic section-options-btn" @click="showOptions(section)"></span>
          <span class="dashicons dashicons-trash sortable-delete" @click.stop="deleteSection(i)"></span>
        </div>
        <div v-show="section.showOptions" class="lightbox">
          <div class="lightbox-bg" @click="showOptions(section) = false"></div>
          <div class="lightbox-header sortable-header">
            <h4 class="sortable-title">{{sections ? sections[section.slug].title : ''}} Options</h4>
            <!-- <svg width="20" height="20" viewBox="0 0 24 24">
              <path d="M15.003 3h2.997v5h-2.997v-5zm8.997 1v20h-24v-24h20l4 4zm-19 5h14v-7h-14v7zm16 4h-18v9h18v-9z"></path>
            </svg> -->
            <span class="dashicons dashicons-no-alt lightbox-close" @click="section.showOptions = false"></span>
          </div>
          <div class="lightbox-content">
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <label :for="'custom-id-' + i">ID</label>
                  <input :id="'custom-id-' + i" type="text" v-model="section.options.id" />
                </div>
                <div class="col-xs-6">
                  <label :for="'custom-class-' + i">Class</label>
                  <input :id="'custom-class-' + i" type="text" v-model="section.options.class" />
                </div>
              </li>
              <li>
                <div class="col-xs-12">
                  <label :for="'background-color-' + i">Background Color</label>
                  <input :id="'background-color-' + i" class="color-picker" data-alpha="true" type="text" v-model="section.options.bg_color" />
                </div>
              </li>
              <li>
                <div class="col-xs-12">
                  <label :for="'background-img-' + i">Background Image</label>
                  <div class="row">
                    <button class="button media-selector" :target="'#background-img-' + i">Select Image</button>
                    <input :id="'background-img-' + i" class="flex-1" type="text" v-model="section.options.bg_img" />
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="sortable-content section-inside">
          <ul>
            <?php
            global $modules;
            foreach ($modules as $key=>$module) : ?>
              <template v-if="section.slug == '<?= $key; ?>'">
                <?= call_user_func_array($module['backend'], array()); ?>
              </template>
            <?php
            endforeach; ?>
          </ul>
        </div>
      </li>
    </ul>
    <button id="add-section-bottom" class="button" @click.prevent="showSectionsFunc('bottom')"><span class="dashicons dashicons-plus"></span>Add Section</button>
  </div>
  <?php
}
add_action('edit_form_after_editor', '_ws_pagebuilder');

// Ajax get sections
function _ws_get_sections() {
  global $modules;
  echo json_encode($modules);
  wp_die();
}
add_action('wp_ajax__ws_get_sections', '_ws_get_sections');
