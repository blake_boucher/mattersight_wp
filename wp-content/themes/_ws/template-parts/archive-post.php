<div id="post-<?= get_the_ID(); ?>" <?php post_class('col-lg-4 col-md-6 card gallery-card'); ?>>
  <?= has_post_thumbnail() ? '<div class="featured-img">' . _ws_thumbnail(get_the_ID(), 'standard', true) . '</div>' : ''; ?>
  <div class="info">
    <div>
      <p class="type">Blog Post</p>
      <h4><?= get_the_title(); ?></h4>
      <p><?= get_the_date() . ' - ' . get_the_author(); ?></p>
      <!-- <p><?= _ws_excerpt(); ?></p> -->
    </div>
    <div>
      <a class="arrow" href="<?= get_permalink(); ?>">learn more</a>
    </div>
  </div>
</div>
