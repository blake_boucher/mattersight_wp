<article id="post-<?php the_ID(); ?>" <?php post_class('post-single'); ?>>
  <?php get_template_part('template-parts/banner'); ?>
  <section class="single-post-content">
    <div class="container row">
      <div class="col-lg-8 wp-text">
        <?php the_content(); ?>
        <?= do_shortcode('[share_buttons buttons="linkedin,twitter,email"]'); ?>
      </div>
      <aside class="col-lg-3 col-lg-offset-1">
        <?php
        $terms = array_map(function($v) {
          return $v->term_id;
        }, get_the_terms(get_the_ID(), 'category'));
        $rs = get_posts(array('post_type'=>'post', 'exclude'=>get_the_ID(), 'post_status'=>'publish', 'posts_per_page'=>2, 'tax_query'=>array(
          array(
            'taxonomy' => 'category',
            'field' => 'term_id',
            'terms' => $terms
          )
        )));
        if ($rs) : ?>
          <h3>Related Blog Posts</h3>
          <?php
          foreach ($rs as $r) : ?>
            <div class="related-post">
              <p><?= $r->post_title; ?></p>
              <a class="arrow" href="<?= get_permalink($r->ID); ?>">read more</a>
            </div>
          <?php
          endforeach;
        endif; ?>
      </aside>
    </div>
  </section>
</article>
