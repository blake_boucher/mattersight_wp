<div id="post-<?= get_the_ID(); ?>" <?php post_class('col-lg-4 col-md-6 card gallery-card'); ?>>
  <?= has_post_thumbnail() ? '<div class="featured-img">' . _ws_thumbnail(get_the_ID(), 'standard', true) . '</div>' : ''; ?>
  <div class="info">
    <div>
      <p class="type"><?= get_post_type_object(get_post_type(get_the_ID()))->labels->singular_name; ?></p>
      <h4><?= get_the_title(); ?></h4>
      <p><?= _ws_excerpt(); ?></p>
    </div>
    <div>
      <?php
      if ($ext = get_post_meta(get_the_ID(), '_banner-external', true)) {
        echo do_shortcode('<a class="external" href="' . $ext . '" target="_blank"><span>learn more</span>[svg id="external"]</a>');
      }
      else {
        echo '<a class="arrow" href="' . get_permalink() . '">learn more</a>';
      } ?>
    </div>
  </div>
</div>
