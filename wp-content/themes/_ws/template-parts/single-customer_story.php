<article id="post-<?php the_ID(); ?>" <?php post_class('post-single'); ?>>
  <header class="page-header">
    <div class="container row">
      <div class="col-lg-8">
        <?php
        $tag = get_post_meta(get_the_ID(), '_banner-headline-type', true) ?: 'h1';
        $parent = url_to_postid(preg_replace('/\/[^\/]*\/$/', '/', $_SERVER['REQUEST_URI']));
        echo '<a class="breadcrumb" href="' . get_permalink($parent) . '">' . get_the_title($parent) . '</a>';
        if ($logo = get_post_meta(get_the_ID(), '_cs-logo', true)) {
          echo '<img class="cs-logo" src="' . $logo . '" alt="Client Logo" />';
        }
        echo '<' . $tag . ' class="page-title">' . (get_post_meta(get_the_ID(), '_banner-headline', true) ?: get_the_title()) . '</' . $tag . '>';
        if ($subheadline = get_post_meta(get_the_ID(), '_banner-subheadline', true)) {
          if (substr($subheadline, 0, 1) == '<') {
            echo '<div class="page-subtitle">' . $subheadline . '</div>';
          }
          else {
            echo '<p class="page-subtitle">' . $subheadline . '</p>';
          }
        } ?>
      </div>
    </div>
  </header>
  <section class="no-padding">
    <div class="container row">
      <div class="col-lg-9 cs-banner-container">
        <div class="cs-banner" style="<?= _ws_thumbnail_background(get_the_ID(), 'large'); ?>">
          <?php
          if ($v = get_post_meta(get_the_ID(), '_banner-video', true)) {
            echo do_shortcode('<button class="lightbox-button" aria-label="Play Video">[svg id="play"]</button>[wistia]' . $v . '[/wistia]');
          } ?>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="row cs-kpis">
          <?php
          $kpi_1 = get_post_meta(get_the_ID(), '_cs-kpi-1', true);
          $kpi_2 = get_post_meta(get_the_ID(), '_cs-kpi-2', true);
          $kpi_3 = get_post_meta(get_the_ID(), '_cs-kpi-3', true); ?>
          <div class="col-lg-12 col-md-4">
            <?= do_shortcode('[svg id="' . $kpi_1['svg'] . '"]'); ?>
            <h3><?= $kpi_1['title']; ?></h3>
            <p><?= $kpi_1['text']; ?></p>
          </div>
          <div class="col-lg-12 col-md-4">
            <?= do_shortcode('[svg id="' . $kpi_2['svg'] . '"]'); ?>
            <h3><?= $kpi_2['title']; ?></h3>
            <p><?= $kpi_2['text']; ?></p>
          </div>
          <div class="col-lg-12 col-md-4">
            <?= do_shortcode('[svg id="' . $kpi_3['svg'] . '"]'); ?>
            <h3><?= $kpi_3['title']; ?></h3>
            <p><?= $kpi_3['text']; ?></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container row">
      <div class="col-md-3 cs-info">
        <?php
        if ($client = get_post_meta(get_the_ID(), '_cs-client-name', true)) : ?>
          <p class="label">client</p>
          <p><?= strtolower($client); ?></p>
        <?php
        endif;
        if ($type = get_the_terms(get_the_ID(), 'type')) : ?>
          <p class="label">type</p>
          <p><?= strtolower($type[0]->name); ?></p>
        <?php
        endif;
        if ($industry = get_the_terms(get_the_ID(), 'industry')) : ?>
          <p class="label">industry</p>
          <p><?= strtolower($industry[0]->name); ?></p>
        <?php
        endif;
        if ($products = get_post_meta(get_the_ID(), '_cs-related-products', true)) : ?>
          <p class="label">product</p>
          <ul>
            <?php
            $products = explode(',', $products);
            foreach ($products as $product) : ?>
              <li>
                <a href="<?= get_permalink($product); ?>" class="arrow"><?= strtolower(get_the_title($product)); ?></a>
              </li>
            <?php
            endforeach; ?>
          </ul>
        <?php
        endif; ?>
      </div>
      <div class="col-md-8">
        <?php the_content(); ?>
      </div>
    </div>
  </section>
  <?php
  if ($t_id = get_post_meta(get_the_ID(), '_cs-testimonial', true)) {
    echo do_shortcode('[testimonial t_id="' . $t_id . '"]');
  }
  if ($text = get_post_meta(get_the_ID(), '_cs-bottom-text', true)) : ?>
    <section>
      <div class="container row">
        <div class="col-md-3">
        </div>
        <div class="col-md-8">
          <?= $text; ?>
        </div>
      </div>
    </section>
  <?php
  endif;
  $related_heading = get_post_meta(get_the_ID(), '_cs-related-heading', true);
  $related_type = get_post_meta(get_the_ID(), '_cs-related-type', true);
  $related_p1 = get_post_meta(get_the_ID(), '_cs-related-post-one', true);
  $related_p2 = get_post_meta(get_the_ID(), '_cs-related-post-two', true);
  $related_p3 = get_post_meta(get_the_ID(), '_cs-related-post-three', true);
  $related_post_type = get_post_meta(get_the_ID(), '_cs-related-post-type', true);
  $related_tax = get_post_meta(get_the_ID(), '_cs-related-tax', true);
  echo do_shortcode('[related heading="' . $related_heading . '" type="' . $related_type . '" post_1="' . $related_p1 . '" post_2="' . $related_p2 . '" post_3="' . $related_p3 . '" post_type="' . $related_post_type . '" tax="' . $related_tax . '"]');
  $cta_heading = get_post_meta(get_the_ID(), '_cs-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_cs-cta-text', true);
  $cta_btn_text = get_post_meta(get_the_ID(), '_cs-cta-btn-text', true);
  $cta_btn_link = get_post_meta(get_the_ID(), '_cs-cta-btn-link', true);
  $cta_bg_img = get_post_meta(get_the_ID(), '_cs-cta-bg-img', true);
  echo do_shortcode('[cta heading="' . $cta_heading . '" btn_text="' . $cta_btn_text . '" btn_link="' . $cta_btn_link . '" ' . ($cta_bg_img ? '' : 'class="white-btn"') . ' bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta]'); ?>
</article>
