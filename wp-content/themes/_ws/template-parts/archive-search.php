<article id="post-<?php the_ID(); ?>" <?php post_class('result col-xl-8 col-lg-10'); ?>>
  <?php
  $external = get_post_meta(get_the_ID(), '_banner-external', true);
  if ($external) {
    echo '<h2 class="result-title"><a href="' . $external . '" target="_blank">' . get_the_title() . '</a></h2>';
  }
  else {
    echo '<h2 class="result-title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
  } ?>
  <p><?= _ws_excerpt(get_the_ID(), 155); ?></p>
</article>
