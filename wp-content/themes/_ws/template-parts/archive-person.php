<div id="post-<?= get_the_ID(); ?>" <?php post_class('col-lg-4 col-sm-6 card gallery-card'); ?>>
  <img class="lazy-load" src="<?= get_the_post_thumbnail_url(get_the_ID(), 'standard'); ?>" alt="<?= get_the_title(); ?>" />
  <div class="info">
    <div>
      <h4><?= get_the_title(); ?></h4>
      <p><?= get_post_meta(get_the_ID(), '_person-role', true); ?></p>
      <!-- <p><?= _ws_excerpt(); ?></p> -->
    </div>
    <div>
      <a class="arrow" href="<?= get_permalink(); ?>">view bio</a>
    </div>
  </div>
</div>
