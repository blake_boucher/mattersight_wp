<article id="post-<?php the_ID(); ?>" <?php post_class(get_post_type() . '-single'); ?>>
  <header class="page-header">
    <div class="container row">
      <div class="col-lg-8 col-md-12">
        <?php
        // Breadcrumbs
        if (is_single()) {
          $parent = url_to_postid(preg_replace('/\/[^\/]*\/$/', '/', get_permalink()));
          echo '<a class="breadcrumb" href="' . get_permalink($parent) . '">' . get_the_title($parent) . '</a>';
        }
        // Headline
        $tag = get_post_meta(get_the_ID(), '_banner-headline-type', true) ?: 'h1';
        echo '<' . $tag . ' class="page-title">' . (get_post_meta(get_the_ID(), '_banner-headline', true) ?: get_the_title()) . '</' . $tag . '>';
        if ($subheadline = get_post_meta(get_the_ID(), '_person-role', true)) {
          if (substr($subheadline, 0, 1) == '<') {
            echo '<div class="page-subtitle">' . $subheadline . '</div>';
          }
          else {
            echo '<p class="page-subtitle">' . $subheadline . '</p>';
          }
        } ?>
      </div>
    </div>
  </header>
  <section class="single-post-content">
    <div class="container row">
      <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6">
        <?php
        if ($img = get_the_post_thumbnail_url(get_the_ID(), 'standard')) {
          echo '<img class="profile-pic" src="' . $img . '" alt="' . get_the_title() . '" />';
        } ?>
        <!-- <h3 class="role"><?=get_post_meta(get_the_ID(), '_person-role', true); ?></h3> -->
      </div>
      <div class="col-xl-8 col-xl-offset-1 col-lg-10 col-md-8">
        <?php the_content(); ?>
      </div>
    </div>
  </section>
</article>
