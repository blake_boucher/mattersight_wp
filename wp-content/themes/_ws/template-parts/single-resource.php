<article id="post-<?php the_ID(); ?>" <?php post_class('post-single'); ?>>
  <?php
  get_template_part('template-parts/banner');
  if ($card = get_post_meta(get_the_ID(), '_resource-card', true)) : ?>
    <section class="single-post-content">
      <div class="container row">
        <div class="col-xl-7 col-lg-7 wp-text">
          <?php the_content(); ?>
          <?= do_shortcode('[share_buttons buttons="linkedin,twitter,email"]'); ?>
        </div>
        <div class="col-xl-4 col-xl-offset-1 col-lg-5 col-md-7 col-sm-9">
          <div class="form-card-container">
            <div class="form-card">
              <?php
              if ($img = get_post_meta(get_the_ID(), '_resource-img', true)) : ?>
                <div class="resource-img">
                  <img src="<?= $img; ?>" alt="<?= get_the_title(); ?>" />
                </div>
              <?php
              endif; ?>
              <div class="form-card-content">
                <?= $card; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php
  else: ?>
    <section class="single-post-content">
      <div class="container row">
        <div class="col-xl-8 col-lg-10 wp-text">
          <?php the_content(); ?>
          <?= do_shortcode('[share_buttons buttons="linkedin,twitter,email"]'); ?>
        </div>
      </div>
    </section>
  <?php
  endif; ?>
</article>
