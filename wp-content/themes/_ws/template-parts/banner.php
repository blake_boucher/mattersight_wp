<header class="page-header">
  <?php
  if (has_post_thumbnail() && !is_404() && !is_search()) : ?>
    <div class="banner-img" <?= get_query_var('amp') ? '' : 'style="' . _ws_thumbnail_background() . '"'; ?>>
      <?php
      if ($v = get_post_meta(get_the_ID(), '_banner-video', true)) {
        echo do_shortcode('<a class="lightbox-link" href="' . $v . '" aria-label="Play Video">[svg id="play"]</a>');
      } ?>
    </div>
  <?php
  else : ?>
    <div class="banner-empty"></div>
  <?php
  endif; ?>
  <div class="container row">
    <div class="col-lg-8 col-md-12">
      <div class="header-card">
        <?php
        // Breadcrumbs
        if (is_single()) {
          $parent = url_to_postid(preg_replace('/\/[^\/]*\/$/', '/', get_permalink()));
          echo '<a class="breadcrumb" href="' . get_permalink($parent) . '">' . get_the_title($parent) . '</a>';
        }
        // Headline
        $tag = get_post_meta(get_the_ID(), '_banner-headline-type', true) ?: 'h1';
        if ($headline = get_post_meta(get_the_ID(), '_banner-headline', true)) {
          echo '<' . $tag . ' class="page-title">' . $headline . '</' . $tag . '>';
        }
        else {
          if (is_home()) {
            echo '<' . $tag . ' class="page-title">' . get_the_title(get_option('page_for_posts', true)) . '</' . $tag . '>';
          }
          else if (is_search()) {
            if (have_posts()) {
              echo '<h1 class="page-title">Search Results: ' . get_search_query() . '</h1>';
            }
            else {
              echo '<h1 class="page-title">Nothing Found</h1>';
            }
          }
          else if (is_404()) {
            echo '<h1 class="page-title">Page Not Found</h1>';
          }
          else if (is_archive()) {
            if (is_tax()) {
              echo '<h1 class="page-title">' . single_term_title('', false) . '</h1>';
            }
            else {
              echo '<h1 class="page-title">' . get_post_type_object($post->post_type)->labels->name . '</h1>';
            }
          }
          else {
            echo '<' . $tag . ' class="page-title">' . get_the_title() . '</' . $tag . '>';
          }
        }
        if (is_singular('post')) {
          echo '<p class="date-author">' . get_the_date() . ' - ' . get_the_author() . '</p>';
        }
        // Subheadline
        if ($subheadline = get_post_meta(get_the_ID(), '_banner-subheadline', true)) {
          if (substr($subheadline, 0, 1) == '<') {
            echo '<div class="page-subtitle">' . $subheadline . '</div>';
          }
          else {
            echo '<p class="page-subtitle">' . $subheadline . '</p>';
          }
        } ?>
      </div>
    </div>
  </div>
</header>
