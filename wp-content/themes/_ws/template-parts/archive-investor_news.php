<div id="post-<?= get_the_ID(); ?>" <?php post_class('col-xs-12'); ?>>
  <div class="event-news">
    <div class="date">
      <p><?= get_the_date(); ?></p>
    </div>
    <div class="info">
      <p><?= get_the_title(); ?></p>
      <a class="arrow" href="<?= get_permalink(); ?>">read now</a>
    </div>
  </div>
</div>
