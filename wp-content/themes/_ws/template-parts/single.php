<article id="post-<?php the_ID(); ?>" <?php post_class('post-single'); ?>>
  <?php get_template_part('template-parts/banner'); ?>
  <section class="single-post-content">
    <div class="container row">
      <div class="col-lg-8 wp-text">
        <?php the_content(); ?>
      </div>
    </div>
  </section>
</article>
