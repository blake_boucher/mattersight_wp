//=include vue/dist/vue.js

// Careers Page
jQuery('#add-careers-personality-item').on('click', function(e) {
  e.preventDefault();
  jQuery(this).prev().append('<li class="sortable-item">\
    <div class="sortable-header">\
      <span class="dashicons dashicons-move sortable-handle"></span>\
      <span class="dashicons dashicons-trash sortable-delete"></span>\
    </div>\
    <ul class="sortable-content">\
      <li>\
        <label for="careers-personality-items-00-svg">Items</label>\
        <input id="careers-personality-items-00-svg" name="careers-personality-items[00][svg]" class="svg-selector" type="text" />\
      </li>\
      <li>\
        <label for="careers-personality-items-00-heading">Heading</label>\
        <input id="careers-personality-items-00-heading" name="careers-personality-items[00][heading]" type="text" />\
      </li>\
      <li>\
        <label for="careers-personality-items-00-text">Text</label>\
        <textarea id="careers-personality-items-00-text" name="careers-personality-items[00][text]"></textarea>\
      </li>\
    </ul>\
  </li>');
  checkOrder();
});

// Investors Overview
jQuery('#add-io-file').on('click', function(e) {
  e.preventDefault();
  jQuery(this).prev().append('<li class="sortable-item">\
    <div class="sortable-header">\
      <span class="dashicons dashicons-move sortable-handle"></span>\
      <span class="dashicons dashicons-trash sortable-delete"></span>\
    </div>\
    <ul class="sortable-content">\
      <li class="row">\
        <div class="col-xs-6">\
          <label for="io-files-00-text">Text</label>\
          <input id="io-files-00-text" name="io-files[00][text]" type="text" />\
        </div>\
        <div class="col-xs-6">\
          <label for="io-files-00-link">Link</label>\
          <div class="row">\
            <button class="button media-selector" target="#io-files-00-link">Select Image</button>\
            <input id="io-files-00-link" class="flex-1" name="io-files[00][link]" type="text" />\
          </div>\
        </div>\
      </li>\
    </ul>\
  </li>');
  checkOrder();
});

// Product Page
jQuery('#add-product-suite').on('click', function(e) {
  e.preventDefault();
  jQuery(this).prev().append('<li class="sortable-item">\
    <div class="sortable-header">\
      <span class="dashicons dashicons-move sortable-handle"></span>\
      <span class="dashicons dashicons-trash sortable-delete"></span>\
    </div>\
    <ul class="sortable-content">\
      <li class="row">\
        <div class="col-xs-6">\
          <ul>\
            <li>\
              <label for="product-suites-00-heading">Heading</label>\
              <input id="product-suites-00-heading" name="product-suites[00][heading]" type="text" />\
            </li>\
            <li>\
              <label for="product-suites-00-text">Text</label>\
              <textarea id="product-suites-00-text" name="product-suites[00][text]"></textarea>\
            </li>\
          </ul>\
        </div>\
        <div class="col-xs-6">\
          <ul>\
            <li>\
              <label for="product-suites-00-img">Image</label>\
              <div class="row">\
                <button class="button media-selector" target="#product-suites-00-img">Select Image</button>\
                <input id="product-suites-00-img" class="flex-1" name="product-suites[00][img]" type="text" />\
              </div>\
            </li>\
          </ul>\
        </div>\
      </li>\
      <li class="row">\
        <div class="col-xs-4">\
          <ul>\
            <li>\
              <label for="product-suites-00-kpi-one-svg">SVG</label>\
              <input id="product-suites-00-kpi-one-svg" class="svg-selector" name="product-suites[00][kpi-one-svg]" type="text" />\
            </li>\
            <li>\
              <label for="product-suites-00-kpi-one-heading">Heading</label>\
              <input id="product-suites-00-kpi-one-heading" name="product-suites[00][kpi-one-heading]" type="text" />\
            </li>\
            <li>\
              <label for="product-suites-00-kpi-one-text">Text</label>\
              <textarea id="product-suites-00-kpi-one-text" name="product-suites[00][kpi-one-text]"></textarea>\
            </li>\
          </ul>\
        </div>\
        <div class="col-xs-4">\
          <ul>\
            <li>\
              <label for="product-suites-00-kpi-two-svg">SVG</label>\
              <input id="product-suites-00-kpi-two-svg" class="svg-selector" name="product-suites[00][kpi-two-svg]" />\
            </li>\
            <li>\
              <label for="product-suites-00-kpi-two-heading">Heading</label>\
              <input id="product-suites-00-kpi-two-heading" name="product-suites[00][kpi-two-heading]" type="text" />\
            </li>\
            <li>\
              <label for="product-suites-00-kpi-two-text">Text</label>\
              <textarea id="product-suites-00-kpi-two-text" name="product-suites[00][kpi-two-text]"></textarea>\
            </li>\
          </ul>\
        </div>\
        <div class="col-xs-4">\
          <ul>\
            <li>\
              <label for="product-suites-00-kpi-three-svg">SVG</label>\
              <input id="product-suites-00-kpi-three-svg" class="svg-selector" name="product-suites[00][kpi-three-svg]" />\
            </li>\
            <li>\
              <label for="product-suites-00-kpi-three-heading">Heading</label>\
              <input id="product-suites-00-kpi-three-heading" name="product-suites[00][kpi-three-heading]" type="text" />\
            </li>\
            <li>\
              <label for="product-suites-00-kpi-three-text">Text</label>\
              <textarea id="product-suites-00-kpi-three-text" name="product-suites[00][kpi-three-text]"></textarea>\
            </li>\
          </ul>\
        </div>\
      </li>\
    </ul>\
  </li>');
  checkOrder();
});

// Calculator
jQuery('#calculator-meta-inside #add-step').on('click', function(e) {
  e.preventDefault();
  jQuery(this).prev().append('<li class="sortable-item">\
    <div class="sortable-header">\
      <span class="dashicons dashicons-move sortable-handle"></span>\
      <span class="dashicons dashicons-trash sortable-delete"></span>\
    </div>\
    <ul class="sortable-content">\
      <li>\
        <label for="calculator-steps-00-slug">Slug</label>\
        <input id="calculator-steps-00-slug" name="calculator-steps[00][slug]" type="text" value="" />\
      </li>\
      <li>\
        <label for="calculator-steps-00-content">Slug</label>\
        <textarea id="calculator-steps-00-content" name="calculator-steps[00][content]" class="text-editor"></textarea>\
      </li>\
    </ul>\
  </li>');
  checkOrder();
});

// SVG
function checkSVG() {
  var svgs = document.querySelectorAll('.svg-options .sortable-item');
  for (var i = 0; i < svgs.length; i++) {
    if (svgs[i].querySelector('[v-model]')) {
      new Vue({
        el: svgs[i],
        data: {
          viewbox: svgs[i].querySelector('[v-model=viewbox]') ? svgs[i].querySelector('[v-model=viewbox]').value : '',
          paths: svgs[i].querySelector('[v-model=paths]') ? svgs[i].querySelector('[v-model=paths]').value : '',
          valid: true
        },
        computed: {
          validViewbox: function() {
            var match = this.viewbox.match(/(-?\d+(\.\d+)?) (-?\d+(\.\d+)?) (-?\d+(\.\d+)?) (-?\d+(\.\d+)?)/g);
            if (match && match[0] == this.viewbox) {
              this.valid = true;
              return this.viewbox;
            }
            else if (this.viewbox == '') {
              this.valid = true;
            }
            else {
              this.valid = false;
            }
          }
        }
      });
    }
  }
}
checkSVG();
var svgString = '<li class="sortable-item svg">\
  <div class="sortable-header">\
    <span class="dashicons dashicons-move sortable-handle"></span>\
    <span class="dashicons dashicons-trash sortable-delete"></span>\
  </div>\
  <div class="sortable-content row">\
    <div class="col-xs-6">\
      <svg v-if="validViewbox" v-bind:view-box.camel="validViewbox" v-html="paths"></svg>\
    </div>\
    <div class="col-xs-6">\
      <label for="svg[00][id]">ID</label>\
      <input id="svg[00][id]" name="svg[00][id]" type="text" value="" />\
      <label for="svg[00][title]">Title</label>\
      <input id="svg[00][title]" name="svg[00][title]" type="text" value="" />\
      <label for="svg[00][viewbox]">ViewBox</label>\
      <input id="svg[00][viewbox]" v-bind:class="{invalid: !valid}" name="svg[00][viewbox]" type="text" v-model="viewbox" value="" />\
    </div>\
    <div class="col-xs-12">\
      <label for="svg[00][path]">Paths</label>\
      <textarea id="svg[00][path]" name="svg[00][path]" v-model="paths"></textarea>\
    </div>\
  </div>\
</li>';
jQuery('#add-svg-top').on('click', function(e) {
  e.preventDefault();
  jQuery(this).next().prepend(svgString);
  checkOrder();
  checkSVG();
});
jQuery('#add-svg-bottom').on('click', function(e) {
  e.preventDefault();
  jQuery(this).prev().append(svgString);
  checkOrder();
  checkSVG();
});
jQuery('#svg-import').on('change', function(e) {
  var reader = new FileReader();
  reader.onload = function() {
    try {
      var result = reader.result.replace(/[\n\t\r]+/g, '');
      var xml = jQuery.parseXML(result);
      var svgs = xml.querySelectorAll('symbol') || xml.querySelectorAll('svg');
      for (var i = svgs.length-1; i >= 0; i--) {
        var id = svgs[i].id || '';
        var title = svgs[i].querySelector('title').innerHTML || '';
        var viewBox = svgs[i].getAttribute('viewBox') || '';
        var paths = svgs[i].querySelectorAll('*:not(title)') || '';
        var output = '';
        for (var ii = 0; ii < paths.length; ii++) {
          output += paths[ii].outerHTML.replace(/xmlns=".*" /g, '') + "\n";
        }
        output = output.slice(0, -1);
        jQuery('.sortable-container').prepend('<li class="sortable-item svg">\
          <div class="sortable-header">\
            <span class="dashicons dashicons-move sortable-handle"></span>\
            <span class="dashicons dashicons-trash sortable-delete"></span>\
          </div>\
          <div class="sortable-content row">\
            <div class="col-xs-6">\
              <svg v-if="validViewbox" v-bind:view-box.camel="validViewbox" v-html="paths"></svg>\
            </div>\
            <div class="col-xs-6">\
              <label for="svg[00][id]">ID</label>\
              <input id="svg[00][id]" name="svg[00][id]" type="text" value="' + id + '" />\
              <label for="svg[00][title]">Title</label>\
              <input id="svg[00][title]" name="svg[00][title]" type="text" value="' + title + '" />\
              <label for="svg[00][viewbox]">ViewBox</label>\
              <input id="svg[00][viewbox]" v-bind:class="{invalid: !valid}" name="svg[00][viewbox]" type="text" v-model="viewbox" value="' + viewBox + '" />\
            </div>\
            <div class="col-xs-12">\
              <label for="svg[00][path]">Paths</label>\
              <textarea id="svg[00][path]" name="svg[00][path]" v-model="paths">' + output + '</textarea>\
            </div>\
          </div>\
        </li>');
        checkOrder();
        checkSVG();
      }
      document.querySelector('.import-msg').innerHTML = 'Data successfully imported.';
      document.querySelector('.import-msg').style.color = 'green';
    }
    catch (err) {
      document.querySelector('.import-msg').innerHTML = 'There was an error reading the file. Confirm it is a .svg file and that the data is valid.';
      document.querySelector('.import-msg').style.color = 'red';
    }
  };
  reader.readAsText(e.target.files[0]);
});

// AMP options pages
if (document.querySelector('#triggers')) {
  new Vue({
    el: '#triggers',
    data: {
      triggers: '',
    },
    computed: {
      tabbedTriggers: function() {
        return this.triggers.replace(/\n/g, '\n  ');
      }
    }
  });
}

// SEO advanced options toggle
jQuery('#seo-meta-inside #seo-advanced-options-toggle').on('click', function(e) {
  e.preventDefault();
  jQuery('#seo-meta-inside #seo-advanced-options').slideToggle();
});

// Handle SEO preview
jQuery('#seo-meta-inside #seo-title').on('input', function() {
  jQuery('#seo-meta-inside #seo-preview-title .title').html(jQuery(this).val());
});
jQuery('#seo-meta-inside #seo-canonical').on('input', function() {
  jQuery('#seo-meta-inside #seo-preview-url').html(jQuery(this).val());
});
jQuery('#seo-meta-inside #seo-desc').on('input', function() {
  jQuery('#seo-meta-inside #seo-preview-desc').html(jQuery(this).val());
});
if (jQuery('#seo-meta-inside #seo-title').val()) {
  jQuery('#seo-meta-inside #seo-title').trigger('input');
}
if (jQuery('#seo-meta-inside #seo-canonical').val()) {
  jQuery('#seo-meta-inside #seo-canonical').trigger('input');
}
if (jQuery('#seo-meta-inside #seo-desc').val()) {
  jQuery('#seo-meta-inside #seo-desc').trigger('input');
}

// Media selector
var mediaUploader;
jQuery(document).on('click', '.media-selector', function(e) {
  e.preventDefault();
  var target = jQuery(this).attr('target');
  var size = jQuery(this).attr('size');
  mediaUploader = wp.media.frames.file_frame = wp.media({
    title: 'Select Image',
    button: {
    text: 'Select Image'
  }, multiple: false });
  mediaUploader.on('select', function() {
    attachment = mediaUploader.state().get('selection').first().toJSON();
    if (size && attachment.sizes[size]) {
      jQuery(target).val(attachment.sizes[size].url.substring(attachment.sizes[size].url.indexOf('/wp-content/')));
      jQuery(target)[0].dispatchEvent(new Event('input', {'bubbles': true}));
    } else {
      jQuery(target).val(attachment.url.substring(attachment.url.indexOf('/wp-content/')));
      jQuery(target)[0].dispatchEvent(new Event('input', {'bubbles': true}));
    }
  });
  mediaUploader.open();
});

// Actually enfore the min/max for inputs
jQuery('input[type=number]').on('input keypress', function(e) {
  // if (e.key=='-' || e.key=='.' || e.key=='+') {
  //   return false;
  // }
  if (jQuery(this).val() > parseInt(jQuery(this).attr('max'))) {
    jQuery(this).val(jQuery(this).attr('max'));
  } else if (jQuery(this).val() < parseInt(jQuery(this).attr('min'))) {
    jQuery(this).val(jQuery(this).attr('min'));
  }
});

// Post selector
function checkPostSelectors() {
  jQuery('.post-selector').each(function() {
    if (jQuery(this).attr('touched')) {
      return true;
    }
    jQuery(this).attr('touched', true);
    jQuery(this).attr('type', 'hidden');
    jQuery.ajax({
      context: this,
      url: locals.ajax_url,
      method: 'post',
      data: {
        action: '_ws_post_selector_types',
        id: jQuery(this).attr('id'),
        value: jQuery(this).val(),
        post_type: jQuery(this).attr('post-type') || '',
        multiple: jQuery(this).attr('multiple') || ''
      },
      success: function(res) {
        jQuery(this).after(res);
        var field = jQuery(this);
        jQuery('.chips').sortable({
          forcePlaceholderSize: true,
          start: function(e, ui) {
            var removed = field.val().split(',');
            removed.splice(ui.item.index(), 1);
            field.val(removed.join(','));
          },
          stop: function(e, ui) {
            var removed = field.val().split(',');
            removed.splice(ui.item.index(), 0, ui.item.attr('pid'));
            field.val(removed);
            field[0].dispatchEvent(new Event('input', {'bubbles': true}));
          }
        });
        if (jQuery(this).attr('multiple')) {
          // Multi add
          jQuery(this).next().next().on('change', '.post-selector-id input[type=radio]', function() {
            var field = jQuery(this).closest('.post-selector-container').prev().prev();
            if (field.val()) {
              field.val(field.val() + ',' + jQuery(this).val());
            }
            else {
              field.val(jQuery(this).val());
            }
            jQuery(this).closest('.post-selector-container').prev().append('<li pid="' + jQuery(this).val() + '"><span>' + jQuery(this).next().html() + '</span><button><span class="dashicons dashicons-no-alt"></span></button></li>');
            field[0].dispatchEvent(new Event('input', {'bubbles': true}));
          });
          // Multi remove
          jQuery(this).next().on('click', 'button', function(e) {
            e.preventDefault();
            var chip = jQuery(this).closest('li');
            var removed = jQuery(this).closest('.chips').prev().val().split(',');
            removed.splice(chip.index(), 1);
            jQuery(this).closest('.chips').prev().val(removed.join(','));
            jQuery(this).closest('.chips').prev()[0].dispatchEvent(new Event('input', {'bubbles': true}));
            chip.remove();
          });
        }
      }
    });
    // View post types
    jQuery('body').on('change', '.post-selector-type input[type=radio]', function() {
      var field = jQuery(this).closest('.post-selector-container').prev();
      jQuery.ajax({
        context: this,
        url: locals.ajax_url,
        method: 'post',
        data: {
          action: '_ws_post_selector_posts',
          id: field.attr('id'),
          value: field.val(),
          post_type: jQuery(this).val()
        },
        success: function(res) {
          jQuery(this).closest('.post-selector-container').children('.post-selector-id').html(res);
        }
      });
    });
    // Save post type
    jQuery('body').on('input', '.post-selector-filter', function() {
      var value = jQuery(this).val().toLowerCase();
      jQuery(this).next().children().each(function() {
        try {
          var thisValue = jQuery(this).children('label').html().toLowerCase();
          if (thisValue.includes(value)) {
            jQuery(this).show();
          } else {
            jQuery(this).hide();
          }
        } catch (err) {}
      });
    });
    // Single change
    jQuery('body').on('change', '.post-selector:not([multiple]) ~ .post-selector-container .post-selector-id input[type=radio]', function() {
      var field = jQuery(this).closest('.post-selector-container').prev();
      field.val(jQuery(this).val());
      field[0].dispatchEvent(new Event('input', {'bubbles': true}));
    });
  });
}
checkPostSelectors();

// SVG selector
function checkSvgSelectors() {
  jQuery('.svg-selector').each(function() {
    if (jQuery(this).attr('touched')) {
      return true;
    }
    jQuery(this).attr('touched', true);
    jQuery(this).attr('type', 'hidden');
    jQuery.ajax({
      context: this,
      url: locals.ajax_url,
      method: 'post',
      data: {
        action: '_ws_svg_selector',
        id: jQuery(this).attr('id'),
        value: jQuery(this).val()
      },
      success: function(res) {
        jQuery(this).after(res);
      }
    });
  });
  jQuery(document).on('click', function(e) {
    jQuery('.svg-selector-container ul').each(function() {
      jQuery(this).css('display', 'none');
    });
    if (e.target.closest('.svg-selector-container button')) {
      e.preventDefault();
      jQuery(e.target).closest('.svg-selector-container').find('ul').css('display', 'flex');
    }
  });
  jQuery('body').on('change', '.svg-selector-container input[type=radio]', function() {
    var svg = jQuery(this).closest('.svg-selector-container').find('.svg-selection use');
    svg.attr('xlink:href', '/wp-content/themes/_ws/template-parts/sprites.svg#' + jQuery(this).val());
    var field = jQuery(this).closest('.svg-selector-container').prev();
    field.val(jQuery(this).val());
    field[0].dispatchEvent(new Event('input', { 'bubbles': true }));
  });
}
checkSvgSelectors();

// Initialize any text editors
jQuery(document).ready(function() {
  jQuery('.text-editor').each(function() {
    if (jQuery(this).closest('.sortable-item').length) {
      return true;
    }
    wp.editor.initialize(this.id, {
      mediaButtons: true,
      tinymce: {
        block_formats: 'Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6;Preformatted=pre',
        toolbar1: 'formatselect, bold, italic, bullist, numlist, blockquote, alignleft, aligncenter, alignright, link, unlink, undo, redo'
      },
      quicktags: true
    });
  });
});

// Check order of sortable items (we add a tildes so that no fields share the same id at any single moment)
function checkOrder() {
  jQuery('.sortable-container').each(function() {
    jQuery(this).find('.sortable-item').each(function(i) {
      // We want to remove wp.editor fields first before their ids change
      jQuery(this).find('.text-editor').each(function() {
        wp.editor.remove(this.id);
      });
      jQuery(this).find('label').each(function() {
        if (jQuery(this).attr('for')) {
          var oldFor = jQuery(this).attr('for');
          jQuery(this).attr('for', oldFor.replace(/\d+/g, '~'+i));
        }
      });
      jQuery(this).find('input, textarea').each(function() {
        if (jQuery(this).attr('id')) {
          var oldId = jQuery(this).attr('id');
          jQuery(this).attr('id', oldId.replace(/\d+/g, '~'+i));
        }
        if (jQuery(this).attr('name')) {
          var oldName = jQuery(this).attr('name');
          jQuery(this).attr('name', oldName.replace(/\d+/g, '~'+i));
        }
      });
      jQuery(this).find('button').each(function() {
        if (jQuery(this).attr('target')) {
          var oldTarget = jQuery(this).attr('target');
          jQuery(this).attr('target', oldTarget.replace(/\d+/g, '~'+i));
        }
      });
    });
    // Remove the tildes
    jQuery(this).find('label').each(function() {
      if (v = jQuery(this).attr('for')) {
        jQuery(this).attr('for', v.replace(/~/g, ''));
      }
    });
    jQuery(this).find('input, textarea').each(function() {
      if (v = jQuery(this).attr('id')) {
        jQuery(this).attr('id', v.replace(/~/g, ''));
      }
      if (v = jQuery(this).attr('name')) {
        jQuery(this).attr('name', v.replace(/~/g, ''));
      }
    });
    jQuery(this).find('button').each(function() {
      if (v = jQuery(this).attr('target')) {
        jQuery(this).attr('target', v.replace(/~/g, ''));
      }
    });
    checkPostSelectors();
    checkSvgSelectors();
    jQuery(this).find('.text-editor').each(function() {
      wp.editor.initialize(this.id, {
        mediaButtons: true,
        tinymce: {
          block_formats: 'Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4;Header 5=h5;Header 6=h6;Preformatted=pre',
          toolbar1: 'formatselect, bold, italic, bullist, numlist, blockquote, alignleft, aligncenter, alignright, link, unlink, undo, redo'
        },
        quicktags: true
      });
    });
  });
}

// Sortable items are draggable if they have a child element with the "sortable-handle" class
jQuery(document).ready(function() {
  var start = 0;
  var end = 0;
  jQuery('.sortable-container').sortable({
    axis: 'y',
    handle: '.sortable-handle',
    cursor: 'ns-resize',
    placeholder: 'sortable-placeholder',
    forcePlaceholderSize: true,
    start: function(e, ui) {
      start = ui.item.index();
    },
    stop: function(e, ui) {
      end = ui.item.index();
      if (typeof pb !== 'undefined') {
        pb.reorder(start, end);
      }
      else {
        checkOrder();
      }
    }
  });
  checkOrder();
});
// Delete sortable item functionality
jQuery('body').on('click', '.sortable-delete', function(e) {
  e.preventDefault();
  jQuery(this).closest('.sortable-item').remove();
  checkOrder();
});

//=include wp-color-picker-alpha/src/wp-color-picker-alpha.js
