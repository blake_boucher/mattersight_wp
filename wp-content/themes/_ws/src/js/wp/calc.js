//=include vue/dist/vue.js

var s = location.hash.charAt(location.hash.length-1);
var ss = ['Seat Count', 'Industry', 'Metrics', 'Quick Report', 'Get a Detailed Report', 'Detailed Report Sent'];
var calcVue = new Vue({
  el: '#main.calculator-container',
  data: {
    steps: ss,
    // step: !isNaN(s) && s <= ss.length ? s : 0,
    step: 0,
    reps: 0,
    industry: '',
    averageHandleTime: '',
    annualCalls: '',
    annualCallsP: 7200000,
    costPerMinute: '',
    averageHandleTimeP: 550,
    costPerMinuteP: 0.8,
    netAHT: 0.055,
    invalid: false,
    invalid1: false,
    invalid2: false,
    invalid3: false,
    report: '',
    graph: ''
  },
  created: function() {
    var t = this;
    window.addEventListener('hashchange', function() {
      var s = location.hash.charAt(location.hash.length-1);
      if (!isNaN(s) && s <= t.steps.length) {
        t.step = s;
      }
    });
  },
  computed: {
    // ball: function() {
    //   var p = this.reps / 25000 * 100
    //   return p > 100 ? '100%' : p < 0 ? '0%' : p + '%';
    // },
    annualCallsFinal: function() {
      var averageHandleTime = this.averageHandleTime || this.averageHandleTimeP;
      return Math.round( (1100 / (averageHandleTime / 3600)) * this.reps );
    },
    annualMinutesSaved: function() {
      var annualCalls = this.annualCalls || this.annualCallsFinal;
      var averageHandleTime = this.averageHandleTime || this.averageHandleTimeP;
      return annualCalls * averageHandleTime * this.netAHT / 60;
    },
    year1: function() {
      var costPerMinute = this.costPerMinute || this.costPerMinuteP;
      return this.annualMinutesSaved * costPerMinute;
    },
    year2: function() {
      return this.year1 * 2;
    },
    year3: function() {
      return this.year1 * 3;
    }
  },
  watch: {
    year1: function(newVal, oldVal) {
      if (this.step == 4) {
        drawChart(newVal, this.year2, this.year3);
      }
    }
  },
  methods: {
    enter: function() {
      var t = document.querySelector('#anchor').offsetTop;
      window.scrollTo(0, t);
      if (this.step == 4) {
        this.drawChart.call();
      }
      if (this.step == 6) {
        findLazy();
      }
    },
    step1: function() {
      var reps = document.querySelector('#reps');
      if (!this.reps || this.reps <= 0) {
        this.invalid = true;
        reps.classList.add('invalid');
        reps.focus();
      }
      else {
        this.invalid = false;
        reps.classList.remove('invalid');
        location.hash = '#step-2';
      }
    },
    step2: function() {
      var opts = document.querySelector('.options');
      if (!this.industry) {
        this.invalid = true;
        opts.classList.add('invalid');
        opts.querySelector('label:first-of-type').focus();
      }
      else {
        this.invalid = false;
        opts.classList.remove('invalid');
        location.hash = '#step-3';
      }
    },
    step3: function() {
      if (this.averageHandleTime < 0) {
        this.invalid1 = true;
        document.querySelector('#handle-time').classList.add('invalid');
        document.querySelector('label[for=handle-time]').focus();
      }
      else {
        this.invalid1 = false;
        document.querySelector('#handle-time').classList.remove('invalid');
      }
      if (this.annualCalls < 0) {
        this.invalid2 = true;
        document.querySelector('#annual-calls').classList.add('invalid');
        document.querySelector('label[for=annual-calls]').focus();
      }
      else {
        this.invalid2 = false;
        document.querySelector('#annual-calls').classList.remove('invalid');
      }
      if (this.costPerMinute < 0) {
        this.invalid3 = true;
        document.querySelector('#cost').classList.add('invalid');
        document.querySelector('label[for=cost]').focus();
      }
      else {
        this.invalid3 = false;
        document.querySelector('#cost').classList.remove('invalid');
      }
      if (this.averageHandleTime >= 0 && this.annualCalls >= 0 && this.costPerMinute >= 0) {
        location.hash = '#step-4';
      }
    },
    drawChart: function() {
      var year = new Date().getFullYear();
      var webData = google.visualization.arrayToDataTable([
        ['Year', 'Routing Gross Value', {role: 'annotation'}],
        ['Year 1', this.year1, '$' + Math.round(this.year1/1000000*10)/10 + 'M'],
        ['Year 2', this.year2, '$' + Math.round(this.year2/1000000*10)/10 + 'M'],
        ['Year 3', this.year3, '$' + Math.round(this.year3/1000000*10)/10 + 'M']
      ]);
      // var pdfData = google.visualization.arrayToDataTable([
      //   ['Year', 'Routing Gross Value'],
      //   ['$' + Math.round(this.year1/1000000*10)/10 + 'M', this.year1],
      //   ['$' + Math.round(this.year2/1000000*10)/10 + 'M', this.year2],
      //   ['$' + Math.round(this.year3/1000000*10)/10 + 'M', this.year3]
      // ]);
      var options = {
        vAxis: {
          baselineColor: 'transparent',
          gridlines: {
            color: 'transparent'
          },
          textStyle: {
            color: 'transparent'
          },
          minValue: 0,
          viewWindow: {
            min: 0,
            max: this.year3 * 1.2
          }
        },
        hAxis: {
          textStyle: {
            color: '#fff',
            fontSize: 16
          }
        },
        annotations: {
          alwaysOutside: true,
          textStyle: {
            lineHeight: 20,
            color: '#fff',
            fontSize: 16
          },
          stem: {
            color: 'transparent'
          }
        },
        tooltip: {
          trigger: 'none'
        },
        legend: 'none',
        backgroundColor: 'transparent',
        colors: ['#97a3ae'],
        chartArea: {
          top: 0,
          left: 0,
          right: 0,
          bottom: 50
        }
      };
      var webEl = document.querySelector('#web-chart');
      var pdfEl = document.querySelector('#pdf-chart');
      if (webEl && pdfEl) {
        var webChart = new google.visualization.ColumnChart(webEl);
        webChart.draw(webData, options);
        options.bar = {groupWidth: '50%'};
        options.colors = ['#3c5064'];
        options.hAxis.textStyle.color = '#888';
        options.annotations.textStyle.color = '#888';
        options.hAxis.textStyle.fontSize = 18;
        var pdfChart = new google.visualization.ColumnChart(pdfEl);
        pdfChart.draw(webData, options);
        this.graph = pdfChart.getImageURI();
      }
    },
    generatePDF: function(email) {
      var t = this;
      var req = new XMLHttpRequest();
      req.open('POST', locals.ajax_url, true);
      req.onload = function() {
        // console.log(req.response);
        if (req.status >= 200 && req.status < 400) {
          location.hash = '#step-6';
        }
      };
      req.onerror = function() {
        // btn.outerHTML = 'We are sorry, but it appears that something has gone wrong. Please try again at a later time.';
      };
      req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      req.send('action=_ws_generatePDF&email=' + email + '&seatCount=' + this.reps + '&industry=' + this.industry + '&annualCalls=' + (this.annualCalls || this.annualCallsFinal) + '&averageHandleTime=' + (this.averageHandleTime || this.averageHandleTimeP) + '&costPerMinute=' + (this.costPerMinute || this.costPerMinuteP) + '&annualMinutesSaved=' + this.annualMinutesSaved + '&year1=' + this.year1 + '&year2=' + this.year2 + '&year3=' + this.year3 + '&graph=' + this.graph);
    }
  }
});
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(function() {
  calcVue.drawChart();
});
window.addEventListener('message', function(event) {
  if (event.origin !== 'https://go.mattersight.com') {
    return;
  }
  if (typeof event.data == 'string' && event.data.indexOf('@') !== -1) {
    calcVue.generatePDF(event.data);
  }
}, false);
