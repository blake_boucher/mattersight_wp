// Header Accessibility
var subs = document.querySelectorAll('.sub-menu a');
for (var i = 0; i < subs.length; i++) {
  subs[i].addEventListener('focus', function(e) {
    var parent = this.parentNode.parentNode;
    parent.style.left = 0;
    parent.style.opacity = 1;
    if (parent.parentNode.parentNode.classList.contains('sub-menu')) {
      var gparent = parent.parentNode.parentNode;
      gparent.style.left = 0;
      gparent.style.opacity = 1;
    }
  });
  subs[i].addEventListener('blur', function(e) {
    var parent = this.parentNode.parentNode;
    parent.style.removeProperty('left');
    parent.style.removeProperty('opacity');
    if (parent.parentNode.parentNode.classList.contains('sub-menu')) {
      var gparent = parent.parentNode.parentNode;
      gparent.style.removeProperty('left');
      gparent.style.removeProperty('opacity');
    }
  });
}

// Search toggle
var searchToggle = document.querySelector('.search-btn');
if (searchToggle) {
  var searchField = document.querySelector('.search-form input');
  searchToggle.addEventListener('click', function(event) {
    searchField.focus();
  });
  searchField.addEventListener('focus', function(event) {
    this.parentNode.style.left = 'auto';
    this.parentNode.style.opacity = 1;
  });
  searchField.addEventListener('blur', function(event) {
    this.parentNode.style.removeProperty('left');
    this.parentNode.style.removeProperty('opacity');
  });

  // document.addEventListener('click', function(event) {
  //   if (!checkParents(event.target, '.search-btn') && event.target != searchField && event.target != document.querySelector('#search-toggle')) {
  //     document.querySelector('#search-toggle').checked = false;
  //   }
  // });
}

// var search = document.querySelector('.filters #search');
// if (search) {
//   search.addEventListener('keyup', function(event) {
//     event.preventDefault();
//   });
// }

// Remove filters
var remove_filters = document.querySelectorAll('.remove-filters button');
if (remove_filters) {
  for (var i = 0; i < remove_filters.length; i++) {
    remove_filters[i].addEventListener('click', function(e) {
      document.querySelector('#filter-options input[value="' + this.dataset.filter + '"]').checked = false;
      document.querySelector('.filters form').submit();
    });
  }
}

// Investor filters
var year = document.querySelector('.investor-filter #year');
if (year) {
  year.addEventListener('change', function(event) {
    year.parentElement.submit();
  });
  var type = document.querySelector('.investor-filter #type');
  if (type) {
    type.addEventListener('change', function(event) {
      type.parentElement.submit();
    });
  }
}

function receiveMessage(event) {
  if (event.origin !== 'https://go.mattersight.com') {
    return;
  }
  if (Array.isArray(event.data) && event.data[1]=='submitted') {
    dataLayer.push({
      'event': 'pardotSubmit'
    });
  }
  else if (Array.isArray(event.data) && event.data[1]) {
    var el = document.querySelector('iframe[src="' + event.data[0] + '"]');
    if (el) {
      el.style.height = event.data[1] + 'px';
      el.style.overflowY = 'hidden';
    }
  }
}
window.addEventListener('message', receiveMessage, false);

var gallery = document.querySelector('[template=gallery]');
if (gallery) {
  if (location.search.indexOf('filters%5B%5D=') !== -1 || location.search.indexOf('search=') !== -1) {
    document.getElementById('anchor').scrollIntoView();
  }
}

// Card links
document.addEventListener('click', function(e) {
  var card = checkParents(e.target, '.gallery-card');
  card = card ? card : checkParents(e.target, '.link-card');
  if (card) {
    var l = card.querySelector('.arrow');
    l = l ? l : card.querySelector('.external');
    if (l) {
      if (l.hasAttribute('target')) {
        var temp = document.createElement('a');
        temp.href = l.href;
        temp.target = l.target;
        e.preventDefault();
        temp.click();
        temp = null;
      }
      else {
        location.href = l.href;
      }
    }
  }
});

var al = document.querySelectorAll('.toggle-list');
if (al) {
  for (var i = 0; i < al.length; i++) {
    al[i].addEventListener('click', function(e) {
      e.preventDefault();
      if (this.nextElementSibling.classList.contains('show')) {
        this.classList.remove('show');
        this.nextElementSibling.classList.remove('show');
      }
      else {
        this.classList.add('show');
        this.nextElementSibling.classList.add('show');
      }
    });
  }
}
