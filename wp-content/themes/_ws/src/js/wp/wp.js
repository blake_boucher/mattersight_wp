//=include svg4everybody/dist/svg4everybody.min.js
//=include objectFitPolyfill/dist/objectFitPolyfill.min.js
//include tiny-slider/dist/min/tiny-slider.js

// if (location.protocol === 'https:' || location.hostname === "localhost") {
//   if ('serviceWorker' in navigator) {
//     window.addEventListener('load', function() {
//       navigator.serviceWorker.register('/sw.js').then(function(registration) {
//         // Registration was successful
//         console.log('ServiceWorker registration successful with scope: ', registration.scope);
//       }, function(err) {
//         // registration failed :(
//         console.log('ServiceWorker registration failed: ', err);
//       });
//     });
//   }
// }

// Sets the correct transition event depending on browser
function whichTransitionEvent() {
  var t;
  var el = document.createElement('fakeelement');
  var transitions = {
    'transition':'transitionend',
    'OTransition':'oTransitionEnd',
    'MozTransition':'transitionend',
    'WebkitTransition':'webkitTransitionEnd'
  }
  for (t in transitions) {
    if (el.style[t] !== undefined) {
      return transitions[t];
    }
  }
}
var transitionEvent = whichTransitionEvent();

// Helper function to see if we're in a child element (for elements added dynamically)
function checkParents(el, selector) {
  var cur = el;
  var all = document.querySelectorAll(selector);
  while (cur) {
    for (var i = 0; i < all.length; i++) {
      if (all[i] == cur) {
        return cur;
      }
    }
    cur = cur.parentNode;
  }
  return false;
}

// Helper function for sending form data via ajax
function serializeForm(form) {
  var field, s = [];
  var len = form.elements.length;
  for (i = 0; i < len; i++) {
    field = form.elements[i];
    if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
      if (field.type == 'select-multiple') {
        for (ii = form.elements[i].options.length - 1; ii >= 0; ii--) {
          if (field.options[ii].selected) {
            s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[ii].value);
          }
        }
      } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
        s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
      }
    }
  }
  return s.join('&').replace(/%20/g, '+');
}

// Helper function for scrolling
function onScreen(el, offset) {
  if (offset == null) {
    offset = 0;
  }
  var isVisible = false;
  if (el) {
    if (el.offsetParent == null) {
      return false;
    }
    var elemTop = el.getBoundingClientRect().top;
    var elemBottom = el.getBoundingClientRect().bottom;
    isVisible = ((elemTop >= 0) && (elemTop <= window.innerHeight + offset)) // if top of element is on screen
    || ((elemBottom >= 0) && (elemBottom <= window.innerHeight - offset)) ; // if bottom of element is on screen
  }
  return isVisible;
}

// UC Browser styles
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );

// Lightbox logic
var lbs = document.querySelectorAll('a.lightbox-link');
for (i = 0; i < lbs.length; i++) {
  lbs[i].addEventListener('click', function(event) {
    event.preventDefault();
    var lb = document.createElement('div');
    lb.classList.add('lightbox');
    lb.setAttribute('role', 'dialog');
    var lbc = document.createElement('div');
    lbc.classList.add('container');
    lbc.classList.add('row');
    var lbh = document.createElement('div');
    lbh.classList.add('col-xs-12');
    lbh.classList.add('lightbox-header');
    var lbbtn = document.createElement('button');
    lbbtn.classList.add('lightbox-close');
    lbbtn.setAttribute('aria-label', 'Close Dialog');
    var lbsvg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    lbsvg.setAttribute('viewBox', '0 0 24 24');
    var lbpath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    lbpath.setAttribute('d', 'M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z');
    var lbr = document.createElement('div');
    lbr.classList.add('col-xs-12');
    lbr.classList.add('lightbox-content');
    var lbv = document.createElement('div');
    lbv.classList.add('video-container');
    var lbi = document.createElement('iframe');
    lbi.setAttribute('src', this.getAttribute('href'));
    lbi.setAttribute('frameborder', '0');
    lbi.setAttribute('webkitallowfullscreen', '');
    lbi.setAttribute('mozallowfullscreen', '');
    lbi.setAttribute('allowfullscreen', '');
    lb.appendChild(lbc);
    lbc.appendChild(lbh);
    lbh.appendChild(lbbtn);
    lbbtn.appendChild(lbsvg);
    lbsvg.appendChild(lbpath);
    lbc.appendChild(lbr);
    lbr.appendChild(lbv);
    lbv.appendChild(lbi)
    document.body.insertBefore(lb, document.body.firstChild);
    document.body.classList.add('no-scroll');
    window.setTimeout(function() {
      document.querySelector('.lightbox').classList.add('show');
    }, 10)
  });
}
lbs = document.querySelectorAll('.lightbox-button');
for (i = 0; i < lbs.length; i++) {
  lbs[i].addEventListener('click', function(event) {
    if (this.nextElementSibling.classList.contains('lightbox')) {
      this.nextElementSibling.classList.add('show');
      document.body.classList.add('no-scroll');
    }
  });
}
document.addEventListener('click', function(event) {
  if (checkParents(event.target, '.lightbox-close')) {
    var lightbox = document.querySelector('.lightbox');
    lightbox.classList.remove('show');
    lightbox.addEventListener(transitionEvent, function(event) {
      event.target.removeEventListener(event.type, arguments.callee);
      document.body.classList.remove('no-scroll');
      var lightbox = document.querySelector('.lightbox');
      try {
        lightbox.parentElement.removeChild(lightbox);
      }
      catch (err) {}
    });
  }
});

// Add class to header if page is not at its top
var scrollCheck = (function scrollCheck() {
  if (window.scrollY > 50) {
    document.querySelector('#site-header').classList.add('scroll-menu');
  } else {
    document.querySelector('#site-header').classList.remove('scroll-menu');
  }
  return scrollCheck;
}());
window.addEventListener('scroll', function() {
  scrollCheck();
});

// Slider Logic (Tiny Slider library)
var tinys = document.querySelectorAll('.slider-container');
for (i = 0; i < tinys.length; i++) {
  var tiny = tns({
    container: tinys[i].querySelector('.slider'),
    items: 1,
    mouseDrag: true
  });
}

// Social Media share buttons
var shares = document.querySelectorAll('.share-button');
for (i = 0; i < shares.length; i++) {
  shares[i].addEventListener('click', function(event) {
    event.preventDefault();
    window.open(this.getAttribute('href'), this.getAttribute('target'), 'resizeable,width=550,height=520');
  })
}

// Infinite scroll
var btns = document.querySelectorAll('.infinite-scroll-btn')
if (btns) {
  for (i = 0; i < btns.length; i++) {
    var div = document.createElement('div');
    div.classList.add('col-xs-12');
    div.classList.add('load-more');
    var btn = document.createElement('button');
    btn.innerHTML = 'load more +';
    div.appendChild(btn);
    btns[i].appendChild(div);
  }
}
document.addEventListener('click', function(event) {
  var btn = checkParents(event.target, '.load-more button');
  if (btn) {
    event.preventDefault();
    var loopContainer = btn.parentNode.parentNode;
    btn.innerHTML = 'Loading...';
    btn.disabled = true;
    var req = new XMLHttpRequest();
    req.open('POST', locals.ajax_url, true);
    req.onload = function() {
      var jsonRes = JSON.parse(req.response);
      if ((req.status >= 200 && req.status < 400) && jsonRes.success) {
        if (jsonRes.data) {
          btn.parentNode.insertAdjacentHTML('beforebegin', jsonRes.data.output);
          btn.innerHTML = 'load more +';
          btn.disabled = false;
          var page = loopContainer.getAttribute('page');
          if (page) {
            loopContainer.setAttribute('page', parseInt(page) + 1);
          }
          else {
            loopContainer.setAttribute('page', 3);
          }
          if (!jsonRes.data.more) {
            btn.parentNode.removeChild(btn);
          }
          findLazy();
          objectFitPolyfill();
        }
      } else {
        btn.innerHTML = 'Load More';
        btn.disabled = false;
      }
    };
    req.onerror = function() {
      btn.outerHTML = 'We are sorry, but it appears that something has gone wrong. Please try again at a later time.';
    };
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    var loop = loopContainer.querySelector('.loop-var') ? JSON.parse(loopContainer.querySelector('.loop-var').value) : '';
    var type = loopContainer.hasAttribute('type') ? '&type=' + loopContainer.getAttribute('type') : '';
    var l = encodeURIComponent(JSON.stringify(loop || locals.wp_query));
    req.send('action=_ws_get_more_posts&page=' + (loopContainer.getAttribute('page') || '2') + '&loop=' + l + type);
  }
});
var scrolls = document.querySelectorAll('.infinite-scroll');
if (scrolls) {
  for (i = 0; i < scrolls.length; i++) {
    var loopContainer = scrolls[i];
    var loading = false;
    var load = document.createElement('div');
    load.classList.add('col-xs-12', 'load-more');
    load.innerHTML = 'Loading...';
    loopContainer.appendChild(load);
    infiniteCheck(loopContainer);
  }
  window.addEventListener('scroll', function() {
    var containers = document.querySelectorAll('.infinite-scroll');
    for (i = 0; i < containers.length; i++) {
      infiniteCheck(containers[i]);
    }
  });
}
var loading = false;
function infiniteCheck(loopContainer) {
  var load = loopContainer.querySelector('.load-more');
  if (onScreen(load) && !loading) {
    loading = true;
    var req = new XMLHttpRequest();
    req.open('POST', locals.ajax_url, true);
    req.onload = function() {
      var jsonRes = JSON.parse(req.response);
      if ((req.status >= 200 && req.status < 400) && jsonRes.success) {
        if (jsonRes.data) {
          load.insertAdjacentHTML('beforebegin', jsonRes.data.output);
          var page = loopContainer.getAttribute('page');
          if (page) {
            loopContainer.setAttribute('page', parseInt(page) + 1);
          }
          else {
            loopContainer.setAttribute('page', 3);
          }
          if (!jsonRes.data.more) {
            load.parentNode.removeChild(load);
          }
          loading = false;
          findLazy();
          objectFitPolyfill();
          infiniteCheck(loopContainer);
        }
      } else {
        // Don't do anything
      }
    };
    req.onerror = function() {
      load.outerHTML = 'We are sorry, but it appears that something has gone wrong. Please try again at a later time.';
    };
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    var loopVar = loopContainer.querySelector('.loop-var');
    if (loopVar) {
      var loop = JSON.parse(loopVar.value);
    }
    var type = loopContainer.hasAttribute('type') ? '&type=' + loopContainer.getAttribute('type') : '';
    var l = encodeURIComponent(JSON.stringify(typeof loop !== 'undefined' ? loop : locals.wp_query));
    req.send('action=_ws_get_more_posts&page=' + (loopContainer.getAttribute('page') || '2') + '&loop=' + l + type);
  }
}

// Actually enforce the min/max for inputs
var numbers = document.querySelectorAll('input[type=number]');
for (i = 0; i < numbers.length; i++) {
  numbers[i].addEventListener('input', checkMinMax);
  numbers[i].addEventListener('keypress', checkMinMax);
}
function checkMinMax() {
  if (this.value > parseInt(this.getAttribute('max'))) {
    this.value = this.getAttribute('max');
  } else if (this.value < parseInt(this.getAttribute('min'))) {
    this.value = this.getAttribute('min');
  }
}

// Lazy load
var lazys = document.querySelectorAll('.lazy-load[data-src]:not([src])');
function findLazy() {
  lazys = document.querySelectorAll('.lazy-load[data-src]:not([src])');
  for (i = 0; i < lazys.length; i++) {
    if (lazys[i].tagName=='IMG') {
      lazys[i].src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
    }
  }
  lazyCheck();
}
function lazyCheck() {
  for (i = 0; i < lazys.length; i++) {
    if (onScreen(lazys[i], 200)) {
      if (lazys[i].tagName=='IMG') {
        if (lazys[i].getAttribute('src')=='data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7') {
          lazys[i].src = lazys[i].dataset['src'];
          if (lazys[i].dataset['srcset']) {
            lazys[i].srcset = lazys[i].dataset['srcset'];
          }
          if (lazys[i].hasAttribute('data-object-fit')) {
            objectFitPolyfill(lazys[i]);
            setTimeout(function() {
              objectFitPolyfill(lazys[i]);
            }, 100);
          }
        }
      }
      else {
        if (!lazys[i].style.backgroundImage) {
          lazys[i].style.backgroundImage = 'url(' + lazys[i].dataset['src'] + ')';
        }
      }
    }
  }
};
window.addEventListener('DOMContentLoaded', function() {
  findLazy();
}, false);
window.addEventListener('scroll', function() {
  lazyCheck();
});

// Toggle slide
function checkToggles() {
  var toggleBtns = document.querySelectorAll('.toggle');
  for (var i = 0; i < toggleBtns.length; i++) {
    var btn = toggleBtns[i];
    var t = document.querySelector(btn.getAttribute('target'));
    t.style.height = 0;
    t.classList.remove('open');
    t.classList.add('closed');
    btn.addEventListener('click', function(event) {
      event.preventDefault();
      var t = document.querySelector(this.getAttribute('target'));
      if (t.style.height == '0px') {
        t.style.height = t.scrollHeight + 'px';
        this.classList.remove('closed');
        this.classList.add('open');
        t.classList.remove('closed');
        t.classList.add('open');
        if (this.classList.contains('location-heading')) {
          this.parentElement.classList.add('shadow');
        }
        if (this.parentElement.parentElement.parentElement.parentElement.classList.contains('suite')) {
          this.parentElement.parentElement.parentElement.classList.add('shadow');
        }
      }
      else {
        t.style.height = 0;
        this.classList.remove('open');
        this.classList.add('closed');
        t.classList.remove('open');
        t.classList.add('closed');
        if (this.classList.contains('location-heading')) {
          this.parentElement.classList.remove('shadow');
        }
        if (this.parentElement.parentElement.parentElement.parentElement.classList.contains('suite')) {
          this.parentElement.parentElement.parentElement.classList.remove('shadow');
        }
      }
    });
  }
}
checkToggles();

//=include _custom.js

if (typeof svg4everybody == 'function') {
  svg4everybody();
}
