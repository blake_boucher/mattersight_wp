<?php get_header(); ?>

<main id="main" class="page">
  <?php
  get_template_part('template-parts/banner');
  if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section id="post-<?php the_ID(); ?>" <?php post_class('page page-content'); ?>>
      <div class="container row">
        <div class="col-xl-8 col-lg-10 wp-text">
          <?php the_content(); ?>
        </div>
      </div>
    </section>
  <?php
  endwhile; endif; ?>
</main>

<?php get_footer(); ?>
