<?php
// Register industry taxonomy
function _ws_industry_taxonomy() {
  $labels = array(
    'name' => 'Industries',
    'singular_name' => 'Industry',
    'all_items' => 'All Industries',
    'edit_item' => 'Edit Industry',
    'view_item' => 'View Industry',
    'update_item' => 'Update Industry',
    'add_new_item' => 'Add New Industry',
    'new_item_name' => 'New Industry Name',
    'parent_item' => 'Parent Industry',
    'parent_item_colon' => 'Parent Industry:',
    'search_items' => 'Search Industries',
    'popular_items' => 'Popular Industries',
    'separate_items_with_commas' => 'Separate industries with commas',
    'add_or_remove_items' => 'Add or remove industries',
    'choose_from_most_used' => 'Choose from the most used industries',
    'not_found' => 'No industries found.'
  );
  register_taxonomy(
    'industry',
    array('customer_story'),
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'show_in_menus' => true,
      'show_in_nav_menus' => false,
      'show_tagcloud' => false,
      'show_in_quick_edit' => true,
      'meta_box_cb' => null,
      'show_admin_column' => true,
      'description' => 'Taxonomy for customer stories',
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
      'capabilities' => array(),
      'sort' => false
    )
  );
}
add_action('init', '_ws_industry_taxonomy');
