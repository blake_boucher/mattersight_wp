<?php
// Register resource cat taxonomy
function _ws_resource_category_taxonomy() {
  $labels = array(
    'name' => 'Resource Categories',
    'singular_name' => 'Resource Category',
    'all_items' => 'All Resource Categories',
    'edit_item' => 'Edit Resource Category',
    'view_item' => 'View Resource Category',
    'update_item' => 'Update Resource Category',
    'add_new_item' => 'Add New Resource Category',
    'new_item_name' => 'New Resource Category Name',
    'parent_item' => 'Parent Resource Category',
    'parent_item_colon' => 'Parent Resource Category:',
    'search_items' => 'Search Resource Categories',
    'popular_items' => 'Popular Resource Categories',
    'separate_items_with_commas' => 'Separate resource categories with commas',
    'add_or_remove_items' => 'Add or remove resource categories',
    'choose_from_most_used' => 'Choose from the most used resource categories',
    'not_found' => 'No resource categories found.'
  );
  register_taxonomy(
    'resource_category',
    array('resource'),
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'show_in_menus' => true,
      'show_in_nav_menus' => false,
      'show_tagcloud' => false,
      'show_in_quick_edit' => true,
      'meta_box_cb' => null,
      'show_admin_column' => true,
      'description' => 'Taxonomy for resources',
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
      'capabilities' => array(),
      'sort' => false
    )
  );
}
add_action('init', '_ws_resource_category_taxonomy');
