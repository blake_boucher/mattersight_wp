<?php
// Register type taxonomy
function _ws_type_taxonomy() {
  $labels = array(
    'name' => 'Types',
    'singular_name' => 'Type',
    'all_items' => 'All Types',
    'edit_item' => 'Edit Type',
    'view_item' => 'View Type',
    'update_item' => 'Update Type',
    'add_new_item' => 'Add New Type',
    'new_item_name' => 'New Type Name',
    'parent_item' => 'Parent Type',
    'parent_item_colon' => 'Parent Type:',
    'search_items' => 'Search Types',
    'popular_items' => 'Popular Types',
    'separate_items_with_commas' => 'Separate types with commas',
    'add_or_remove_items' => 'Add or remove types',
    'choose_from_most_used' => 'Choose from the most used types',
    'not_found' => 'No types found.'
  );
  register_taxonomy(
    'type',
    array('customer_story'),
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'show_in_menus' => true,
      'show_in_nav_menus' => false,
      'show_tagcloud' => false,
      'show_in_quick_edit' => true,
      'meta_box_cb' => null,
      'show_admin_column' => true,
      'description' => 'Taxonomy for customer stories',
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
      'capabilities' => array(),
      'sort' => false
    )
  );
}
add_action('init', '_ws_type_taxonomy');
