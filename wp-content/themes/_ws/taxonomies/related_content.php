<?php
// Register related content type taxonomy
function _ws_related_content_taxonomy() {
  $labels = array(
    'name' => 'Related Content',
    'singular_name' => 'Related Content',
    'all_items' => 'All Related Content',
    'edit_item' => 'Edit Related Content',
    'view_item' => 'View Related Content',
    'update_item' => 'Update Related Content',
    'add_new_item' => 'Add New Related Content',
    'new_item_name' => 'New Related Content Name',
    'parent_item' => 'Parent Related Content',
    'parent_item_colon' => 'Parent Related Content:',
    'search_items' => 'Search Related Content',
    'popular_items' => 'Popular Related Content',
    'separate_items_with_commas' => 'Separate related content with commas',
    'add_or_remove_items' => 'Add or remove related content',
    'choose_from_most_used' => 'Choose from the most used related content',
    'not_found' => 'No related content found.'
  );
  register_taxonomy(
    'related_content',
    array('resource', 'customer_story', 'post', 'news', 'event'),
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'show_in_menus' => true,
      'show_in_nav_menus' => false,
      'show_tagcloud' => false,
      'show_in_quick_edit' => true,
      'meta_box_cb' => null,
      'show_admin_column' => true,
      'description' => 'Taxonomy for related content across several post types',
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
      'capabilities' => array(),
      'sort' => false
    )
  );
}
add_action('init', '_ws_related_content_taxonomy');
