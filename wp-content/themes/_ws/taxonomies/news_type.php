<?php
// Register news type taxonomy
function _ws_news_type_taxonomy() {
  $labels = array(
    'name' => 'News Types',
    'singular_name' => 'News Type',
    'all_items' => 'All News Types',
    'edit_item' => 'Edit News Type',
    'view_item' => 'View News Type',
    'update_item' => 'Update News Type',
    'add_new_item' => 'Add New News Type',
    'new_item_name' => 'New News Type Name',
    'parent_item' => 'Parent News Type',
    'parent_item_colon' => 'Parent News Type:',
    'search_items' => 'Search News Types',
    'popular_items' => 'Popular News Types',
    'separate_items_with_commas' => 'Separate news types with commas',
    'add_or_remove_items' => 'Add or remove news types',
    'choose_from_most_used' => 'Choose from the most used news types',
    'not_found' => 'No news types found.'
  );
  register_taxonomy(
    'news_type',
    array('news'),
    array(
      'labels' => $labels,
      'public' => true,
      'show_ui' => true,
      'show_in_menus' => true,
      'show_in_nav_menus' => false,
      'show_tagcloud' => false,
      'show_in_quick_edit' => true,
      'meta_box_cb' => null,
      'show_admin_column' => true,
      'description' => 'Taxonomy for news',
      'hierarchical' => true,
      'query_var' => true,
      'rewrite' => true,
      'capabilities' => array(),
      'sort' => false
    )
  );
}
add_action('init', '_ws_news_type_taxonomy');
