<?php
// Register investor news post type
function _ws_investor_news_post_type() {
  $labels = array(
    'name' => 'Investor News',
    'singular_name' => 'Investor News',
    'add_new_item' => 'Add New Investor News',
    'edit_item' => 'Edit Investor News',
    'new_item' => 'New Investor News',
    'view_item' => 'View Investor News',
    'search_items' => 'Search Investor News',
    'not_found' => 'No investor news found',
    'not_found_in_trash' => 'No investor news found in Trash',
    'parent_item_colon' => 'Parent Investor News:',
    'all_items' => 'All Investor News',
    'archives' => 'Investor News Archives',
    'insert_into_item' => 'Insert into investor news',
    'uploaded_to_this_item' => 'Uploaded to this investor news',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable investor news',
    'public' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => false,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-dismiss',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'about/investors/latest-news', 'with_front'=>false),
    'query_var' => true
  );
  register_post_type('investor_news', $args);
}
add_action('init', '_ws_investor_news_post_type');

function _ws_activate_scheduled_news_check() {
  if (!wp_next_scheduled('_ws_daily_news_check')) {
    wp_schedule_event(time(), 'twicedaily', '_ws_daily_news_check');
  }
}
add_action('after_setup_theme', '_ws_activate_scheduled_news_check');

function _ws_external_news() {
  checkNewsPage("https://clientapi.gcs-web.com/data/2c370818-342c-4412-933e-1ec15abb5fa0/news");
}
add_action('_ws_daily_news_check', '_ws_external_news');

function _ws_deactivate_scheduled_news_check() {
  wp_clear_scheduled_hook('_ws_daily_news_check');
}
add_action('switch_theme', '_ws_deactivate_scheduled_news_check');

function checkNewsPage($url) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  $data = curl_exec($curl);
  curl_close($curl);
  $data = json_decode($data);
  foreach ($data->data as $i=>$news) {
    $n = get_posts(array('post_type'=>'investor_news', 'post_status'=>'publish', 'meta_key'=>'_investor-news-id', 'meta_value'=>$news->id));
    if ($n) {
      break;
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $news->link->url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $thisNews = curl_exec($curl);
    curl_close($curl);
    $thisNews = json_decode($thisNews);
    wp_insert_post(array(
      'post_date' => $news->releaseDate->dateUTC,
      'post_content' => $thisNews->data->body[0]->value,
      'post_title' => $news->title,
      'post_status' => 'publish',
      'post_type' => 'investor_news',
      'meta_input' => array(
        '_investor-news-id' => $news->id
      )
    ));
    if ($i + 1 >= $data->meta->count && $data->links->next) {
      checkNewsPage($data->links->next);
    }
  }
}
