<?php
// Register research post type
function _ws_research_post_type() {
  $labels = array(
    'name' => 'Research',
    'singular_name' => 'Research',
    'add_new_item' => 'Add New Research',
    'edit_item' => 'Edit Research',
    'new_item' => 'New Research',
    'view_item' => 'View Research',
    'search_items' => 'Search Research',
    'not_found' => 'No research found',
    'not_found_in_trash' => 'No research found in Trash',
    'parent_item_colon' => 'Parent Research:',
    'all_items' => 'All Research',
    'archives' => 'Research Archives',
    'insert_into_item' => 'Insert into research',
    'uploaded_to_this_item' => 'Uploaded to this research',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable researchs',
    'public' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-chart-pie',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'resources/personality-labs', 'with_front'=>false),
    'query_var' => true
  );
  register_post_type('research', $args);
}
add_action('init', '_ws_research_post_type');

// Fill meta box
function _ws_research_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'research-nonce');
  $research_img = get_post_meta(get_the_ID(), '_research-img', true);
  $research_card = get_post_meta(get_the_ID(), '_research-card', true); ?>
  <div id="research-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="research-img">Resource Image</label>
          <div class="row">
            <button class="button media-selector" target="#research-img" size="medium">Select Image</button>
            <input id="research-img" class="flex-1" name="research-img" type="text" value="<?= $research_img; ?>" />
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="research-card">Form <small>Card</small></label>
          <textarea id="research-card" name="research-card"><?= $research_card; ?></textarea>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_research_meta() {
  add_meta_box('research_meta', 'Research', '_ws_research_meta_fields', 'research', 'normal', 'high');
}
add_action('admin_init', '_ws_research_meta');

// Save meta values
function _ws_save_research_meta($post_id) {
  if (!isset($_POST['research-nonce']) || !wp_verify_nonce($_POST['research-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $research_img = isset($_POST['research-img']) ? $_POST['research-img'] : '';
  update_post_meta($post_id, '_research-img', $research_img);

  $research_card = isset($_POST['research-card']) ? $_POST['research-card'] : '';
  update_post_meta($post_id, '_research-card', $research_card);
}
add_action('save_post', '_ws_save_research_meta');
