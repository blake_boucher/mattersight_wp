<?php
// Register investor event post type
function _ws_investor_event_post_type() {
  $labels = array(
    'name' => 'Investor Events',
    'singular_name' => 'Investor Event',
    'add_new_item' => 'Add New Investor Event',
    'edit_item' => 'Edit Investor Event',
    'new_item' => 'New Investor Event',
    'view_item' => 'View Investor Event',
    'search_items' => 'Search Investor Events',
    'not_found' => 'No investor events found',
    'not_found_in_trash' => 'No investor events found in Trash',
    'parent_item_colon' => 'Parent Investor Event:',
    'all_items' => 'All Investor Events',
    'archives' => 'Investor Event Archives',
    'insert_into_item' => 'Insert into investor event',
    'uploaded_to_this_item' => 'Uploaded to this investor event',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable investor events',
    'public' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => false,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-dismiss',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'about/investors/investor-events', 'with_front'=>false),
    'query_var' => true
  );
  register_post_type('investor_event', $args);
}
add_action('init', '_ws_investor_event_post_type');

function _ws_activate_scheduled_event_check() {
  if (!wp_next_scheduled('_ws_daily_event_check')) {
    wp_schedule_event(time(), 'twicedaily', '_ws_daily_event_check');
  }
}
add_action('after_setup_theme', '_ws_activate_scheduled_event_check');

function _ws_external_events() {
  checkEventPage("https://clientapi.gcs-web.com/data/2c370818-342c-4412-933e-1ec15abb5fa0/events");
}
add_action('_ws_daily_event_check', '_ws_external_events');

function _ws_deactivate_scheduled_event_check() {
  wp_clear_scheduled_hook('_ws_daily_event_check');
}
add_action('switch_theme', '_ws_deactivate_scheduled_event_check');

function checkEventPage($url) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  $data = curl_exec($curl);
  curl_close($curl);
  $data = json_decode($data);
  foreach ($data->data as $i=>$event) {
    $n = get_posts(array('post_type'=>'investor_event', 'post_status'=>'publish', 'meta_key'=>'_investor-event-id', 'meta_value'=>$event->id));
    if ($n) {
      break;
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $event->link->url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $thisEvent = curl_exec($curl);
    curl_close($curl);
    $thisEvent = json_decode($thisEvent);
    $content = '<p><b>Date</b><br />' . date('F j, Y \a\t g:i A ', strtotime($event->startDate->date)) . $event->startDate->timezone->code . '<br />' . date('F j, Y \a\t g:i A ', strtotime($event->endDate->date)) . $event->endDate->timezone->code . '</p>';
    if ($event->webCast->url) {
      $content .= '<p><a href="' . $event->webCast->url . '">' . $event->webCast->title . '</a></p>';
    }
    if ($event->location->locality) {
      $content .= '<p><b>Location</b><br />' . ($event->location->locName ? $event->location->locName . '<br />' : '') . $event->location->locality . ($event->location->administrativeArea ? ', ' . $event->location->administrativeArea : '') . '</p>';
    }
    if ($thisEvent->data->teleconference) {
      $content .= '<p><b>Teleconference Information</b><br />Primary: ' . $thisEvent->data->teleconference->live->primary . '<br />Secondary: ' . $thisEvent->data->teleconference->live->secondary . '<br />Passcode: ' . $thisEvent->data->teleconference->live->passcode . '</p>';
    }
    wp_insert_post(array(
      'post_date' => $event->startDate->date,
      'post_content' => $content,
      'post_title' => $event->title,
      'post_status' => 'publish',
      'post_type' => 'investor_event',
      'meta_input' => array(
        '_investor-event-id' => $event->id
      )
    ));
    if ($i + 1 >= $data->meta->count && $data->links->next) {
      checkEventPage($data->links->next);
    }
  }
}
