<?php
// Register resource post type
function _ws_resource_post_type() {
  $labels = array(
    'name' => 'Resources',
    'singular_name' => 'Resource',
    'add_new_item' => 'Add New Resource',
    'edit_item' => 'Edit Resource',
    'new_item' => 'New Resource',
    'view_item' => 'View Resource',
    'search_items' => 'Search Resources',
    'not_found' => 'No resources found',
    'not_found_in_trash' => 'No resources found in Trash',
    'parent_item_colon' => 'Parent Resource:',
    'all_items' => 'All Resources',
    'archives' => 'Resource Archives',
    'insert_into_item' => 'Insert into resource',
    'uploaded_to_this_item' => 'Uploaded to this resource',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable resources',
    'public' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-analytics',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'resources/insights', 'with_front'=>false),
    'query_var' => true
  );
  register_post_type('resource', $args);
}
add_action('init', '_ws_resource_post_type');

// Fill meta box
function _ws_resource_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'resource-nonce');
  $resource_img = get_post_meta(get_the_ID(), '_resource-img', true);
  $resource_card = get_post_meta(get_the_ID(), '_resource-card', true); ?>
  <div id="resource-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="resource-img">Resource Image</label>
          <div class="row">
            <button class="button media-selector" target="#resource-img" size="medium">Select Image</button>
            <input id="resource-img" class="flex-1" name="resource-img" type="text" value="<?= $resource_img; ?>" />
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="resource-card">Form <small>Card</small></label>
          <textarea id="resource-card" name="resource-card"><?= $resource_card; ?></textarea>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_resource_meta() {
  add_meta_box('resource_meta', 'Resource', '_ws_resource_meta_fields', 'resource', 'normal', 'high');
}
add_action('admin_init', '_ws_resource_meta');

// Save meta values
function _ws_save_resource_meta($post_id) {
  if (!isset($_POST['resource-nonce']) || !wp_verify_nonce($_POST['resource-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $resource_img = isset($_POST['resource-img']) ? $_POST['resource-img'] : '';
  update_post_meta($post_id, '_resource-img', $resource_img);

  $resource_card = isset($_POST['resource-card']) ? $_POST['resource-card'] : '';
  update_post_meta($post_id, '_resource-card', $resource_card);
}
add_action('save_post', '_ws_save_resource_meta');
