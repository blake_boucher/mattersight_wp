<?php
// Register customer story post type
function _ws_customer_story_post_type() {
  $labels = array(
    'name' => 'Customer Stories',
    'singular_name' => 'Customer Story',
    'add_new_item' => 'Add New Customer Story',
    'edit_item' => 'Edit Customer Story',
    'new_item' => 'New Customer Story',
    'view_item' => 'View Customer Story',
    'search_items' => 'Search Customer Stories',
    'not_found' => 'No customer stories found',
    'not_found_in_trash' => 'No customer stories found in Trash',
    'parent_item_colon' => 'Parent Customer Story:',
    'all_items' => 'All Customer Stories',
    'archives' => 'Customer Story Archives',
    'insert_into_item' => 'Insert into customer story',
    'uploaded_to_this_item' => 'Uploaded to this customer story',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
    );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable people',
    'public' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-book',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'customer-stories', 'with_front'=>false),
    'query_var' => true
    );
  register_post_type('customer_story', $args);
}
add_action('init', '_ws_customer_story_post_type');

// Fill meta box
function _ws_customer_story_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'cs-nonce');
  $cs_logo = get_post_meta(get_the_ID(), '_cs-logo', true);
  $cs_kpi_1 = get_post_meta(get_the_ID(), '_cs-kpi-1', true);
  $cs_kpi_2 = get_post_meta(get_the_ID(), '_cs-kpi-2', true);
  $cs_kpi_3 = get_post_meta(get_the_ID(), '_cs-kpi-3', true);
  $cs_client_name = get_post_meta(get_the_ID(), '_cs-client-name', true);
  $cs_related_products = get_post_meta(get_the_ID(), '_cs-related-products', true);
  $cs_testimonial = get_post_meta(get_the_ID(), '_cs-testimonial', true);
  $cs_bottom_text = get_post_meta(get_the_ID(), '_cs-bottom-text', true);
  $cs_related_heading = get_post_meta(get_the_ID(), '_cs-related-heading', true);
  $cs_related_type = get_post_meta(get_the_ID(), '_cs-related-type', true);
  $cs_related_post_one = get_post_meta(get_the_ID(), '_cs-related-post-one', true);
  $cs_related_post_two = get_post_meta(get_the_ID(), '_cs-related-post-two', true);
  $cs_related_post_three = get_post_meta(get_the_ID(), '_cs-related-post-three', true);
  $cs_related_post_type = get_post_meta(get_the_ID(), '_cs-related-post-type', true);
  $cs_related_tax = get_post_meta(get_the_ID(), '_cs-related-tax', true);
  $cs_cta_heading = get_post_meta(get_the_ID(), '_cs-cta-heading', true);
  $cs_cta_text = get_post_meta(get_the_ID(), '_cs-cta-text', true);
  $cs_cta_btn_text = get_post_meta(get_the_ID(), '_cs-cta-btn-text', true);
  $cs_cta_btn_link = get_post_meta(get_the_ID(), '_cs-cta-btn-link', true);
  $cs_cta_bg_img = get_post_meta(get_the_ID(), '_cs-cta-bg-img', true); ?>
  <div id="cs-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="cs-logo">Logo</label>
          <div class="row">
            <button class="button media-selector" target="#cs-logo" size="standard">Select Image</button>
            <input id="cs-logo" class="flex-1" name="cs-logo" type="text" value="<?= $cs_logo; ?>" />
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 1</legend>
            <ul>
              <li>
                <label for="cs-kpi-1-svg">Icon</label>
                <input id="cs-kpi-1-svg" name="cs-kpi-1[svg]" class="svg-selector" value="<?= $cs_kpi_1['svg']; ?>" />
                <small><a href="/wp-admin/options-general.php?page=svg" target="_blank">SVG Manager &rarr;</a></small>
              </li>
              <li>
                <label for="cs-kpi-1-title">Title</label>
                <input id="cs-kpi-1-title" name="cs-kpi-1[title]" type="text" value="<?= $cs_kpi_1['title']; ?>" />
              </li>
              <li>
                <label for="cs-kpi-1-text">Text</label>
                <textarea id="cs-kpi-1-text" name="cs-kpi-1[text]"><?= $cs_kpi_1['text']; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 2</legend>
            <ul>
              <li>
                <label for="cs-kpi-2-svg">Icon</label>
                <input id="cs-kpi-2-svg" name="cs-kpi-2[svg]" class="svg-selector" value="<?= $cs_kpi_2['svg']; ?>" />
                <small><a href="/wp-admin/options-general.php?page=svg" target="_blank">SVG Manager &rarr;</a></small>
              </li>
              <li>
                <label for="cs-kpi-2-title">Title</label>
                <input id="cs-kpi-2-title" name="cs-kpi-2[title]" type="text" value="<?= $cs_kpi_2['title']; ?>" />
              </li>
              <li>
                <label for="cs-kpi-2-text">Text</label>
                <textarea id="cs-kpi-2-text" name="cs-kpi-2[text]"><?= $cs_kpi_2['text']; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 3</legend>
            <ul>
              <li>
                <label for="cs-kpi-3-svg">Icon</label>
                <input id="cs-kpi-3-svg" name="cs-kpi-3[svg]" class="svg-selector" value="<?= $cs_kpi_3['svg']; ?>" />
                <small><a href="/wp-admin/options-general.php?page=svg" target="_blank">SVG Manager &rarr;</a></small>
              </li>
              <li>
                <label for="cs-kpi-3-title">Title</label>
                <input id="cs-kpi-3-title" name="cs-kpi-3[title]" type="text" value="<?= $cs_kpi_3['title']; ?>" />
              </li>
              <li>
                <label for="cs-kpi-3-text">Text</label>
                <textarea id="cs-kpi-3-text" name="cs-kpi-3[text]"><?= $cs_kpi_3['text']; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="cs-client-name">Client Name</label>
          <input id="cs-client-name" name="cs-client-name" type="text" value="<?= $cs_client_name; ?>" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="cs-related-products">Related Products</label>
          <input id="cs-related-products" name="cs-related-products" class="post-selector" post-type="page" multiple value="<?= $cs_related_products; ?>" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="cs-testimonial">Testimonial</label>
          <input id="cs-testimonial" name="cs-testimonial" class="post-selector" post-type="testimonial" value="<?= $cs_testimonial; ?>" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="cs-bottom-text">Additional Text Section</label>
          <textarea id="cs-bottom-text" name="cs-bottom-text" class="text-editor"><?= $cs_bottom_text; ?></textarea>
        </div>
      </li>
      <li class="row pb-related">
        <div class="col-xs-12">
          <fieldset>
            <legend>Related Content</legend>
            <ul>
              <li>
                <label for="cs-related-heading">Heading</label>
                <input id="cs-related-heading" name="cs-related-heading" type="text" value="<?= $cs_related_heading; ?>" >
              </li>
              <li>
                <input id="cs-related-type-posts" name="cs-related-type" type="radio" value="posts" <?= $cs_related_type=='posts' ? 'checked' : ''; ?> />
                <label for="cs-related-type-posts">Specific Posts</label>
                <div class="row">
                  <div class="col-xs-4">
                    <input id="cs-related-post-one" name="cs-related-post-one" class="post-selector" type="text" value="<?= $cs_related_post_one; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="cs-related-post-two" name="cs-related-post-two" class="post-selector" type="text" value="<?= $cs_related_post_two; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="cs-related-post-three" name="cs-related-post-three" class="post-selector" type="text" value="<?= $cs_related_post_three; ?>" />
                  </div>
                </div>
              </li>
              <li>
                <input id="cs-related-type-post-type" name="cs-related-type" type="radio" value="post-type" <?= $cs_related_type=='post-type' ? 'checked' : ''; ?> />
                <label for="cs-related-type-post-type">Post Type</label>
                <div>
                  <select id="cs-related-post-type" name="cs-related-post-type">
                    <?php
                    $types = get_post_types(array('public'=>true));
                    foreach ($types as $type) {
                      $t = get_post_type_object($type);
                      echo '<option value="' . $t->name . '" ' . ($cs_related_post_type==$t->name ? 'selected' : '') . '>' . $t->labels->name . '</option>';
                    } ?>
                  </select>
                </div>
              </li>
              <li>
                <input id="cs-related-type-tax" name="cs-related-type" type="radio" value="tax" <?= $cs_related_type=='tax' ? 'checked' : ''; ?> />
                <label for="cs-related-type-tax">Taxonomy</label>
                <div>
                  <select id="cs-related-tax" name="cs-related-tax">
                    <?php
                    $terms = get_terms('related_content');
                    foreach ($terms as $term) {
                      echo '<option value="' . $term->slug . '"' . ($cs_related_tax==$term->slug ? 'selected' : '') . '>' . $term->name . ' (' . $term->count . ')</option>';
                    } ?>
                  </select>
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li>
                <label for="cs-cta-heading">Heading</label>
                <input id="cs-cta-heading" name="cs-cta-heading" type="text" value="<?= $cs_cta_heading; ?>">
              </li>
              <li>
                <label for="cs-cta-text">Text</label>
                <textarea id="cs-cta-text" name="cs-cta-text" class="text-editor"><?= $cs_cta_text; ?></textarea>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="cs-cta-btn-text">Button Text</label>
                  <input id="cs-cta-btn-text" name="cs-cta-btn-text" type="text" value="<?= $cs_cta_btn_text; ?>">
                </div>
                <div class="col-xs-6">
                  <label for="cs-cta-btn-link">Button Link</label>
                  <input id="cs-cta-btn-link" name="cs-cta-btn-link" type="text" value="<?= $cs_cta_btn_link; ?>">
                </div>
              </li>
              <li>
                <label for="cs-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#cs-cta-bg-img">Select Image</button>
                  <input id="cs-cta-bg-img" name="cs-cta-bg-img" class="flex-1" type="text" value="<?= $cs_cta_bg_img; ?>">
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
<?php
}

// Create meta box
function _ws_customer_story_meta() {
  add_meta_box('cs_meta', 'Customer Story', '_ws_customer_story_meta_fields', 'customer_story', 'normal', 'high');
}
add_action('admin_init', '_ws_customer_story_meta');

// Save meta values
function _ws_save_customer_story_meta($post_id) {
  if (!isset($_POST['cs-nonce']) || !wp_verify_nonce($_POST['cs-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $cs_logo = isset($_POST['cs-logo']) ? $_POST['cs-logo'] : '';
  update_post_meta($post_id, '_cs-logo', $cs_logo);

  $cs_kpi_1 = isset($_POST['cs-kpi-1']) ? $_POST['cs-kpi-1'] : '';
  update_post_meta($post_id, '_cs-kpi-1', $cs_kpi_1);

  $cs_kpi_2 = isset($_POST['cs-kpi-2']) ? $_POST['cs-kpi-2'] : '';
  update_post_meta($post_id, '_cs-kpi-2', $cs_kpi_2);

  $cs_kpi_3 = isset($_POST['cs-kpi-3']) ? $_POST['cs-kpi-3'] : '';
  update_post_meta($post_id, '_cs-kpi-3', $cs_kpi_3);

  $cs_client_name = isset($_POST['cs-client-name']) ? $_POST['cs-client-name'] : '';
  update_post_meta($post_id, '_cs-client-name', $cs_client_name);

  $cs_related_products = isset($_POST['cs-related-products']) ? $_POST['cs-related-products'] : '';
  update_post_meta($post_id, '_cs-related-products', $cs_related_products);

  $cs_testimonial = isset($_POST['cs-testimonial']) ? $_POST['cs-testimonial'] : '';
  update_post_meta($post_id, '_cs-testimonial', $cs_testimonial);

  $cs_bottom_text = isset($_POST['cs-bottom-text']) ? $_POST['cs-bottom-text'] : '';
  update_post_meta($post_id, '_cs-bottom-text', $cs_bottom_text);

  $cs_related_heading = isset($_POST['cs-related-heading']) ? $_POST['cs-related-heading'] : '';
  update_post_meta($post_id, '_cs-related-heading', $cs_related_heading);

  $cs_related_type = isset($_POST['cs-related-type']) ? $_POST['cs-related-type'] : '';
  update_post_meta($post_id, '_cs-related-type', $cs_related_type);

  $cs_related_post_one = isset($_POST['cs-related-post-one']) ? $_POST['cs-related-post-one'] : '';
  update_post_meta($post_id, '_cs-related-post-one', $cs_related_post_one);

  $cs_related_post_two = isset($_POST['cs-related-post-two']) ? $_POST['cs-related-post-two'] : '';
  update_post_meta($post_id, '_cs-related-post-two', $cs_related_post_two);

  $cs_related_post_three = isset($_POST['cs-related-post-three']) ? $_POST['cs-related-post-three'] : '';
  update_post_meta($post_id, '_cs-related-post-three', $cs_related_post_three);

  $cs_related_post_type = isset($_POST['cs-related-post-type']) ? $_POST['cs-related-post-type'] : '';
  update_post_meta($post_id, '_cs-related-post-type', $cs_related_post_type);

  $cs_related_tax = isset($_POST['cs-related-tax']) ? $_POST['cs-related-tax'] : '';
  update_post_meta($post_id, '_cs-related-tax', $cs_related_tax);

  $cs_cta_heading = isset($_POST['cs-cta-heading']) ? $_POST['cs-cta-heading'] : '';
  update_post_meta($post_id, '_cs-cta-heading', $cs_cta_heading);

  $cs_cta_text = isset($_POST['cs-cta-text']) ? $_POST['cs-cta-text'] : '';
  update_post_meta($post_id, '_cs-cta-text', $cs_cta_text);

  $cs_cta_btn_text = isset($_POST['cs-cta-btn-text']) ? $_POST['cs-cta-btn-text'] : '';
  update_post_meta($post_id, '_cs-cta-btn-text', $cs_cta_btn_text);

  $cs_cta_btn_link = isset($_POST['cs-cta-btn-link']) ? $_POST['cs-cta-btn-link'] : '';
  update_post_meta($post_id, '_cs-cta-btn-link', $cs_cta_btn_link);

  $cs_cta_bg_img = isset($_POST['cs-cta-bg-img']) ? $_POST['cs-cta-bg-img'] : '';
  update_post_meta($post_id, '_cs-cta-bg-img', $cs_cta_bg_img);
}
add_action('save_post', '_ws_save_customer_story_meta');
