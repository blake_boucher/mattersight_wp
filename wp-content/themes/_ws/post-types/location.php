<?php
// Register location post type
function _ws_location_post_type() {
  $labels = array(
    'name' => 'Locations',
    'singular_name' => 'Location',
    'add_new_item' => 'Add New Location',
    'edit_item' => 'Edit Location',
    'new_item' => 'New Location',
    'view_item' => 'View Location',
    'search_items' => 'Search Locations',
    'not_found' => 'No locations found',
    'not_found_in_trash' => 'No locations found in Trash',
    'parent_item_colon' => 'Parent Location:',
    'all_items' => 'All Locations',
    'archives' => 'Location Archives',
    'insert_into_item' => 'Insert into location',
    'uploaded_to_this_item' => 'Uploaded to this location',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable locations',
    'public' => false,
    'exclude_from_search' => true,
    'publicly_queryable' => false,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-location',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'locations'),
    'query_var' => true
  );
  register_post_type('location', $args);
}
add_action('init', '_ws_location_post_type');

// Fill meta box
function _ws_location_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'location-nonce');
  $contact = get_post_meta(get_the_ID(), '_contact', true);
  $location = get_post_meta(get_the_ID(), '_location', true); ?>
  <div id="location-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="contact">Contact Info</label>
          <textarea id="contact" name="contact" class="text-editor"><?= $contact; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="location">Address</label>
          <textarea id="location" name="location" class="text-editor"><?= $location; ?></textarea>
          <iframe id="location-map" width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyB5dOtdhz53nCEusX4aU4yRKkOGns_Dsn8&q=<?= str_replace(' ', '+', strip_tags($location)); ?>" allowfullscreen></iframe>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_location_meta() {
  add_meta_box('location_meta', 'Location', '_ws_location_meta_fields', 'location', 'normal', 'high');
}
add_action('admin_init', '_ws_location_meta');

// Save meta values
function _ws_save_location_meta($post_id) {
  if (!isset($_POST['location-nonce']) || !wp_verify_nonce($_POST['location-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $contact = isset($_POST['contact']) ? $_POST['contact'] : '';
  update_post_meta($post_id, '_contact', $contact);

  $location = isset($_POST['location']) ? $_POST['location'] : '';
  update_post_meta($post_id, '_location', $location);
}
add_action('save_post', '_ws_save_location_meta');
