<?php
// Register investor filing post type
function _ws_investor_filing_post_type() {
  $labels = array(
    'name' => 'Investor Filings',
    'singular_name' => 'Investor Filing',
    'add_new_item' => 'Add New Investor Filing',
    'edit_item' => 'Edit Investor Filing',
    'new_item' => 'New Investor Filing',
    'view_item' => 'View Investor Filing',
    'search_items' => 'Search Investor Filings',
    'not_found' => 'No investor filings found',
    'not_found_in_trash' => 'No investor filings found in Trash',
    'parent_item_colon' => 'Parent Investor Filing:',
    'all_items' => 'All Investor Filings',
    'archives' => 'Investor Filing Archives',
    'insert_into_item' => 'Insert into investor filing',
    'uploaded_to_this_item' => 'Uploaded to this investor filing',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable investor filings',
    'public' => true,
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => false,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-dismiss',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'about/investors/financials', 'with_front'=>false),
    'query_var' => true
  );
  register_post_type('investor_filing', $args);
}
add_action('init', '_ws_investor_filing_post_type');

function _ws_activate_scheduled_filing_check() {
  if (!wp_next_scheduled('_ws_daily_filing_check')) {
    wp_schedule_event(time(), 'twicedaily', '_ws_daily_filing_check');
  }
}
add_action('after_setup_theme', '_ws_activate_scheduled_filing_check');

function _ws_external_filings() {
  checkFilingPage("https://clientapi.gcs-web.com/data/2c370818-342c-4412-933e-1ec15abb5fa0/filings");
}
add_action('_ws_daily_filing_check', '_ws_external_filings');

function _ws_deactivate_scheduled_filing_check() {
  wp_clear_scheduled_hook('_ws_daily_filing_check');
}
add_action('switch_theme', '_ws_deactivate_scheduled_filing_check');

function checkFilingPage($url) {
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  $data = curl_exec($curl);
  curl_close($curl);
  $data = json_decode($data);
  $dupes = 0;
  foreach ($data->data as $i=>$filing) {
    $n = get_posts(array('post_type'=>'investor_filing', 'post_status'=>'publish', 'meta_key'=>'_investor-filing-id', 'meta_value'=>$filing->accessionNo));
    if ($n) {
      $dupes++;
      error_log('duplicate new:');
      error_log('_investor-filing-id: ' . $filing->accessionNo);
      error_log('_investor-filing-page: ' . intval(explode('=', $data->links->self)[1]));
      error_log('duplicate old:');
      // error_log(print_r($n[0], true));
      error_log('_investor-filing-id: ' . get_post_meta($n[0]->ID, '_investor-filing-id', true));
      error_log('_investor-filing-page: ' . get_post_meta($n[0]->ID, '_investor-filing-page', true));
      continue;
    }
    if ($dupes >= 5) {
      break;
    }
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $filing->link->url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $thisFiling = curl_exec($curl);
    curl_close($curl);
    $thisFiling = json_decode($thisFiling);
    $content = '<p>Filing Date: ' . date('F j, Y', strtotime($thisFiling->data->dateFiled->dateUTC)) . ' <br />Document Date: ' . date('F j, Y', strtotime($thisFiling->data->documentDate->dateUTC)) . ' <br />Form Description: ' . $thisFiling->data->description . '</p><p><b>Formats:</b></p><ul>';
    if ($html = $thisFiling->data->formats->html) {
      $content .= '<li><a href="' . $html->url . '" target="_blank">HTML</a></li>';
    }
    if ($rtf = $thisFiling->data->formats->rtf) {
      $content .= '<li><a href="' . $rtf->url . '" target="_blank">DOC</a></li>';
    }
    if ($pdf = $thisFiling->data->formats->pdf) {
      $content .= '<li><a href="' . $pdf->url . '" target="_blank">PDF</a></li>';
    }
    if ($xls = $thisFiling->data->formats->xls) {
      $content .= '<li><a href="' . $xls->url . '" target="_blank">XLS</a></li>';
    }
    $content .= '</ul>';
    wp_insert_post(array(
      'post_date' => $thisFiling->data->dateFiled->dateUTC,
      'post_content' => $content,
      'post_title' => $filing->formType,
      'post_status' => 'publish',
      'post_type' => 'investor_filing',
      'meta_input' => array(
        '_investor-filing-id' => $filing->accessionNo,
        '_investor-filing-page' => intval(explode('=', $data->links->self)[1])
      )
    ));
    if ($i + 1 >= $data->meta->count && $data->links->next) {
      checkFilingPage($data->links->next);
    }
  }
}

function _ws_posts_where_title_array($where, $wp_query) {
  global $wpdb;
  if ($post_title__in = $wp_query->get('post_title__in')) {
    $where .= ' and (';
    foreach ($post_title__in as $i=>$title) {
      $where .= $wpdb->posts . '.post_title = "' . esc_sql($wpdb->esc_like($title)) . '"';
      $where .= $i < count($post_title__in)-1 ? ' OR ' : '';
    }
    $where .= ')';
  }
  return $where;
}
add_filter('posts_where', '_ws_posts_where_title_array', 10, 2);

function _ws_posts_where_title_not_array($where, $wp_query) {
  global $wpdb;
  if ($post_title__not_in = $wp_query->get('post_title__not_in')) {
    $where .= ' and (';
    foreach ($post_title__not_in as $i=>$title) {
      $where .= $wpdb->posts . '.post_title != "' . esc_sql($wpdb->esc_like($title)) . '"';
      $where .= $i < count($post_title__not_in)-1 ? ' and ' : '';
    }
    $where .= ')';
  }
  return $where;
}
add_filter('posts_where', '_ws_posts_where_title_not_array', 10, 2);

// Add start date column
function _ws_filing_columns($columns) {
  $columns['page'] = 'Page';
  $columns['id'] = 'ID';
  return $columns;
}
add_filter('manage_edit-investor_filing_columns', '_ws_filing_columns');

// Column content
function _ws_filing_column_content($column_name, $post_id) {
  if ($column_name == 'page') {
    $o = get_post_meta($post_id, '_investor-filing-page', true);
  }
  else if ($column_name == 'id') {
    $o = get_post_meta($post_id, '_investor-filing-id', true);
  }
  else {
    return;
  }
  echo $o;
}
add_action('manage_investor_filing_posts_custom_column', '_ws_filing_column_content', 10, 2);

// Make column sortable
function _ws_sortable_filing_column($columns) {
  $columns['page'] = '_investor-filing-page';
  // $columns['id'] = '_investor-filing-id';
  return $columns;
}
add_filter('manage_edit-investor_filing_sortable_columns', '_ws_sortable_filing_column');

// Sort column by date and then time
function _ws_orderby_filing($query) {
  if (!is_admin()) {
    return;
  }
  $orderby = $query->get('orderby');
  if ($orderby=='_investor-filing-page') {
    $query->set('meta_key', '_investor-filing-page');
    $query->set('orderby', 'meta_value_num');
  }
}
add_action('pre_get_posts', '_ws_orderby_filing');
