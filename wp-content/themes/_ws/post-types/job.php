<?php
// Register job post type
function _ws_job_post_type() {
  $labels = array(
    'name' => 'Jobs',
    'singular_name' => 'Job',
    'add_new_item' => 'Add New Job',
    'edit_item' => 'Edit Job',
    'new_item' => 'New Job',
    'view_item' => 'View Job',
    'search_items' => 'Search Jobs',
    'not_found' => 'No jobs found',
    'not_found_in_trash' => 'No jobs found in Trash',
    'parent_item_colon' => 'Parent Job:',
    'all_items' => 'All Jobs',
    'archives' => 'Job Archives',
    'insert_into_item' => 'Insert into job',
    'uploaded_to_this_item' => 'Uploaded to this job',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
  );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable jobs',
    'public' => false,
    'exclude_from_search' => true,
    'publicly_queryable' => false,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-megaphone',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'jobs', 'with_front'=>false),
    'query_var' => true
  );
  register_post_type('job', $args);
}
add_action('init', '_ws_job_post_type');

// Fill meta box
function _ws_job_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'job-nonce');
  $job_field = get_post_meta(get_the_ID(), '_job-field', true);
  $job_location = get_post_meta(get_the_ID(), '_job-location', true);
  $job_link = get_post_meta(get_the_ID(), '_job-link', true); ?>
  <div id="job-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-6">
          <label for="job-field">Field</label>
          <input id="job-field" name="job-field" type="text" value="<?= $job_field; ?>" />
        </div>
        <div class="col-xs-6">
          <label for="job-location">Location</label>
          <input id="job-location" name="job-location" type="text" value="<?= $job_location; ?>" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="job-link">Link</label>
          <input id="job-link" name="job-link" type="text" value="<?= $job_link; ?>" />
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_job_meta() {
  add_meta_box('job_meta', 'Job', '_ws_job_meta_fields', 'job', 'normal', 'high');
}
add_action('admin_init', '_ws_job_meta');

// Save meta values
function _ws_save_job_meta($post_id) {
  if (!isset($_POST['job-nonce']) || !wp_verify_nonce($_POST['job-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $job_field = isset($_POST['job-field']) ? $_POST['job-field'] : '';
  update_post_meta($post_id, '_job-field', $job_field);

  $job_location = isset($_POST['job-location']) ? $_POST['job-location'] : '';
  update_post_meta($post_id, '_job-location', $job_location);

  $job_link = isset($_POST['job-link']) ? $_POST['job-link'] : '';
  update_post_meta($post_id, '_job-link', $job_link);
}
add_action('save_post', '_ws_save_job_meta');
