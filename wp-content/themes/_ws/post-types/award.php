<?php
// Register award post type
function _ws_award_post_type() {
  $labels = array(
    'name' => 'Awards',
    'singular_name' => 'Award',
    'add_new_item' => 'Add New Award',
    'edit_item' => 'Edit Award',
    'new_item' => 'New Award',
    'view_item' => 'View Award',
    'search_items' => 'Search Awards',
    'not_found' => 'No awards found',
    'not_found_in_trash' => 'No awards found in Trash',
    'parent_item_colon' => 'Parent Award:',
    'all_items' => 'All Awards',
    'archives' => 'Award Archives',
    'insert_into_item' => 'Insert into award',
    'uploaded_to_this_item' => 'Uploaded to this award',
    'featured_image' => 'Featured image',
    'set_featured_image' => 'Set featured image',
    'remove_featured_image' => 'Remove featured image',
    'use_featured_image' => 'Use as featured image'
    );
  $args = array(
    'labels' => $labels,
    'description' => 'Sortable/filterable awards',
    'public' => false,
    'exclude_from_search' => true,
    'publicly_queryable' => false,
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'show_in_menu' => true,
    'show_in_admin_bar' => true,
    'menu_position' => 20,
    'menu_icon' => 'dashicons-awards',
    'capability_type' => 'post',
    'hierarchical' => false,
    'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions'),
    'register_meta_box_cb' => null,
    'taxonomies' => array(),
    'has_archive' => false,
    'rewrite' => array('slug'=>'awards', 'with_front'=>false),
    'query_var' => true
    );
  register_post_type('award', $args);
}
add_action('init', '_ws_award_post_type');
