<?php
/* Template Name: Investors Governance */

get_header(); ?>

<main id="main" template="investors-governance">
  <?php
  get_template_part('template-parts/banner', 'split');
  wp_nav_menu(array(
    'theme_location' => 'investors',
    'container' => 'nav',
    'container_class' => 'investors-menu',
    'item_spacing' => 'discard'
  ));
  $loop = new WP_Query($args = array('post_type'=>'person', 'post_status'=>'publish', 'posts_per_page'=>-1));
  if ($loop->have_posts()) : ?>
    <section class="posts">
      <div class="container row">
        <div class="col-xs-12 no-padding">
          <h2>Leadership</h2>
        </div>
        <?php
        while ($loop->have_posts()) : $loop->the_post();
          get_template_part('template-parts/archive', get_post_type());
        endwhile; ?>
      </div>
    </section>
  <?php
  endif; ?>
  <section>
    <div class="container row">
      <div class="col-xl-8 col-lg-10 wp-text">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif; ?>
      </div>
    </div>
  </section>
  <?php
  $cta_heading = get_post_meta(get_the_ID(), '_ig-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_ig-cta-text', true);
  $cta_form = esc_html(get_post_meta(get_the_ID(), '_ig-cta-form', true));
  $cta_bg_img = get_post_meta(get_the_ID(), '_ig-cta-bg-img', true);
  echo do_shortcode('[cta_form ' . ($cta_bg_img ? 'class="overlay"' : '') . ' heading="' . $cta_heading . '" form="' . $cta_form . '" bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta_form]'); ?>
</main>

<?php get_footer(); ?>
