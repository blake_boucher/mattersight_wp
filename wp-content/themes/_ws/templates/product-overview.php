<?php
/* Template Name: Product/Solution Overview */

get_header(); ?>

<main id="main" template="product-overview">
  <?php
  get_template_part('template-parts/banner', 'gray');
  $kpis = get_post_meta(get_the_ID(), '_po-kpis', true) ?: array();
  $o = '';
  foreach ($kpis as $i=>$kpi) {
    $o .= 'svg_' . ($i+1) . '="' . (isset($kpi['svg']) ? $kpi['svg'] : '') . '" title_' . ($i+1) . '="' . (isset($kpi['title']) ? $kpi['title'] : '') . '" text_' . ($i+1) . '="' . (isset($kpi['text']) ? $kpi['text'] : '') . '" ';
  }
  echo do_shortcode('[kpis class="po-kpis" ' . $o . ']');
  echo do_shortcode('[overview_list class="po-products" heading="' . get_post_meta(get_the_ID(), '_po-list-heading', true) . '" p_id="' . get_the_ID() . '"]');
  if ($r_id = get_post_meta(get_the_ID(), '_po-featured-id', true)) {
    echo do_shortcode('[featured_resource heading="' . get_post_meta(get_the_ID(), '_po-featured-heading', true) . '" r_id="' . $r_id .'"]');
  }
  if ($t_id = get_post_meta(get_the_ID(), '_po-testimonial', true)) {
    echo do_shortcode('[testimonial t_id="' . $t_id . '"]');
  }
  echo do_shortcode('[clients class="po-clients"]');
  $cta_heading = get_post_meta(get_the_ID(), '_po-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_po-cta-text', true);
  $cta_btn_text = get_post_meta(get_the_ID(), '_po-cta-btn-text', true);
  $cta_btn_link = get_post_meta(get_the_ID(), '_po-cta-btn-link', true);
  $cta_bg_img = get_post_meta(get_the_ID(), '_po-cta-bg-img', true);
  echo do_shortcode('[cta heading="' . $cta_heading . '" btn_text="' . $cta_btn_text . '" btn_link="' . $cta_btn_link . '" ' . ($cta_bg_img ? 'class="overlay"' : 'class="white-btn"') . ' bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta]'); ?>
</main>

<?php get_footer(); ?>
