<?php
/* Template Name: Investors Overview */

get_header(); ?>

<main id="main" template="investors-overview">
  <?php
  get_template_part('template-parts/banner', 'split');
  wp_nav_menu(array(
    'theme_location' => 'investors',
    'container' => 'nav',
    'container_class' => 'investors-menu',
    'item_spacing' => 'discard'
  )); ?>
  <section class="io-overview-text">
    <div class="container row">
      <div class="col-xl-7 col-lg-8 wp-text">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif; ?>
      </div>
      <div class="col-xl-4 col-xl-offset-1 col-lg-4 io-right">
        <div id="stock-kpi">
          <?php
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, 'https://clientapi.gcs-web.com/data/2c370818-342c-4412-933e-1ec15abb5fa0/Quotes');
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          $data = curl_exec($curl);
          curl_close($curl);
          $data = json_decode($data)->data[0]; ?>
          <h3><?= $data->exchange; ?>: <?= $data->symbol; ?></h3>
          <p><b><?= number_format($data->lastTrade, 2); ?></b><small> USD</small> <?= $data->changeNumber >= 0 ? '+' . $data->changeNumber : $data->changeNumber; ?> (<?= $data->changePercent; ?>%)</p>
        </div>
        <?php
        $files = get_post_meta(get_the_ID(), '_io-files', true);
        foreach ($files as $file) {
          echo do_shortcode('<a class="investor-file" href="' . $file['link'] . '" target="_blank"><span>' . $file['text'] . '</span>[svg id="external"]</a>');
        } ?>
      </div>
    </div>
  </section>
  <?php
  $cta_heading = get_post_meta(get_the_ID(), '_io-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_io-cta-text', true);
  $cta_form = esc_html(get_post_meta(get_the_ID(), '_io-cta-form', true));
  $cta_bg_img = get_post_meta(get_the_ID(), '_io-cta-bg-img', true);
  echo do_shortcode('[cta_form ' . ($cta_bg_img ? 'class="overlay"' : '') . ' heading="' . $cta_heading . '" form="' . $cta_form . '" bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta_form]'); ?>
</main>

<?php get_footer(); ?>
