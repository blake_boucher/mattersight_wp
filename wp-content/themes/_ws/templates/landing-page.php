<?php
/* Template Name: Landing Page */

get_header(); ?>

<main id="main" template="landing-page">
  <header class="page-header lp-header" style="<?= _ws_thumbnail_background(); ?>">
    <div class="container row">
      <div class="col-xl-7 col-lg-7 col-lg-offset-0">
        <?php
        $tag = get_post_meta(get_the_ID(), '_banner-headline-type', true) ?: 'h1';
        echo '<' . $tag . ' class="page-title">' . (get_post_meta(get_the_ID(), '_banner-headline', true) ?: get_the_title()) . '</' . $tag . '>';
        if ($subheadline = get_post_meta(get_the_ID(), '_banner-subheadline', true)) {
          if (substr($subheadline, 0, 1) == '<') {
            echo '<div class="page-subtitle">' . $subheadline . '</div>';
          }
          else {
            echo '<p class="page-subtitle">' . $subheadline . '</p>';
          }
        } ?>
      </div>
      <div class="col-xl-4 col-xl-offset-1 col-lg-5 col-lg-offset-0 col-md-7 col-sm-9">
        <div class="form-card-container">
          <div class="form-card">
            <?php
            if ($img = get_post_meta(get_the_ID(), '_lp-resource-img', true)) : ?>
              <div class="resource-img">
                <img src="<?= $img; ?>" alt="<?= get_the_title(); ?>" />
              </div>
            <?php
            endif; ?>
            <div class="form-card-content">
              <?= get_post_meta(get_the_ID(), '_lp-card', true); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <section>
    <div class="container row">
      <div class="col-xl-7 col-lg-7 col-sm-12 wp-text">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif; ?>
      </div>
    </div>
  </section>
</main>

<?php get_footer(); ?>
