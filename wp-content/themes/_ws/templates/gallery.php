<?php
/* Template Name: Gallery */

get_header(); ?>

<main id="main" template="gallery">
  <?php
  $s = isset($_GET['search']) ? $_GET['search'] : '';
  $filters = isset($_GET['filters']) ? $_GET['filters'] : array();
  $p_type = get_post_meta(get_the_ID(), '_gallery-type', true);
  get_template_part('template-parts/banner', 'split'); ?>
  <div id="anchor"></div>
  <div class="filters">
    <form class="container row" action="" method="get">
      <div class="col-xl-3 col-lg-4 col-md-6">
        <?php
        if ($tax = get_post_meta(get_the_ID(), '_gallery-tax', true)) : ?>
          <div class="input-group">
            <button type="button" class="toggle" target="#filter-options" aria-label="Show/Hide Filters">
              <div class="toggle-flex">
                <span>filter</span>
                <?= do_shortcode('[svg id="plus"]'); ?>
              </div>
            </button>
            <div id="filter-options">
              <?php
              if ($tax == 'year') : ?>
                <fieldset>
                  <legend>Year</legend>
                  <ul>
                    <?php
                    $years = wp_get_archives(array('type'=>'yearly', 'format'=>'custom', 'echo'=>0, 'post_type'=>$p_type));
                    $years = explode('</a>', $years);
                    array_pop($years);
                    foreach ($years as $i=>$year) :
                      $year = substr($year, -4); ?>
                      <li>
                        <input id="filter-<?= $i; ?>" name="filters[]" type="checkbox" value="<?= $year; ?>" <?= in_array($year, $filters) ? 'checked' : ''; ?> />
                        <label for="filter-<?= $i; ?>"><?= $year; ?></label>
                      </li>
                    <?php
                    endforeach; ?>
                  </ul>
                </fieldset>
              <?php
              elseif ($tax == 'event') : ?>
                <fieldset>
                  <legend>Event Timeline</legend>
                  <ul>
                    <li>
                      <input id="filter-0" name="filters[]" type="checkbox" value="upcoming" <?= in_array('upcoming', $filters) ? 'checked' : ''; ?> />
                      <label for="filter-0">Upcoming</label>
                    </li>
                    <li>
                      <input id="filter-1" name="filters[]" type="checkbox" value="past" <?= in_array('past', $filters) ? 'checked' : ''; ?> />
                      <label for="filter-1">Past</label>
                    </li>
                  </ul>
                </fieldset>
              <?php
              else:
                $terms = get_terms(array('taxonomy' => $tax)); ?>
                <fieldset>
                  <legend><?= get_taxonomy($tax)->label; ?></legend>
                  <ul>
                    <?php
                    foreach ($terms as $i=>$term) : ?>
                      <li>
                        <input id="filter-<?= $i; ?>" name="filters[]" type="checkbox" value="<?= $term->slug; ?>" <?= in_array($term->slug, $filters) ? 'checked' : ''; ?> />
                        <label for="filter-<?= $i; ?>"><?= $term->name; ?></label>
                      </li>
                    <?php
                    endforeach; ?>
                  </ul>
                </fieldset>
              <?php
              endif; ?>
              <div>
                <button type="submit" class="arrow">Apply Filters</button>
              </div>
            </div>
          </div>
        <?php
        endif; ?>
      </div>
      <div class="col-xl-3 col-xl-offset-6 col-lg-4 col-lg-offset-4 col-md-6">
        <div class="input-group">
          <label class="screen-reader-text" for="search">Search</label>
          <input id="search" name="search" type="text" value="<?= $s; ?>" placeholder="search" />
          <button class="search-icon" type="submit" aria-label="Apply Filters"><?= do_shortcode('[svg id="search"]'); ?></button>
        </div>
      </div>
    </form>
  </div>
  <section class="posts">
    <?php
    if ($filters) : ?>
      <div class="container row">
        <div class="col-xs-12 remove-filters">
          <?php
          foreach ($filters as $filter) {
            if ($tax == 'year') {
              echo do_shortcode('<button data-filter="' . $filter . '" aria-label="Remove Filter"><span>' . $filter . '</span>[svg id="close"]</button>');
            }
            else if ($tax == 'event') {
              if ($filter == 'upcoming') {
                echo do_shortcode('<button data-filter="' . $filter . '" aria-label="Remove Filter"><span>Upcoming</span>[svg id="close"]</button>');
              }
              if ($filter == 'past') {
                echo do_shortcode('<button data-filter="' . $filter . '" aria-label="Remove Filter"><span>Past</span>[svg id="close"]</button>');
              }
            }
            else {
              echo do_shortcode('<button data-filter="' . $filter . '" aria-label="Remove Filter"><span>' . get_term_by('slug', $filter, $tax)->name . '</span>[svg id="close"]</button>');
            }
          } ?>
        </div>
      </div>
    <?php
    endif;
    $args = array(
      'post_type' => $p_type,
      'post_status' => 'publish',
      'paged' => $paged,
      's' => $s
    );
    if ($filters) {
      if ($tax == 'year') {
        $args['date_query'] = array(
          array(
            'year' => $filters,
            'compare' => 'IN'
          )
        );
      }
      else if ($tax == 'event') {
        $args['meta_query'] = array(
          'relation' => 'OR'
        );
        if (in_array('upcoming', $filters)) {
          array_push($args['meta_query'],
            array(
              'key' => '_event-sortable-start',
              'value' => date('YmdHi'),
              'compare' => '>='
            )
          );
        }
        if (in_array('past', $filters)) {
          array_push($args['meta_query'],
            array(
              'key' => '_event-sortable-start',
              'value' => date('YmdHi'),
              'compare' => '<'
            )
          );
        }
      }
      else {
        $args['tax_query'] = array(
          array(
            'taxonomy' => $tax,
            'field' => 'slug',
            'terms' => $filters
          )
        );
      }
    }
    if ($p_type == 'event') {
      $args['orderby'] = 'meta_value_num';
      $args['meta_key'] = '_event-sortable-start';
    }
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $loop = new WP_Query($args);
    if ($loop->have_posts()) : ?>
      <div class="container row <?= $loop->max_num_pages > 1 ? 'infinite-scroll-btn' : ''; ?>">
        <input class="loop-var" type="hidden" value="<?= htmlentities(json_encode($loop)); ?>" />
        <?php
        while ($loop->have_posts()) : $loop->the_post();
          get_template_part('template-parts/archive', get_post_type());
        endwhile; ?>
      </div>
    <?php
    else: ?>
      <div class="container row">
        <div class="col-xs-12 center">
          <p>No results found.</p>
        </div>
      </div>
    <?php
    endif; wp_reset_postdata(); ?>
  </section>
</main>

<?php get_footer(); ?>
