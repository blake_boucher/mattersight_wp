<?php
/* Template Name: Investors Financial */

get_header(); ?>

<main id="main" template="investors-financial">
  <?php
  get_template_part('template-parts/banner', 'split');
  wp_nav_menu(array(
    'theme_location' => 'investors',
    'container' => 'nav',
    'container_class' => 'investors-menu',
    'item_spacing' => 'discard'
  )); ?>
  <section>
    <div class="container row">
      <div class="col-xl-7 col-lg-6 col-md-5">
        <h2>Filings</h2>
      </div>
      <div class="col-xl-5 col-lg-6 col-md-7">
        <form class="investor-filter" action="" method="get">
          <?php
          $y = isset($_GET['y']) ? $_GET['y'] : '';
          $t = isset($_GET['t']) ? $_GET['t'] : '';
          $years = wp_get_archives(array('type'=>'yearly', 'format'=>'custom', 'echo' => 0, 'post_type'=>'investor_filing'));
          $years = explode('</a>', $years);
          array_pop($years); ?>
          <label class="screen-reader-text" for="type">Type</label>
          <select id="type" name="t">
            <option value="">select type</option>
            <option value="annual" <?= $t=='annual' ? 'selected' : ''; ?>>annual filings</option>
            <option value="quarterly" <?= $t=='quarterly' ? 'selected' : ''; ?>>quarterly filings</option>
            <option value="current" <?= $t=='current' ? 'selected' : ''; ?>>current reports</option>
            <option value="proxy" <?= $t=='proxy' ? 'selected' : ''; ?>>proxy filings</option>
            <option value="section" <?= $t=='section' ? 'selected' : ''; ?>>section 16 filings</option>
            <option value="other" <?= $t=='other' ? 'selected' : ''; ?>>other filings</option>
          </select>
          <label class="screen-reader-text" for="year">Year</label>
          <select id="year" name="y">
            <option value="">select year</option>
            <?php
            foreach ($years as $year) {
              $year = substr($year, -4);
              echo '<option value="' . $year . '" ' . ($year == $y ? 'selected' : '') . '>' . $year . '</option>';
            } ?>
          </select>
        </form>
      </div>
    </div>
    <?php
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $args = array('post_type'=>'investor_filing', 'post_status'=>'publish', 'paged'=>$paged);
    if ($y) {
      $args['date_query'] = array(
        array(
          'year' => $y
        )
      );
    }
    if ($t == 'annual') {
      $args['post_title__in'] = array('10-K', '10-K/A');
    }
    else if ($t == 'quarterly') {
      $args['post_title__in'] = array('10-Q');
    }
    else if ($t == 'current') {
      $args['post_title__in'] = array('8-K', '8-K/A');
    }
    else if ($t == 'proxy') {
      $args['post_title__in'] = array('DEF 14A', 'DEFA14A', 'DEFS14A', 'PREM14A', 'PRES14A');
    }
    else if ($t == 'section') {
      $args['post_title__in'] = array('3', '3/A', '4', '4/A', '5', '5/A');
    }
    else if ($t == 'other') {
      $args['post_title__not_in'] = array('10-K', '10-K/A', '10-Q', '8-K', '8-K/A', 'DEF 14A', 'DEFA14A', 'DEFS14A', 'PREM14A', 'PRES14A', '3', '3/A', '4', '4/A', '5', '5/A');
    }
    $loop = new WP_Query($args);
    if ($loop->have_posts()) : ?>
      <div class="container row <?= $loop->max_num_pages > 1 ? 'infinite-scroll-btn' : ''; ?>">
        <input class="loop-var" type="hidden" value="<?= htmlentities(json_encode($loop)); ?>" />
        <?php
        while ($loop->have_posts()) : $loop->the_post();
          get_template_part('template-parts/archive', get_post_type());
        endwhile; ?>
      </div>
    <?php
    else: ?>
      <div class="container row">
        <div class="col-xs-12 center">
          <p>No results found.</p>
        </div>
      </div>
    <?php
    endif; wp_reset_postdata(); ?>
  </section>
  <?php
  $cta_heading = get_post_meta(get_the_ID(), '_if-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_if-cta-text', true);
  $cta_form = esc_html(get_post_meta(get_the_ID(), '_if-cta-form', true));
  $cta_bg_img = get_post_meta(get_the_ID(), '_if-cta-bg-img', true);
  echo do_shortcode('[cta_form ' . ($cta_bg_img ? 'class="overlay"' : '') . ' heading="' . $cta_heading . '" form="' . $cta_form . '" bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta_form]'); ?>
</main>

<?php get_footer(); ?>
