<?php
/* Template Name: Investors Contact */

get_header(); ?>

<main id="main" template="investors-contact">
  <?php
  get_template_part('template-parts/banner', 'split');
  wp_nav_menu(array(
    'theme_location' => 'investors',
    'container' => 'nav',
    'container_class' => 'investors-menu',
    'item_spacing' => 'discard'
  )); ?>
  <section class="ic-text">
    <div class="container row">
      <div class="col-lg-6 wp-text">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif; ?>
      </div>
      <div class="col-lg-6">
        <?= get_post_meta(get_the_ID(), '_ic-quote', true); ?>
      </div>
    </div>
  </section>
  <?php
  $cta_heading = get_post_meta(get_the_ID(), '_ic-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_ic-cta-text', true);
  $cta_form = esc_html(get_post_meta(get_the_ID(), '_ic-cta-form', true));
  $cta_bg_img = get_post_meta(get_the_ID(), '_ic-cta-bg-img', true);
  echo do_shortcode('[cta_form  ' . ($cta_bg_img ? 'class="overlay"' : '') . ' heading="' . $cta_heading . '" form="' . $cta_form . '" bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta_form]'); ?>
</main>

<?php get_footer(); ?>
