<?php
/* Template Name: Personality Labs */

get_header(); ?>

<main id="main" template="personality-labs">
  <header class="page-header">
    <div class="container row">
      <div class="col-lg-8 col-md-12">
        <?php
        // Headline
        $tag = get_post_meta(get_the_ID(), '_banner-headline-type', true) ?: 'h1';
        if ($headline = get_post_meta(get_the_ID(), '_banner-headline', true)) {
          echo '<' . $tag . ' class="page-title">' . $headline . '</' . $tag . '>';
        }
        // Subheadline
        if ($subheadline = get_post_meta(get_the_ID(), '_banner-subheadline', true)) {
          if (substr($subheadline, 0, 1) == '<') {
            echo '<div class="page-subtitle">' . $subheadline . '</div>';
          }
          else {
            echo '<p class="page-subtitle">' . $subheadline . '</p>';
          }
        } ?>
      </div>
    </div>
  </header>
  <section class="pl-header">
    <div class="container row">
      <div class="col-xl-6 col-lg-7 col-lg-offset-0 col-md-10 col-md-offset-1">
        <?php
        if (have_posts()) : while (have_posts()) : the_post();
          the_content();
        endwhile; endif; ?>
      </div>
      <div class="col-xl-5 col-xl-offset-1 col-lg-5 col-lg-offset-0 col-md-10 col-md-offset-1">
        <div class="form-card-container">
          <div class="form-card">
            <div class="form-card-content">
              <?= get_post_meta(get_the_ID(), '_pl-top-form', true); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section>
    <div class="container row">
      <div class="col-xs-12">
        <h2>Latest Research</h2>
      </div>
      <div class="col-xs-12">
        <div class="featured-research">
          <?php $featured_resource = get_post_meta(get_the_ID(), '_pl-featured-research', true); ?>
          <div class="featured-img">
            <?= _ws_thumbnail($featured_resource, 'full'); ?>
          </div>
          <h4><?= get_the_title($featured_resource); ?></h4>
          <a class="arrow" href="<?= get_permalink($featured_resource); ?>">learn more</a>
        </div>
      </div>
      <?php
      $researches = get_post_meta(get_the_ID(), '_pl-research', true);
      foreach ($researches as $research) : ?>
        <div class="col-md-6">
          <div class="research">
            <div class="featured-img">
              <?= _ws_thumbnail($research, 'standard'); ?>
            </div>
            <h4><?= get_the_title($research); ?></h4>
            <a class="arrow" href="<?= get_permalink($research); ?>">learn more</a>
          </div>
        </div>
      <?php
      endforeach; ?>
    </div>
  </section>
  <section class="advisors">
    <div class="container row">
      <div class="col-xs-12">
        <h2>Contributors</h2>
      </div>
      <?php
      $advisors = get_post_meta(get_the_ID(), '_pl-advisors', true) ?: array();
      foreach ($advisors as $advisor) : ?>
        <div <?php post_class('col-lg-4 col-sm-6 card gallery-card'); ?>>
          <img class="lazy-load" src="<?= $advisor['img']; ?>" alt="<?= $advisor['name']; ?>" />
          <div class="info">
            <div>
              <h4><?= $advisor['name']; ?></h4>
              <p class="advisor-title"><small><?= $advisor['title']; ?></small></p>
              <?= do_shortcode($advisor['bio']); ?>
            </div>
          </div>
        </div>
      <?php
      endforeach; ?>
    </div>
  </section>
  <?php
  $heading = get_post_meta(get_the_ID(), '_pl-cta-heading', true);
  $text = get_post_meta(get_the_ID(), '_pl-cta-text', true);
  $form = esc_html(get_post_meta(get_the_ID(), '_pl-cta-form', true));
  $bg = get_the_post_thumbnail_url();
  $x = get_post_meta($id, '_banner-x', true);
  $y = get_post_meta($id, '_banner-y', true);
  $xy = $x != '' && $y != '' ? $x . '% ' . $y . '%' : '';
  $pos = get_post_meta(get_the_ID(), '_banner-x', true) . get_post_meta(get_the_ID(), '_banner-y', true);
  echo do_shortcode('[cta_form class="overlay" bg_img="' . $bg . '" bg_pos="' . $xy . '" heading="' . $heading . '" form="' . $form . '"]' . $text . '[/cta_form]'); ?>
</main>

<?php get_footer(); ?>
