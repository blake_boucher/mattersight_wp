<?php
/* Template Name: Front Page */

get_header(); ?>

<main id="main" template="front-page">
  <header class="page-header relative">
    <div class="container row">
      <div class="col-xl-6 col-xl-offset-6 col-lg-8 col-lg-offset-4 col-md-10 col-md-offset-1">
        <div class="fp-banner-card">
          <?php
          $tag = get_post_meta(get_the_ID(), '_banner-headline-type', true) ?: 'h1';
          echo '<' . $tag . ' class="page-title">' . (get_post_meta(get_the_ID(), '_banner-headline', true) ?: get_the_title()) . '</' . $tag . '>';
          if ($subheadline = get_post_meta(get_the_ID(), '_banner-subheadline', true)) {
            if (substr($subheadline, 0, 1) == '<') {
              echo '<div class="page-subtitle">' . $subheadline . '</div>';
            }
            else {
              echo '<p class="page-subtitle">' . $subheadline . '</p>';
            }
          } ?>
        </div>
      </div>
    </div>
    <div class="fp-background" <?= get_query_var('amp') ? '' : 'style="' . _ws_thumbnail_background() . '"'; ?>></div>
  </header>
  <?php
  if (have_posts()) : while (have_posts()) : the_post();
    if (get_the_content()) : ?>
      <section class="fp-text">
        <div class="container row">
          <div class="col-xl-8 col-lg-10 col-sm-12">
            <?php the_content(); ?>
          </div>
        </div>
      </section>
    <?php
    endif;
  endwhile; endif;
  $heading = get_post_meta(get_the_ID(), '_fp-overview-heading', true);
  $parent = get_post_meta(get_the_ID(), '_fp-overview-parent', true);
  echo do_shortcode('[overview_list heading="' . $heading . '" p_id="' . $parent . '"]'); ?>
  <section class="kpis">
    <div class="container row no-padding">
      <?php
      $kpis = get_post_meta(get_the_ID(), '_fp-kpis', true) ?: array();
      foreach ($kpis as $kpi) : ?>
        <div class="col-lg-3 col-sm-6">
          <?= do_shortcode('[svg id="' . $kpi['svg'] . '"]'); ?>
          <h3><?= $kpi['text']; ?></h3>
        </div>
      <?php
      endforeach; ?>
    </div>
  </section>
  <?php
  $related_heading = get_post_meta(get_the_ID(), '_fp-related-heading', true);
  $related_type = get_post_meta(get_the_ID(), '_fp-related-type', true);
  $related_p1 = get_post_meta(get_the_ID(), '_fp-related-post-one', true);
  $related_p2 = get_post_meta(get_the_ID(), '_fp-related-post-two', true);
  $related_p3 = get_post_meta(get_the_ID(), '_fp-related-post-three', true);
  $related_post_type = get_post_meta(get_the_ID(), '_fp-related-post-type', true);
  $related_tax = get_post_meta(get_the_ID(), '_fp-related-tax', true);
  echo do_shortcode('[related heading="' . $related_heading . '" type="' . $related_type . '" post_1="' . $related_p1 . '" post_2="' . $related_p2 . '" post_3="' . $related_p3 . '" post_type="' . $related_post_type . '" tax="' . $related_tax . '"]');
  echo do_shortcode('[testimonial t_id="' . get_post_meta(get_the_ID(), '_fp-testimonial', true) . '"]');
  echo do_shortcode('[clients class="fp-clients" max="8"]');
  $cta_heading = get_post_meta(get_the_ID(), '_fp-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_fp-cta-text', true);
  $cta_btn_text = get_post_meta(get_the_ID(), '_fp-cta-btn-text', true);
  $cta_btn_link = get_post_meta(get_the_ID(), '_fp-cta-btn-link', true);
  $cta_bg_img = get_post_meta(get_the_ID(), '_fp-cta-bg-img', true);
  echo do_shortcode('[cta ' . ($cta_bg_img ? 'class="overlay"' : 'class="white-btn"') . ' heading="' . $cta_heading . '" btn_text="' . $cta_btn_text . '" btn_link="' . $cta_btn_link . '" bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta]'); ?>
</main>

<?php get_footer(); ?>
