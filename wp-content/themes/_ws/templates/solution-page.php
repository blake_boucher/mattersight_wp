<?php
/* Template Name: Solution Page */

get_header(); ?>

<main id="main" template="solution-page">
  <?php
  get_template_part('template-parts/banner', 'gray');
  if (have_posts()) : while (have_posts()) : the_post();
    if (get_the_content()) : ?>
      <section class="fp-text">
        <div class="container row">
          <div class="col-xl-8 col-lg-10 col-sm-12 wp-text">
            <?php the_content(); ?>
          </div>
        </div>
      </section>
    <?php
    endif;
  endwhile; endif;
  $kpis = get_post_meta(get_the_ID(), '_solution-kpis', true) ?: array();
  $o = '';
  foreach ($kpis as $i=>$kpi) {
    $o .= 'svg_' . ($i+1) . '="' . (isset($kpi['svg']) ? $kpi['svg'] : '') . '" title_' . ($i+1) . '="' . (isset($kpi['title']) ? $kpi['title'] : '') . '" text_' . ($i+1) . '="' . (isset($kpi['text']) ? $kpi['text'] : '') . '" ';
  }
  echo do_shortcode('[kpis class="solution-kpis" ' . $o . ' btn_text="' . get_post_meta(get_the_ID(), '_solution-kpi-btn-text', true) . '" btn_link="' . get_post_meta(get_the_ID(), '_solution-kpi-btn-link', true) . '"]');
  if ($suites = get_post_meta(get_the_ID(), '_solution-suites', true)) : ?>
    <section class="solution-suites">
      <div class="container row">
        <div class="col-xl-6 col-xl-offset-3 col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 center">
          <h2><?= get_post_meta(get_the_ID(), '_solution-suites-heading', true); ?></h2>
        </div>
      </div>
      <div>
        <?php
        foreach ($suites as $i=>$suite) : ?>
          <div class="container-fluid row suite">
            <div class="col-xs-12 row no-padding">
              <div class="col-md-6 col-sm-12 no-padding">
                <div class="featured-img">
                  <?= '<img src="' . $suite['img'] . '" alt="' . $suite['heading'] . '" style="object-fit:cover;object-position:50% 0%" data-object-fit="cover" data-object-position="50% 0%" />'; ?>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 card-content">
                <div>
                  <?= $suite['heading'] ? '<h3>' . $suite['heading'] . '</h3>' : ''; ?>
                  <?= $suite['text'] ? '<p>' . $suite['text'] . '</p>' : ''; ?>
                  <?= $suite['kpi-one-heading'] || $suite['kpi-one-text'] ? '<button class="toggle" target="#solution-dropdown-' . $i . '">expand benefits +</button>' : ''; ?>
                </div>
              </div>
            </div>
            <?php
            if ($suite['kpi-one-heading'] || $suite['kpi-one-text']) : ?>
              <div id="solution-dropdown-<?= $i; ?>" class="col-xs-12 solution-dropdown">
                <div class="row">
                  <div class="col-md-4 kpi">
                    <?= do_shortcode('[svg id="' . $suite['kpi-one-svg'] . '"]'); ?>
                    <h4><?= $suite['kpi-one-heading']; ?></h4>
                    <p><?= $suite['kpi-one-text']; ?></p>
                  </div>
                  <div class="col-md-4 kpi">
                    <?= do_shortcode('[svg id="' . $suite['kpi-two-svg'] . '"]'); ?>
                    <h4><?= $suite['kpi-two-heading']; ?></h4>
                    <p><?= $suite['kpi-two-text']; ?></p>
                  </div>
                  <div class="col-md-4 kpi">
                    <?= do_shortcode('[svg id="' . $suite['kpi-three-svg'] . '"]'); ?>
                    <h4><?= $suite['kpi-three-heading']; ?></h4>
                    <p><?= $suite['kpi-three-text']; ?></p>
                  </div>
                  <div class="col-xs-12 kpi-button">
                    <button class="toggle" target="#solution-dropdown-<?= $i; ?>">hide benefits -</button>
                  </div>
                </div>
              </div>
            <?php
            endif; ?>
          </div>
        <?php
        endforeach;
        $policy = get_post_meta(get_the_ID(), '_solution-privacy-policy', true);
        if ($policy) : ?>
          <div class="container-fluid row">
            <div class="col-xs-12">
              <a class="arrow" href="<?= $policy; ?>">Product Privacy Policy</a>
            </div>
          </div>
        <?php
        endif; ?>
      </div>
    </section>
  <?php
  endif;
  $cta_heading = get_post_meta(get_the_ID(), '_solution-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_solution-cta-text', true);
  $cta_btn_text = get_post_meta(get_the_ID(), '_solution-cta-btn-text', true);
  $cta_btn_link = get_post_meta(get_the_ID(), '_solution-cta-btn-link', true);
  $cta_bg_img = get_post_meta(get_the_ID(), '_solution-cta-bg-img', true);
  echo do_shortcode('[cta heading="' . $cta_heading . '" btn_text="' . $cta_btn_text . '" btn_link="' . $cta_btn_link . '" ' . ($cta_bg_img ? 'class="overlay"' : 'class="white-btn"') . ' bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta]');
  $related_heading = get_post_meta(get_the_ID(), '_solution-related-heading', true);
  $related_type = get_post_meta(get_the_ID(), '_solution-related-type', true);
  $related_p1 = get_post_meta(get_the_ID(), '_solution-related-post-one', true);
  $related_p2 = get_post_meta(get_the_ID(), '_solution-related-post-two', true);
  $related_p3 = get_post_meta(get_the_ID(), '_solution-related-post-three', true);
  $related_post_type = get_post_meta(get_the_ID(), '_solution-related-post-type', true);
  $related_tax = get_post_meta(get_the_ID(), '_solution-related-tax', true);
  echo do_shortcode('[related heading="' . $related_heading . '" type="' . $related_type . '" post_1="' . $related_p1 . '" post_2="' . $related_p2 . '" post_3="' . $related_p3 . '" post_type="' . $related_post_type . '" tax="' . $related_tax . '"]'); ?>
</main>

<?php get_footer(); ?>
