<?php
/* Template Name: Calculator */

get_header(); ?>

<main id="main" class="calculator-container" template="calculator">
  <transition name="fade" mode="out-in" @enter="enter">
    <div id="slide-1" v-if="step == 0" key="slide-1">
      <?php get_template_part('template-parts/banner', 'split'); ?>
      <div id="anchor"></div>
      <section class="questions">
        <div class="container row">
          <div class="col-lg-5 col-md-6 service-sales">
            <div>
              <h3>Service</h3>
              <?= do_shortcode('<p>' . get_post_meta(get_the_ID(), '_calc-service-intro', true) . '</p>'); ?>
            </div>
            <div>
              <a class="btn" href="#step-1" @click="step = 1">start calculating</a>
            </div>
          </div>
          <div class="col-lg-5 col-lg-offset-2 col-md-6 service-sales">
            <div>
              <h3>Sales</h3>
              <?= do_shortcode('<p>' . get_post_meta(get_the_ID(), '_calc-sales-intro', true) . '</p>'); ?>
            </div>
            <div>
              <a class="btn" href="#contact-us" @click="step = 'contact-us'">contact us</a>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div id="slide-2" v-if="step >= 1" key="slide-2">
      <header class="page-header">
        <div class="container row">
          <div class="col-lg-6">
            <h3><?= get_the_title(); ?></h3>
          </div>
        </div>
      </header>
      <div id="anchor"></div>
      <section class="questions">
        <transition name="fade" mode="out-in" @enter="enter">
          <div v-if="step >= 1" key="container" class="container">
            <div class="row">
              <div class="col-lg-6 col-md-8">
                <transition name="fade" mode="out-in">
                  <p v-for="(s, i) in steps" v-if="step == i+1" :key="i" class="step-title">{{s}}</p>
                </transition>
                <div class="meter">
                  <div v-for="n in steps.length" :class="{done: step >= n}"></div>
                </div>
              </div>
            </div>
            <transition name="fade" mode="out-in" @enter="enter">
              <form id="step-1" v-if="step == 1" key="step-1" class="row">
                <div class="col-xs-12">
                  <label for="reps" class="question">How many customer service or contact reps do you have?</label>
                  <input id="reps" v-model="reps" min="0" type="number" required />
                  <p v-if="invalid" class="warning-text">Invalid Value</p>
                  <!-- <div class="slider">
                    <div v-for="n in 6" class="notch"><span>{{(n-1)*5000}}</span></div>
                    <div class="ball" :style="{left: ball}">{{reps}}</div>
                  </div> -->
                  <div class="center">
                    <a class="btn" href="#step-2" @click.prevent="step1()">next</a>
                  </div>
                </div>
              </form>
              <div id="step-2" v-if="step == 2" key="step-2" class="row">
                <div class="col-xs-12">
                  <fieldset>
                    <legend class="question">What is your industry?</legend>
                    <div class="options">
                      <input id="communications" v-model="industry" name="industry" type="radio" value="Communications, Media and Technology" required />
                      <label tabindex="0" for="communications" @keydown.enter="industry = 'communications'">
                        <?= do_shortcode('[svg id="computer"]'); ?>
                        <p>Communications, Media and Technology</p>
                      </label>
                      <input id="financial" v-model="industry" name="industry" type="radio" value="Financial" required />
                      <label tabindex="0" for="financial" @keydown.enter="industry = 'financial'">
                        <?= do_shortcode('[svg id="briefcase"]'); ?>
                        <p>Financial Services</p>
                      </label>
                      <input id="healthcare" v-model="industry" name="industry" type="radio" value="Healthcare" required />
                      <label tabindex="0" for="healthcare" @keydown.enter="industry = 'healthcare'">
                        <?= do_shortcode('[svg id="doctor"]'); ?>
                        <p>Healthcare</p>
                      </label>
                      <input id="products" v-model="industry" name="industry" type="radio" value="Products" required />
                      <label tabindex="0" for="products" @keydown.enter="industry = 'products'">
                        <?= do_shortcode('[svg id="box"]'); ?>
                        <p>Products</p>
                      </label>
                      <input id="other" v-model="industry" name="industry" type="radio" value="Other" required />
                      <label tabindex="0" for="other" @keydown.enter="industry = 'other'">
                        <?= do_shortcode('[svg id="globe"]'); ?>
                        <p>Other</p>
                      </label>
                    </div>
                    <div v-if="invalid" class="warning-text">Required Field</div>
                  </fieldset>
                  <div class="center">
                    <a class="btn" href="#step-3" @click.prevent="step2()">next</a>
                  </div>
                </div>
              </div>
              <div id="step-3" v-if="step == 3" key="step-3" class="row">
                <div class="col-xs-12">
                  <h3>For the most accurate calculation, we just need your best estimate of the following three KPIs:</h3>
                </div>
                <div class="col-lg-4 optional-question">
                  <label for="handle-time">Average Handle Time <small>(seconds)</small></label>
                  <input id="handle-time" type="number" v-model="averageHandleTime" :placeholder="averageHandleTimeP" />
                  <p v-if="invalid1" class="warning-text">Invalid Value</p>
                </div>
                <div class="col-lg-4 optional-question">
                  <label for="annual-calls">Annual Calls Handled</label>
                  <input id="annual-calls" type="number" v-model="annualCalls" :placeholder="annualCallsP" />
                  <p v-if="invalid2" class="warning-text">Invalid Value</p>
                </div>
                <div class="col-lg-4 optional-question">
                  <label for="cost">Cost Per Minute <small>(USD)</small></label>
                  <input id="cost" type="number" step="0.01" v-model="costPerMinute" :placeholder="costPerMinuteP" />
                  <p v-if="invalid3" class="warning-text">Invalid Value</p>
                </div>
                <div class="col-md-8">
                  <p class="copy">Each field contains the average metrics we've seen at the leading contact centers we work with. Feel free to use these estimates if you are unsure about your exact numbers.</p>
                </div>
                <div class="col-xs-12 center">
                  <a class="btn" href="#step-4" @click.prevent="step3()">next</a>
                </div>
              </div>
              <div id="step-4" v-if="step == 4" key="step-4" class="row">
                <div class="col-xs-12">
                  <h3>Realized savings of ${{Math.round(year3/1000000*10)/10}}M over a 3-year period while delivering a best-in-class customer experience.</h3>
                  <p class="chart-title">Cumulative Savings</p>
                  <div id="web-chart"></div>
                  <div id="pdf-chart"></div>
                  <div class="center">
                    <a class="btn" href="#step-5" @click="step = 5">get a detailed report</a>
                  </div>
                </div>
              </div>
              <div id="step-5" v-if="step == 5" key="step-5" class="row">
                <div class="col-lg-7">
                  <h3><?= get_post_meta(get_the_ID(), '_calc-service-form-heading', true); ?></h3>
                  <?= do_shortcode(get_post_meta(get_the_ID(), '_calc-service-form-text', true)); ?>
                </div>
                <div class="col-xl-4 col-xl-offset-1 col-lg-5">
                  <div class="form-card-container">
                    <div class="form-card">
                      <div class="form-card-content">
                        <?= get_post_meta(get_the_ID(), '_calc-service-form', true); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div id="step-6" v-if="step == 6" key="step-6" class="row">
                <div class="col-md-8">
                  <?= do_shortcode(get_post_meta(get_the_ID(), '_calc-thank-you', true)); ?>
                </div>
              </div>
            </transition>
          </div>
        </transition>
      </section>
      <transition name="fade">
        <div v-if="step == 6">
          <?php
          $related_heading = get_post_meta(get_the_ID(), '_calc-related-heading', true);
          $related_type = get_post_meta(get_the_ID(), '_calc-related-type', true);
          $related_p1 = get_post_meta(get_the_ID(), '_calc-related-post-one', true);
          $related_p2 = get_post_meta(get_the_ID(), '_calc-related-post-two', true);
          $related_p3 = get_post_meta(get_the_ID(), '_calc-related-post-three', true);
          $related_post_type = get_post_meta(get_the_ID(), '_calc-related-post-type', true);
          $related_tax = get_post_meta(get_the_ID(), '_calc-related-tax', true);
          echo do_shortcode('[related heading="' . $related_heading . '" type="' . $related_type . '" post_1="' . $related_p1 . '" post_2="' . $related_p2 . '" post_3="' . $related_p3 . '" post_type="' . $related_post_type . '" tax="' . $related_tax . '"]'); ?>
        </div>
      </transition>
    </div>
    <div id="contact-us" v-if="step == 'contact-us'" key="slide-3">
      <header class="page-header">
        <div class="container row">
          <div class="col-lg-6">
            <h3><?= get_the_title(); ?></h3>
          </div>
        </div>
      </header>
      <div id="anchor"></div>
      <section class="questions">
        <div class="container row">
          <div class="col-xl-7 col-lg-7">
            <h3><?= get_post_meta(get_the_ID(), '_calc-sales-form-heading', true); ?></h3>
            <?= do_shortcode(get_post_meta(get_the_ID(), '_calc-sales-form-text', true)); ?>
          </div>
          <div class="col-xl-4 col-xl-offset-1 col-lg-5 col-md-6 col-sm-8">
            <div class="form-card-container">
              <div class="form-card">
                <div class="form-card-content">
                  <?= get_post_meta(get_the_ID(), '_calc-sales-form', true); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </transition>
</main>

<?php get_footer(); ?>
