<?php
/* Template Name: Careers Page */

get_header(); ?>

<main id="main" template="careers-page">
  <?php
  get_template_part('template-parts/banner', 'gray');
  if (have_posts()) : while (have_posts()) : the_post();
    if (get_the_content()) {
      ob_start();
      the_content();
      $content = ob_get_contents();
      ob_end_clean();
      echo do_shortcode('[text class="gray"]' . $content . '[/text]');
    }
  endwhile; endif; ?>
  <section class="careers-personalities">
    <div class="container row">
      <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <h2><?= get_post_meta(get_the_ID(), '_careers-personality-heading', true); ?></h2>
        <?= do_shortcode(get_post_meta(get_the_ID(), '_careers-personality-text', true)); ?>
        <div class="row personality-types">
          <?php
          $items = get_post_meta(get_the_ID(), '_careers-personality-items', true) ?: array();
          foreach ($items as $item) : ?>
            <div class="col-md-4 col-sm-6">
              <?php
              if ($item['svg']) {
                echo do_shortcode('[svg id="' . $item['svg'] . '"]');
              }
              if ($item['heading']) {
                echo '<h3>' . $item['heading'] . '</h3>';
              }
              if ($item['text']) {
                echo '<p>' . $item['text'] . '</p>';
              } ?>
            </div>
          <?php
          endforeach; ?>
        </div>
      </div>
    </div>
  </section>
  <section class="checkerboard">
    <div class="container row">
      <div class="col-xs-12">
        <h2><?= get_post_meta(get_the_ID(), '_careers-checkerboard-title', true); ?></h2>
      </div>
    </div>
    <div class="container-fluid row no-padding squares">
      <div class="col-md-4">
        <?= get_post_meta(get_the_ID(), '_careers-checkerboard-text-1', true); ?>
      </div>
      <div class="col-md-4 no-padding">
        <div class="featured-img">
          <img class="lazy-load" data-src="<?= get_post_meta(get_the_ID(), '_careers-checkerboard-img-1', true); ?>" alt="Working at Mattersight" style="object-fit:cover;" data-object-fit="cover" />
        </div>
      </div>
      <div class="col-md-4">
        <?= get_post_meta(get_the_ID(), '_careers-checkerboard-text-2', true); ?>
      </div>
      <div class="col-md-4 no-padding">
        <div class="featured-img">
          <img class="lazy-load" data-src="<?= get_post_meta(get_the_ID(), '_careers-checkerboard-img-2', true); ?>" alt="Working at Mattersight" style="object-fit:cover;" data-object-fit="cover" />
        </div>
      </div>
      <div class="col-md-4">
        <?= get_post_meta(get_the_ID(), '_careers-checkerboard-text-3', true); ?>
      </div>
      <div class="col-md-4 no-padding">
        <div class="featured-img">
          <img class="lazy-load" data-src="<?= get_post_meta(get_the_ID(), '_careers-checkerboard-img-3', true); ?>" alt="Working at Mattersight" style="object-fit:cover;" data-object-fit="cover" />
        </div>
      </div>
      <div class="col-md-4">
        <?= get_post_meta(get_the_ID(), '_careers-checkerboard-text-4', true); ?>
      </div>
      <div class="col-md-4 no-padding">
        <div class="featured-img">
          <img class="lazy-load" data-src="<?= get_post_meta(get_the_ID(), '_careers-checkerboard-img-4', true); ?>" alt="Working at Mattersight" style="object-fit:cover;" data-object-fit="cover" />
        </div>
      </div>
      <div class="col-md-4">
        <?= get_post_meta(get_the_ID(), '_careers-checkerboard-text-5', true); ?>
      </div>
    </div>
  </section>
  <?= do_shortcode('[locations heading="Our Locations"]'); ?>
  <section class="gray">
    <div class="container row">
      <div class="col-xs-12 center">
        <h2>Explore Mattersight</h2>
      </div>
      <div class="col-md-3 col-md-offset-3 center">
        <img class="lazy-load aligncenter" data-src="<?= _ws_get_attachment_url_by_slug('linkedin'); ?>" alt="linkedin" />
        <a class="arrow" href="https://www.linkedin.com/company/mattersight-corporation/" target="_blank">view linkedin</a>
      </div>
      <div class="col-md-3 center">
        <img class="lazy-load aligncenter" data-src="<?= _ws_get_attachment_url_by_slug('glassdoor'); ?>" alt="glassdoor" />
        <a class="arrow" href="https://www.glassdoor.com/Overview/Working-at-Mattersight-EI_IE409129.11,22.htm" target="_blank">view glassdoor</a>
      </div>
    </div>
  </section>
  <?php
  if ($img = get_post_meta(get_the_ID(), '_careers-img', true)) {
    echo '<img class="lazy-load careers-img" data-src="' . $img . '" alt="Working at Mattersight" />';
  }
  if ($t_id = get_post_meta(get_the_ID(), '_careers-testimonial', true)) : ?>
    <section class="pb-testimonial">
      <div class="container row">
        <div class="col-xl-8 col-xl-offset-2 col-lg-10 col-lg-offset-1 center">
          <h2 class="red-text">employee spotlight</h2>
          <blockquote>
            <p class="h3"><?= get_post($t_id)->post_content; ?></p>
          </blockquote>
          <div class="attestant">
            <?php
            if ($img = _ws_thumbnail($t_id, 'thumbnail', true)) {
              echo '<div class="featured-img">' . ($img ?: '') . '</div>';
            } ?>
            <div>
              <p class="name"><?= get_post_meta($t_id, '_testimonial-name', true) ?: get_the_title($t_id); ?></p>
              <?php
              if ($title = get_post_meta($t_id, '_testimonial-title', true)) {
                echo '<p class="title">' . $title . '</p>';
              }
              if ($linkedin = get_post_meta($t_id, '_testimonial-linkedin', true)) {
                echo '<a class="arrow" href="' . $linkedin . '" target="_blank">view linkedin</a>';
              } ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php
  endif;
  $job_heading = get_post_meta(get_the_ID(), '_careers-job-heading', true);
  $job_1 = get_post_meta(get_the_ID(), '_careers-job-1', true);
  $job_2 = get_post_meta(get_the_ID(), '_careers-job-2', true);
  $job_3 = get_post_meta(get_the_ID(), '_careers-job-3', true);
  echo do_shortcode('[featured_jobs heading="' . $job_heading . '" job_1="' . $job_1 . '" job_2="' . $job_2 . '" job_3="' . $job_3 . '"]'); ?>
</main>

<?php get_footer(); ?>
