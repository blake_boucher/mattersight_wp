<?php
/* Template Name: Investors News */

get_header(); ?>

<main id="main" template="investors-news">
  <?php
  get_template_part('template-parts/banner', 'split');
  wp_nav_menu(array(
    'theme_location' => 'investors',
    'container' => 'nav',
    'container_class' => 'investors-menu',
    'item_spacing' => 'discard'
  )); ?>
  <section>
    <div class="container row">
      <div class="col-xl-9 col-lg-8 col-md-8 col-sm-6">
        <h2>Latest News</h2>
      </div>
      <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6">
        <form class="investor-filter" action="" method="get">
          <?php
          $y = isset($_GET['y']) ? $_GET['y'] : '';
          $years = wp_get_archives(array('type'=>'yearly', 'format'=>'custom', 'echo' => 0, 'post_type'=>'investor_news'));
          $years = explode('</a>', $years);
          array_pop($years); ?>
          <label class="screen-reader-text" for="year">Year</label>
          <select id="year" name="y">
            <option value="">select year</option>
            <?php
            foreach ($years as $year) {
              $year = substr($year, -4);
              echo '<option value="' . $year . '" ' . ($year == $y ? 'selected' : '') . '>' . $year . '</option>';
            } ?>
          </select>
        </form>
      </div>
    </div>
    <?php
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $args = array('post_type'=>'investor_news', 'post_status'=>'publish', 'paged'=>$paged);
    if ($y) {
      $args['date_query'] = array(
        array(
          'year' => $y
        )
      );
    }
    $loop = new WP_Query($args);
    if ($loop->have_posts()) : ?>
      <div class="container row <?= $loop->max_num_pages > 1 ? 'infinite-scroll-btn' : ''; ?>">
        <input class="loop-var" type="hidden" value="<?= htmlentities(json_encode($loop)); ?>" />
        <?php
        while ($loop->have_posts()) : $loop->the_post();
          get_template_part('template-parts/archive', get_post_type());
        endwhile; ?>
      </div>
    <?php
    else: ?>
      <div class="container row">
        <div class="col-xs-12 center">
          <p>No results found.</p>
        </div>
      </div>
    <?php
    endif; wp_reset_postdata(); ?>
  </section>
  <?php
  $cta_heading = get_post_meta(get_the_ID(), '_in-cta-heading', true);
  $cta_text = get_post_meta(get_the_ID(), '_in-cta-text', true);
  $cta_form = esc_html(get_post_meta(get_the_ID(), '_in-cta-form', true));
  $cta_bg_img = get_post_meta(get_the_ID(), '_in-cta-bg-img', true);
  echo do_shortcode('[cta_form ' . ($cta_bg_img ? 'class="overlay"' : '') . ' heading="' . $cta_heading . '" form="' . $cta_form . '" bg_img="' . $cta_bg_img . '"]' . $cta_text . '[/cta_form]'); ?>
</main>

<?php get_footer(); ?>
