<footer id="site-footer">
  <div class="row container">
    <div class="col-xs-12 relative">
      <div class="footer-form">
        <iframe title="Newsletter Signup" src="https://go.mattersight.com/l/71002/2018-03-27/5sdqks" width="100%" type="text/html" frameborder="0" allowTransparency="true" style="border: 0"></iframe>
      </div>
    </div>
  </div>
  <?php
  if (is_active_sidebar('footer')) : ?>
    <div class="footer-widgets-area">
      <div class="container">
        <ul class="footer-widgets row">
          <?php dynamic_sidebar('footer'); ?>
        </ul>
      </div>
    </div>
  <?php
  endif;
  if (has_nav_menu('footer')) {
    wp_nav_menu(array('theme_location'=>'footer', 'container_class'=>'footer-menu', 'menu_class'=>'container', 'item_spacing'=>'discard'));
  } ?>
  <div class="copyright">
    <div class="container row">
      <div class="col-md-6 left">
        <?= do_shortcode('[social_icons icons="linkedin, twitter, facebook"]'); ?>
      </div>
      <div class="col-md-6 right">
        <small class="light">&copy;	<?= date('Y'); ?>. Mattersight Corporation. All rights reserved. <a href="/legal-privacy/">Legal & Privacy</a></small>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
