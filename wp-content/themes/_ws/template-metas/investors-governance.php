<?php
// Investors governance meta fields
function _ws_ig_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'ig-nonce');
  $ig_cta_heading = get_post_meta(get_the_ID(), '_ig-cta-heading', true);
  $ig_cta_text = get_post_meta(get_the_ID(), '_ig-cta-text', true);
  $ig_cta_form = get_post_meta(get_the_ID(), '_ig-cta-form', true);
  $ig_cta_bg_img = get_post_meta(get_the_ID(), '_ig-cta-bg-img', true); ?>
  <div id="ig-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="ig-cta-heading">Heading</label>
                      <input id="ig-cta-heading" name="ig-cta-heading" type="text" value="<?= $ig_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="ig-cta-text">Text</label>
                      <textarea id="ig-cta-text" name="ig-cta-text" class="text-editor"><?= $ig_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="ig-cta-form">Form (Card)</label>
                      <textarea id="ig-cta-form" name="ig-cta-form"><?= $ig_cta_form; ?></textarea>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <label for="ig-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#ig-cta-bg-img">Select Image</button>
                  <input id="ig-cta-bg-img" class="flex-1" name="ig-cta-bg-img" type="text" value="<?= $ig_cta_bg_img; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_ig_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/investors-governance.php') {
    add_meta_box('ig-meta-box', 'Investors Overview Template Options', '_ws_ig_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_ig_meta');

// Save meta values
function _ws_save_ig_meta($post_id) {
  if (!isset($_POST['ig-nonce']) || !wp_verify_nonce($_POST['ig-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $ig_cta_heading = isset($_POST['ig-cta-heading']) ? $_POST['ig-cta-heading'] : '';
  update_post_meta($post_id, '_ig-cta-heading', $ig_cta_heading);

  $ig_cta_text = isset($_POST['ig-cta-text']) ? $_POST['ig-cta-text'] : '';
  update_post_meta($post_id, '_ig-cta-text', $ig_cta_text);

  $ig_cta_form = isset($_POST['ig-cta-form']) ? $_POST['ig-cta-form'] : '';
  update_post_meta($post_id, '_ig-cta-form', $ig_cta_form);

  $ig_cta_bg_img = isset($_POST['ig-cta-bg-img']) ? $_POST['ig-cta-bg-img'] : '';
  update_post_meta($post_id, '_ig-cta-bg-img', $ig_cta_bg_img);
}
add_action('save_post', '_ws_save_ig_meta');
