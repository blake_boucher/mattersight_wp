<?php
// Landing page meta fields
function _ws_lp_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'lp-nonce');
  $lp_resource_img = get_post_meta(get_the_ID(), '_lp-resource-img', true);
  $lp_card = get_post_meta(get_the_ID(), '_lp-card', true); ?>
  <div id="lp-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="lp-resource-img">Form Resource Image</label>
          <div class="row">
            <button class="button media-selector" target="#lp-resource-img">Select Image</button>
            <input id="lp-resource-img" class="flex-1" name="lp-resource-img" type="text" value="<?= $lp_resource_img; ?>" />
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="lp-card">Form <small>Card</small></label>
          <textarea id="lp-card" name="lp-card"><?= $lp_card; ?></textarea>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_lp_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/landing-page.php') {
    add_meta_box('lp-meta-box', 'Landing Page Template Options', '_ws_lp_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_lp_meta');

// Save meta values
function _ws_save_lp_meta($post_id) {
  if (!isset($_POST['lp-nonce']) || !wp_verify_nonce($_POST['lp-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $lp_resource_img = isset($_POST['lp-resource-img']) ? $_POST['lp-resource-img'] : '';
  update_post_meta($post_id, '_lp-resource-img', $lp_resource_img);

  $lp_card = isset($_POST['lp-card']) ? $_POST['lp-card'] : '';
  update_post_meta($post_id, '_lp-card', $lp_card);
}
add_action('save_post', '_ws_save_lp_meta');
