<?php
// Investors generic meta fields
function _ws_in_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'in-nonce');
  $in_quote = get_post_meta(get_the_ID(), '_in-quote', true);
  $in_cta_heading = get_post_meta(get_the_ID(), '_in-cta-heading', true);
  $in_cta_text = get_post_meta(get_the_ID(), '_in-cta-text', true);
  $in_cta_form = get_post_meta(get_the_ID(), '_in-cta-form', true);
  $in_cta_bg_img = get_post_meta(get_the_ID(), '_in-cta-bg-img', true); ?>
  <div id="in-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="in-cta-heading">Heading</label>
                      <input id="in-cta-heading" name="in-cta-heading" type="text" value="<?= $in_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="in-cta-text">Text</label>
                      <textarea id="in-cta-text" name="in-cta-text" class="text-editor"><?= $in_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="in-cta-form">Form (Card)</label>
                      <textarea id="in-cta-form" name="in-cta-form"><?= $in_cta_form; ?></textarea>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <label for="in-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#in-cta-bg-img">Select Image</button>
                  <input id="in-cta-bg-img" class="flex-1" name="in-cta-bg-img" type="text" value="<?= $in_cta_bg_img; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_in_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/investors-news.php') {
    add_meta_box('in-meta-box', 'Investors News Template Options', '_ws_in_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_in_meta');

// Save meta values
function _ws_save_in_meta($post_id) {
  if (!isset($_POST['in-nonce']) || !wp_verify_nonce($_POST['in-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $in_cta_heading = isset($_POST['in-cta-heading']) ? $_POST['in-cta-heading'] : '';
  update_post_meta($post_id, '_in-cta-heading', $in_cta_heading);

  $in_cta_text = isset($_POST['in-cta-text']) ? $_POST['in-cta-text'] : '';
  update_post_meta($post_id, '_in-cta-text', $in_cta_text);

  $in_cta_form = isset($_POST['in-cta-form']) ? $_POST['in-cta-form'] : '';
  update_post_meta($post_id, '_in-cta-form', $in_cta_form);

  $in_cta_bg_img = isset($_POST['in-cta-bg-img']) ? $_POST['in-cta-bg-img'] : '';
  update_post_meta($post_id, '_in-cta-bg-img', $in_cta_bg_img);
}
add_action('save_post', '_ws_save_in_meta');
