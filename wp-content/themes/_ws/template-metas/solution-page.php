<?php
// Solution page meta fields
function _ws_solution_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'solution-nonce');
  $solution_kpis = get_post_meta(get_the_ID(), '_solution-kpis', true);
  $solution_kpi_btn_text = get_post_meta(get_the_ID(), '_solution-kpi-btn-text', true);
  $solution_kpi_btn_link = get_post_meta(get_the_ID(), '_solution-kpi-btn-link', true);
  $solution_suites_heading = get_post_meta(get_the_ID(), '_solution-suites-heading', true);
  $solution_suites = get_post_meta(get_the_ID(), '_solution-suites', true) ?: array();
  $solution_privacy_policy = get_post_meta(get_the_ID(), '_solution-privacy-policy', true);
  $solution_cta_heading = get_post_meta(get_the_ID(), '_solution-cta-heading', true);
  $solution_cta_text = get_post_meta(get_the_ID(), '_solution-cta-text', true);
  $solution_cta_btn_text = get_post_meta(get_the_ID(), '_solution-cta-btn-text', true);
  $solution_cta_btn_link = get_post_meta(get_the_ID(), '_solution-cta-btn-link', true);
  $solution_cta_bg_img = get_post_meta(get_the_ID(), '_solution-cta-bg-img', true);
  $solution_related_heading = get_post_meta(get_the_ID(), '_solution-related-heading', true);
  $solution_related_type = get_post_meta(get_the_ID(), '_solution-related-type', true);
  $solution_related_post_one = get_post_meta(get_the_ID(), '_solution-related-post-one', true);
  $solution_related_post_two = get_post_meta(get_the_ID(), '_solution-related-post-two', true);
  $solution_related_post_three = get_post_meta(get_the_ID(), '_solution-related-post-three', true);
  $solution_related_post_type = get_post_meta(get_the_ID(), '_solution-related-post-type', true);
  $solution_related_tax = get_post_meta(get_the_ID(), '_solution-related-tax', true); ?>
  <div id="solution-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 1</legend>
            <ul>
              <li>
                <label for="solution-kpis-1-svg">SVG</label>
                <input id="solution-kpis-1-svg" name="solution-kpis[0][svg]" class="svg-selector" type="text" value="<?= isset($solution_kpis[0]['svg']) ? $solution_kpis[0]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="solution-kpis-1-title">Title</label>
                <input id="solution-kpis-1-title" name="solution-kpis[0][title]" type="text" value="<?= isset($solution_kpis[0]['title']) ? $solution_kpis[0]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="solution-kpis-1-text">Text</label>
                <textarea id="solution-kpis-1-text" name="solution-kpis[0][text]"><?= isset($solution_kpis[0]['text']) ? $solution_kpis[0]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 2</legend>
            <ul>
              <li>
                <label for="solution-kpis-2-svg">SVG</label>
                <input id="solution-kpis-2-svg" name="solution-kpis[1][svg]" class="svg-selector" type="text" value="<?= isset($solution_kpis[1]['svg']) ? $solution_kpis[1]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="solution-kpis-2-title">Title</label>
                <input id="solution-kpis-2-title" name="solution-kpis[1][title]" type="text" value="<?= isset($solution_kpis[1]['title']) ? $solution_kpis[1]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="solution-kpis-2-text">Text</label>
                <textarea id="solution-kpis-2-text" name="solution-kpis[1][text]"><?= isset($solution_kpis[1]['text']) ? $solution_kpis[1]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 3</legend>
            <ul>
              <li>
                <label for="solution-kpis-3-svg">SVG</label>
                <input id="solution-kpis-3-svg" name="solution-kpis[2][svg]" class="svg-selector" type="text" value="<?= isset($solution_kpis[2]['svg']) ? $solution_kpis[2]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="solution-kpis-3-title">Title</label>
                <input id="solution-kpis-3-title" name="solution-kpis[2][title]" type="text" value="<?= isset($solution_kpis[2]['title']) ? $solution_kpis[2]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="solution-kpis-3-text">Text</label>
                <textarea id="solution-kpis-3-text" name="solution-kpis[2][text]"><?= isset($solution_kpis[2]['text']) ? $solution_kpis[2]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-sm-6">
          <label for="solution-kpi-btn-text">KPI Button Text</label>
          <input id="solution-kpi-btn-text" name="solution-kpi-btn-text" type="text" value="<?= $solution_kpi_btn_text; ?>" />
        </div>
        <div class="col-sm-6">
          <label for="solution-kpi-btn-link">KPI Button URL</label>
          <input id="solution-kpi-btn-link" name="solution-kpi-btn-link" type="text" value="<?= $solution_kpi_btn_link; ?>" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Solution Suites</legend>
            <ul>
              <li>
                <label for="solution-suites-heading">Heading</label>
                <input id="solution-suites-heading" name="solution-suites-heading" type="text" value="<?= $solution_suites_heading; ?>" />
              </li>
            </ul>
            <ul class="sortable-container">
              <?php
              foreach($solution_suites as $i=>$solution_suite) : ?>
                <li class="sortable-item">
                  <div class="sortable-header">
                    <span class="dashicons dashicons-move sortable-handle"></span>
                    <span class="dashicons dashicons-trash sortable-delete"></span>
                  </div>
                  <ul class="sortable-content">
                    <li class="row">
                      <div class="col-xs-6">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-heading" name="product-suites[<?= $i; ?>][heading]" type="text" value="<?= $solution_suite['heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-text" name="product-suites[<?= $i; ?>][text]"><?= $solution_suite['text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                      <div class="col-xs-6">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-img">Image</label>
                            <div class="row">
                              <button class="button media-selector" target="#product-suites-<?= $i; ?>-img" size="large">Select Image</button>
                              <input id="product-suites-<?= $i; ?>-img" class="flex-1" name="product-suites[<?= $i; ?>][img]" type="text" value="<?= $solution_suite['img']; ?>" />
                            </div>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="row">
                      <div class="col-xs-4">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-one-svg">SVG</label>
                            <input id="product-suites-<?= $i; ?>-kpi-one-svg" class="svg-selector" name="product-suites[<?= $i; ?>][kpi-one-svg]" type="text" value="<?= $solution_suite['kpi-one-svg']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-one-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-kpi-one-heading" name="product-suites[<?= $i; ?>][kpi-one-heading]" type="text" value="<?= $solution_suite['kpi-one-heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-one-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-kpi-one-text" name="product-suites[<?= $i; ?>][kpi-one-text]"><?= $solution_suite['kpi-one-text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                      <div class="col-xs-4">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-two-svg">SVG</label>
                            <input id="product-suites-<?= $i; ?>-kpi-two-svg" class="svg-selector" name="product-suites[<?= $i; ?>][kpi-two-svg]" value="<?= $solution_suite['kpi-two-svg']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-two-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-kpi-two-heading" name="product-suites[<?= $i; ?>][kpi-two-heading]" type="text" value="<?= $solution_suite['kpi-two-heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-two-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-kpi-two-text" name="product-suites[<?= $i; ?>][kpi-two-text]"><?= $solution_suite['kpi-two-text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                      <div class="col-xs-4">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-three-svg">SVG</label>
                            <input id="product-suites-<?= $i; ?>-kpi-three-svg" class="svg-selector" name="product-suites[<?= $i; ?>][kpi-three-svg]" value="<?= $solution_suite['kpi-three-svg']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-three-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-kpi-three-heading" name="product-suites[<?= $i; ?>][kpi-three-heading]" type="text" value="<?= $solution_suite['kpi-three-heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-three-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-kpi-three-text" name="product-suites[<?= $i; ?>][kpi-three-text]"><?= $solution_suite['kpi-three-text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                    </li>
                  </ul>
                </li>
              <?php
              endforeach; ?>
            </ul>
            <button id="add-product-suite" class="button">Add Solution Suite</button>
            <ul>
              <li>
                <label for="solution-privacy-policy">Privacy Policy URL</label>
                <input id="solution-privacy-policy" name="solution-privacy-policy" type="text" value="<?= $solution_privacy_policy; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li>
                <label for="solution-cta-heading">Heading</label>
                <input id="solution-cta-heading" name="solution-cta-heading" type="text" value="<?= $solution_cta_heading; ?>">
              </li>
              <li>
                <label for="solution-cta-text">Text</label>
                <textarea id="solution-cta-text" name="solution-cta-text" class="text-editor"><?= $solution_cta_text; ?></textarea>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="solution-cta-btn-text">Button Text</label>
                  <input id="solution-cta-btn-text" name="solution-cta-btn-text" type="text" value="<?= $solution_cta_btn_text; ?>">
                </div>
                <div class="col-xs-6">
                  <label for="solution-cta-btn-link">Button Link</label>
                  <input id="solution-cta-btn-link" name="solution-cta-btn-link" type="text" value="<?= $solution_cta_btn_link; ?>">
                </div>
              </li>
              <li>
                <label for="solution-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#solution-cta-bg-img">Select Image</button>
                  <input id="solution-cta-bg-img" name="solution-cta-bg-img" class="flex-1" type="text" value="<?= $solution_cta_bg_img; ?>">
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row pb-related">
        <div class="col-xs-12">
          <fieldset>
            <legend>Related Content</legend>
            <ul>
              <li>
                <label for="solution-related-heading">Heading</label>
                <input id="solution-related-heading" name="solution-related-heading" type="text" value="<?= $solution_related_heading; ?>" >
              </li>
              <li>
                <input id="solution-related-type-posts" name="solution-related-type" type="radio" value="posts" <?= $solution_related_type=='posts' ? 'checked' : ''; ?> />
                <label for="solution-related-type-posts">Specific Posts</label>
                <div class="row">
                  <div class="col-xs-4">
                    <input id="solution-related-post-one" name="solution-related-post-one" class="post-selector" type="text" value="<?= $solution_related_post_one; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="solution-related-post-two" name="solution-related-post-two" class="post-selector" type="text" value="<?= $solution_related_post_two; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="solution-related-post-three" name="solution-related-post-three" class="post-selector" type="text" value="<?= $solution_related_post_three; ?>" />
                  </div>
                </div>
              </li>
              <li>
                <input id="solution-related-type-post-type" name="solution-related-type" type="radio" value="post-type" <?= $solution_related_type=='post-type' ? 'checked' : ''; ?> />
                <label for="solution-related-type-post-type">Post Type</label>
                <div>
                  <select id="solution-related-post-type" name="solution-related-post-type">
                    <?php
                    $types = get_post_types(array('public'=>true));
                    foreach ($types as $type) {
                      $t = get_post_type_object($type);
                      echo '<option value="' . $t->name . '" ' . ($solution_related_post_type==$t->name ? 'selected' : '') . '>' . $t->labels->name . '</option>';
                    } ?>
                  </select>
                </div>
              </li>
              <li>
                <input id="solution-related-type-tax" name="solution-related-type" type="radio" value="tax" <?= $solution_related_type=='tax' ? 'checked' : ''; ?> />
                <label for="solution-related-type-tax">Taxonomy</label>
                <div>
                  <select id="solution-related-tax" name="solution-related-tax">
                    <?php
                    $terms = get_terms('related_content');
                    foreach ($terms as $term) {
                      echo '<option value="' . $term->slug . '"' . ($solution_related_tax==$term->slug ? 'selected' : '') . '>' . $term->name . ' (' . $term->count . ')</option>';
                    } ?>
                  </select>
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_solution_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/solution-page.php') {
    add_meta_box('po-meta-box', 'Solution Page Template Options', '_ws_solution_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_solution_meta');

// Save meta values
function _ws_save_solution_meta($post_id) {
  if (!isset($_POST['solution-nonce']) || !wp_verify_nonce($_POST['solution-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $solution_kpis = isset($_POST['solution-kpis']) ? $_POST['solution-kpis'] : '';
  update_post_meta($post_id, '_solution-kpis', $solution_kpis);

  $solution_kpi_btn_text = isset($_POST['solution-kpi-btn-text']) ? $_POST['solution-kpi-btn-text'] : '';
  update_post_meta($post_id, '_solution-kpi-btn-text', $solution_kpi_btn_text);

  $solution_kpi_btn_link = isset($_POST['solution-kpi-btn-link']) ? $_POST['solution-kpi-btn-link'] : '';
  update_post_meta($post_id, '_solution-kpi-btn-link', $solution_kpi_btn_link);

  $solution_suites_heading = isset($_POST['solution-suites-heading']) ? $_POST['solution-suites-heading'] : '';
  update_post_meta($post_id, '_solution-suites-heading', $solution_suites_heading);

  $solution_suites = isset($_POST['product-suites']) ? $_POST['product-suites'] : '';
  update_post_meta($post_id, '_solution-suites', $solution_suites);

  $solution_privacy_policy = isset($_POST['solution-privacy-policy']) ? $_POST['solution-privacy-policy'] : '';
  update_post_meta($post_id, '_solution-privacy-policy', $solution_privacy_policy);

  $solution_cta_heading = isset($_POST['solution-cta-heading']) ? $_POST['solution-cta-heading'] : '';
  update_post_meta($post_id, '_solution-cta-heading', $solution_cta_heading);

  $solution_cta_text = isset($_POST['solution-cta-text']) ? $_POST['solution-cta-text'] : '';
  update_post_meta($post_id, '_solution-cta-text', $solution_cta_text);

  $solution_cta_btn_text = isset($_POST['solution-cta-btn-text']) ? $_POST['solution-cta-btn-text'] : '';
  update_post_meta($post_id, '_solution-cta-btn-text', $solution_cta_btn_text);

  $solution_cta_btn_link = isset($_POST['solution-cta-btn-link']) ? $_POST['solution-cta-btn-link'] : '';
  update_post_meta($post_id, '_solution-cta-btn-link', $solution_cta_btn_link);

  $solution_cta_bg_img = isset($_POST['solution-cta-bg-img']) ? $_POST['solution-cta-bg-img'] : '';
  update_post_meta($post_id, '_solution-cta-bg-img', $solution_cta_bg_img);

  $solution_related_heading = isset($_POST['solution-related-heading']) ? $_POST['solution-related-heading'] : '';
  update_post_meta($post_id, '_solution-related-heading', $solution_related_heading);

  $solution_related_type = isset($_POST['solution-related-type']) ? $_POST['solution-related-type'] : '';
  update_post_meta($post_id, '_solution-related-type', $solution_related_type);

  $solution_related_post_one = isset($_POST['solution-related-post-one']) ? $_POST['solution-related-post-one'] : '';
  update_post_meta($post_id, '_solution-related-post-one', $solution_related_post_one);

  $solution_related_post_two = isset($_POST['solution-related-post-two']) ? $_POST['solution-related-post-two'] : '';
  update_post_meta($post_id, '_solution-related-post-two', $solution_related_post_two);

  $solution_related_post_three = isset($_POST['solution-related-post-three']) ? $_POST['solution-related-post-three'] : '';
  update_post_meta($post_id, '_solution-related-post-three', $solution_related_post_three);

  $solution_related_post_type = isset($_POST['solution-related-post-type']) ? $_POST['solution-related-post-type'] : '';
  update_post_meta($post_id, '_solution-related-post-type', $solution_related_post_type);

  $solution_related_tax = isset($_POST['solution-related-tax']) ? $_POST['solution-related-tax'] : '';
  update_post_meta($post_id, '_solution-related-tax', $solution_related_tax);
}
add_action('save_post', '_ws_save_solution_meta');
