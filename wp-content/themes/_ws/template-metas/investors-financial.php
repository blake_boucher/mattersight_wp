<?php
// Investors generic meta fields
function _ws_if_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'if-nonce');
  $if_cta_heading = get_post_meta(get_the_ID(), '_if-cta-heading', true);
  $if_cta_text = get_post_meta(get_the_ID(), '_if-cta-text', true);
  $if_cta_form = get_post_meta(get_the_ID(), '_if-cta-form', true);
  $if_cta_bg_img = get_post_meta(get_the_ID(), '_if-cta-bg-img', true); ?>
  <div id="if-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="if-cta-heading">Heading</label>
                      <input id="if-cta-heading" name="if-cta-heading" type="text" value="<?= $if_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="if-cta-text">Text</label>
                      <textarea id="if-cta-text" name="if-cta-text" class="text-editor"><?= $if_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="if-cta-form">Form (Card)</label>
                      <textarea id="if-cta-form" name="if-cta-form"><?= $if_cta_form; ?></textarea>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <label for="if-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#if-cta-bg-img">Select Image</button>
                  <input id="if-cta-bg-img" class="flex-1" name="if-cta-bg-img" type="text" value="<?= $if_cta_bg_img; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_if_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/investors-financial.php') {
    add_meta_box('if-meta-box', 'Investors Financial Template Options', '_ws_if_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_if_meta');

// Save meta values
function _ws_save_if_meta($post_id) {
  if (!isset($_POST['if-nonce']) || !wp_verify_nonce($_POST['if-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $if_cta_heading = isset($_POST['if-cta-heading']) ? $_POST['if-cta-heading'] : '';
  update_post_meta($post_id, '_if-cta-heading', $if_cta_heading);

  $if_cta_text = isset($_POST['if-cta-text']) ? $_POST['if-cta-text'] : '';
  update_post_meta($post_id, '_if-cta-text', $if_cta_text);

  $if_cta_form = isset($_POST['if-cta-form']) ? $_POST['if-cta-form'] : '';
  update_post_meta($post_id, '_if-cta-form', $if_cta_form);

  $if_cta_bg_img = isset($_POST['if-cta-bg-img']) ? $_POST['if-cta-bg-img'] : '';
  update_post_meta($post_id, '_if-cta-bg-img', $if_cta_bg_img);
}
add_action('save_post', '_ws_save_if_meta');
