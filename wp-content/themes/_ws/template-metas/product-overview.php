<?php
// Product overview meta fields
function _ws_po_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'po-nonce');
  $po_kpis = get_post_meta(get_the_ID(), '_po-kpis', true) ?: array();
  $po_list_heading = get_post_meta(get_the_ID(), '_po-list-heading', true);
  $po_featured_heading = get_post_meta(get_the_ID(), '_po-featured-heading', true);
  $po_featured_id = get_post_meta(get_the_ID(), '_po-featured-id', true);
  $po_testimonial = get_post_meta(get_the_ID(), '_po-testimonial', true);
  $po_cta_heading = get_post_meta(get_the_ID(), '_po-cta-heading', true);
  $po_cta_text = get_post_meta(get_the_ID(), '_po-cta-text', true);
  $po_cta_btn_text = get_post_meta(get_the_ID(), '_po-cta-btn-text', true);
  $po_cta_btn_link = get_post_meta(get_the_ID(), '_po-cta-btn-link', true);
  $po_cta_bg_img = get_post_meta(get_the_ID(), '_po-cta-bg-img', true); ?>
  <div id="po-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 1</legend>
            <ul>
              <li>
                <label for="po-kpis-1-svg">SVG</label>
                <input id="po-kpis-1-svg" name="po-kpis[0][svg]" class="svg-selector" type="text" value="<?= isset($po_kpis[0]['svg']) ? $po_kpis[0]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="po-kpis-1-title">Title</label>
                <input id="po-kpis-1-title" name="po-kpis[0][title]" type="text" value="<?= isset($po_kpis[0]['title']) ? $po_kpis[0]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="po-kpis-1-text">Text</label>
                <textarea id="po-kpis-1-text" name="po-kpis[0][text]"><?= isset($po_kpis[0]['text']) ? $po_kpis[0]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 2</legend>
            <ul>
              <li>
                <label for="po-kpis-2-svg">SVG</label>
                <input id="po-kpis-2-svg" name="po-kpis[1][svg]" class="svg-selector" type="text" value="<?= isset($po_kpis[1]['svg']) ? $po_kpis[1]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="po-kpis-2-title">Title</label>
                <input id="po-kpis-2-title" name="po-kpis[1][title]" type="text" value="<?= isset($po_kpis[1]['title']) ? $po_kpis[1]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="po-kpis-2-text">Text</label>
                <textarea id="po-kpis-2-text" name="po-kpis[1][text]"><?= isset($po_kpis[1]['text']) ? $po_kpis[1]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 3</legend>
            <ul>
              <li>
                <label for="po-kpis-3-svg">SVG</label>
                <input id="po-kpis-3-svg" name="po-kpis[2][svg]" class="svg-selector" type="text" value="<?= isset($po_kpis[2]['svg']) ? $po_kpis[2]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="po-kpis-3-title">Title</label>
                <input id="po-kpis-3-title" name="po-kpis[2][title]" type="text" value="<?= isset($po_kpis[2]['title']) ? $po_kpis[2]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="po-kpis-3-text">Text</label>
                <textarea id="po-kpis-3-text" name="po-kpis[2][text]"><?= isset($po_kpis[2]['text']) ? $po_kpis[2]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="po-list-heading">Overview List Heading</label>
          <input id="po-list-heading" name="po-list-heading" type="text" value="<?= $po_list_heading; ?>">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="po-featured-heading">Featured Resource Heading</label>
          <input id="po-featured-heading" name="po-featured-heading" type="text" value="<?= $po_featured_heading; ?>">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="po-featured-id">Featured Resource</label>
          <input id="po-featured-id" name="po-featured-id" class="post-selector" value="<?= $po_featured_id; ?>">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="po-testimonial">Testimonial</label>
          <input id="po-testimonial" name="po-testimonial" class="post-selector" post-type="testimonial" value="<?= $po_testimonial; ?>">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li>
                <label for="po-cta-heading">Heading</label>
                <input id="po-cta-heading" name="po-cta-heading" type="text" value="<?= $po_cta_heading; ?>">
              </li>
              <li>
                <label for="po-cta-text">Text</label>
                <textarea id="po-cta-text" name="po-cta-text" class="text-editor"><?= $po_cta_text; ?></textarea>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="po-cta-btn-text">Button Text</label>
                  <input id="po-cta-btn-text" name="po-cta-btn-text" type="text" value="<?= $po_cta_btn_text; ?>">
                </div>
                <div class="col-xs-6">
                  <label for="po-cta-btn-link">Button Link</label>
                  <input id="po-cta-btn-link" name="po-cta-btn-link" type="text" value="<?= $po_cta_btn_link; ?>">
                </div>
              </li>
              <li>
                <label for="po-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#po-cta-bg-img">Select Image</button>
                  <input id="po-cta-bg-img" name="po-cta-bg-img" class="flex-1" type="text" value="<?= $po_cta_bg_img; ?>">
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_po_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/product-overview.php') {
    add_meta_box('po-meta-box', 'Product Overview Template Options', '_ws_po_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_po_meta');

// Save meta values
function _ws_save_po_meta($post_id) {
  if (!isset($_POST['po-nonce']) || !wp_verify_nonce($_POST['po-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $po_kpis = isset($_POST['po-kpis']) ? $_POST['po-kpis'] : '';
  update_post_meta($post_id, '_po-kpis', $po_kpis);

  $po_list_heading = isset($_POST['po-list-heading']) ? $_POST['po-list-heading'] : '';
  update_post_meta($post_id, '_po-list-heading', $po_list_heading);

  $po_featured_heading = isset($_POST['po-featured-heading']) ? $_POST['po-featured-heading'] : '';
  update_post_meta($post_id, '_po-featured-heading', $po_featured_heading);

  $po_featured_id = isset($_POST['po-featured-id']) ? $_POST['po-featured-id'] : '';
  update_post_meta($post_id, '_po-featured-id', $po_featured_id);

  $po_testimonial = isset($_POST['po-testimonial']) ? $_POST['po-testimonial'] : '';
  update_post_meta($post_id, '_po-testimonial', $po_testimonial);

  $po_cta_heading = isset($_POST['po-cta-heading']) ? $_POST['po-cta-heading'] : '';
  update_post_meta($post_id, '_po-cta-heading', $po_cta_heading);

  $po_cta_text = isset($_POST['po-cta-text']) ? $_POST['po-cta-text'] : '';
  update_post_meta($post_id, '_po-cta-text', $po_cta_text);

  $po_cta_btn_text = isset($_POST['po-cta-btn-text']) ? $_POST['po-cta-btn-text'] : '';
  update_post_meta($post_id, '_po-cta-btn-text', $po_cta_btn_text);

  $po_cta_btn_link = isset($_POST['po-cta-btn-link']) ? $_POST['po-cta-btn-link'] : '';
  update_post_meta($post_id, '_po-cta-btn-link', $po_cta_btn_link);

  $po_cta_bg_img = isset($_POST['po-cta-bg-img']) ? $_POST['po-cta-bg-img'] : '';
  update_post_meta($post_id, '_po-cta-bg-img', $po_cta_bg_img);
}
add_action('save_post', '_ws_save_po_meta');
