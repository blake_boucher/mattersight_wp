<?php
// Front page meta fields
function _ws_fp_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'fp-nonce');
  $fp_overview_heading = get_post_meta(get_the_ID(), '_fp-overview-heading', true);
  $fp_overview_parent = get_post_meta(get_the_ID(), '_fp-overview-parent', true);
  $fp_kpis = get_post_meta(get_the_ID(), '_fp-kpis', true) ?: array();
  $fp_related_heading = get_post_meta(get_the_ID(), '_fp-related-heading', true);
  $fp_related_type = get_post_meta(get_the_ID(), '_fp-related-type', true);
  $fp_related_post_one = get_post_meta(get_the_ID(), '_fp-related-post-one', true);
  $fp_related_post_two = get_post_meta(get_the_ID(), '_fp-related-post-two', true);
  $fp_related_post_three = get_post_meta(get_the_ID(), '_fp-related-post-three', true);
  $fp_related_post_type = get_post_meta(get_the_ID(), '_fp-related-post-type', true);
  $fp_related_tax = get_post_meta(get_the_ID(), '_fp-related-tax', true);
  $fp_testimonial = get_post_meta(get_the_ID(), '_fp-testimonial', true);
  $fp_cta_heading = get_post_meta(get_the_ID(), '_fp-cta-heading', true);
  $fp_cta_text = get_post_meta(get_the_ID(), '_fp-cta-text', true);
  $fp_cta_btn_text = get_post_meta(get_the_ID(), '_fp-cta-btn-text', true);
  $fp_cta_btn_link = get_post_meta(get_the_ID(), '_fp-cta-btn-link', true);
  $fp_cta_bg_img = get_post_meta(get_the_ID(), '_fp-cta-bg-img', true); ?>
  <div id="fp-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Overview List</legend>
            <ul>
              <li>
                <label for="fp-overview-heading">Headline</label>
                <input id="fp-overview-heading" name="fp-overview-heading" type="text" value="<?= $fp_overview_heading; ?>" />
              </li>
              <li>
                <label for="fp-overview-parent">Parent Page</label>
                <input id="fp-overview-parent" name="fp-overview-parent" class="post-selector" value="<?= $fp_overview_parent; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 1</legend>
            <ul>
              <li>
                <label for="fp-kpis-1-title">SVG</label>
                <input id="fp-kpis-1-title" name="fp-kpis[0][svg]" class="svg-selector" type="text" value="<?= $fp_kpis[0]['svg']; ?>" />
              </li>
              <li>
                <label for="fp-kpis-1-text">Text</label>
                <input id="fp-kpis-1-text" name="fp-kpis[0][text]" type="text" value="<?= $fp_kpis[0]['text']; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 2</legend>
            <ul>
              <li>
                <label for="fp-kpis-2-title">SVG</label>
                <input id="fp-kpis-2-title" name="fp-kpis[1][svg]" class="svg-selector" type="text" value="<?= $fp_kpis[1]['svg']; ?>" />
              </li>
              <li>
                <label for="fp-kpis-2-text">Text</label>
                <input id="fp-kpis-2-text" name="fp-kpis[1][text]" type="text" value="<?= $fp_kpis[1]['text']; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 3</legend>
            <ul>
              <li>
                <label for="fp-kpis-3-title">SVG</label>
                <input id="fp-kpis-3-title" name="fp-kpis[2][svg]" class="svg-selector" type="text" value="<?= $fp_kpis[2]['svg']; ?>" />
              </li>
              <li>
                <label for="fp-kpis-3-text">Text</label>
                <input id="fp-kpis-3-text" name="fp-kpis[2][text]" type="text" value="<?= $fp_kpis[2]['text']; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 4</legend>
            <ul>
              <li>
                <label for="fp-kpis-4-title">SVG</label>
                <input id="fp-kpis-4-title" name="fp-kpis[3][svg]" class="svg-selector" type="text" value="<?= $fp_kpis[3]['svg']; ?>" />
              </li>
              <li>
                <label for="fp-kpis-4-text">Text</label>
                <input id="fp-kpis-4-text" name="fp-kpis[3][text]" type="text" value="<?= $fp_kpis[3]['text']; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row pb-related">
        <div class="col-xs-12">
          <fieldset>
            <legend>Related Content</legend>
            <ul>
              <li>
                <label for="fp-related-heading">Heading</label>
                <input id="fp-related-heading" name="fp-related-heading" type="text" value="<?= $fp_related_heading; ?>" >
              </li>
              <li>
                <input id="fp-related-type-posts" name="fp-related-type" type="radio" value="posts" <?= $fp_related_type=='posts' ? 'checked' : ''; ?> />
                <label for="fp-related-type-posts">Specific Posts</label>
                <div class="row">
                  <div class="col-xs-4">
                    <input id="fp-related-post-one" name="fp-related-post-one" class="post-selector" type="text" value="<?= $fp_related_post_one; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="fp-related-post-two" name="fp-related-post-two" class="post-selector" type="text" value="<?= $fp_related_post_two; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="fp-related-post-three" name="fp-related-post-three" class="post-selector" type="text" value="<?= $fp_related_post_three; ?>" />
                  </div>
                </div>
              </li>
              <li>
                <input id="fp-related-type-post-type" name="fp-related-type" type="radio" value="post-type" <?= $fp_related_type=='post-type' ? 'checked' : ''; ?> />
                <label for="fp-related-type-post-type">Post Type</label>
                <div>
                  <select id="fp-related-post-type" name="fp-related-post-type">
                    <?php
                    $types = get_post_types(array('public'=>true));
                    foreach ($types as $type) {
                      $t = get_post_type_object($type);
                      echo '<option value="' . $t->name . '" ' . ($fp_related_post_type==$t->name ? 'selected' : '') . '>' . $t->labels->name . '</option>';
                    } ?>
                  </select>
                </div>
              </li>
              <li>
                <input id="fp-related-type-tax" name="fp-related-type" type="radio" value="tax" <?= $fp_related_type=='tax' ? 'checked' : ''; ?> />
                <label for="fp-related-type-tax">Taxonomy</label>
                <div>
                  <select id="fp-related-tax" name="fp-related-tax">
                    <?php
                    $terms = get_terms('related_content');
                    foreach ($terms as $term) {
                      echo '<option value="' . $term->slug . '"' . ($fp_related_tax==$term->slug ? 'selected' : '') . '>' . $term->name . ' (' . $term->count . ')</option>';
                    } ?>
                  </select>
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="fp-testimonial">Testimonial</label>
          <input id="fp-testimonial" name="fp-testimonial" class="post-selector" post-type="testimonial" type="text" value="<?= $fp_testimonial; ?>">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li>
                <label for="fp-cta-heading">Heading</label>
                <input id="fp-cta-heading" name="fp-cta-heading" type="text" value="<?= $fp_cta_heading; ?>">
              </li>
              <li>
                <label for="fp-cta-text">Text</label>
                <textarea id="fp-cta-text" name="fp-cta-text" class="text-editor"><?= $fp_cta_text; ?></textarea>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="fp-cta-btn-text">Button Text</label>
                  <input id="fp-cta-btn-text" name="fp-cta-btn-text" type="text" value="<?= $fp_cta_btn_text; ?>">
                </div>
                <div class="col-xs-6">
                  <label for="fp-cta-btn-link">Button Link</label>
                  <input id="fp-cta-btn-link" name="fp-cta-btn-link" type="text" value="<?= $fp_cta_btn_link; ?>">
                </div>
              </li>
              <li>
                <label for="fp-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#fp-cta-bg-img">Select Image</button>
                  <input id="fp-cta-bg-img" name="fp-cta-bg-img" class="flex-1" type="text" value="<?= $fp_cta_bg_img; ?>">
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_fp_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/front-page.php') {
    add_meta_box('fp-meta-box', 'Front Page Template Options', '_ws_fp_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_fp_meta');

// Save meta values
function _ws_save_fp_meta($post_id) {
  if (!isset($_POST['fp-nonce']) || !wp_verify_nonce($_POST['fp-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $fp_overview_heading = isset($_POST['fp-overview-heading']) ? $_POST['fp-overview-heading'] : '';
  update_post_meta($post_id, '_fp-overview-heading', $fp_overview_heading);

  $fp_overview_parent = isset($_POST['fp-overview-parent']) ? $_POST['fp-overview-parent'] : '';
  update_post_meta($post_id, '_fp-overview-parent', $fp_overview_parent);

  $fp_kpis = isset($_POST['fp-kpis']) ? $_POST['fp-kpis'] : '';
  update_post_meta($post_id, '_fp-kpis', $fp_kpis);

  $fp_related_heading = isset($_POST['fp-related-heading']) ? $_POST['fp-related-heading'] : '';
  update_post_meta($post_id, '_fp-related-heading', $fp_related_heading);

  $fp_related_type = isset($_POST['fp-related-type']) ? $_POST['fp-related-type'] : '';
  update_post_meta($post_id, '_fp-related-type', $fp_related_type);

  $fp_related_post_one = isset($_POST['fp-related-post-one']) ? $_POST['fp-related-post-one'] : '';
  update_post_meta($post_id, '_fp-related-post-one', $fp_related_post_one);

  $fp_related_post_two = isset($_POST['fp-related-post-two']) ? $_POST['fp-related-post-two'] : '';
  update_post_meta($post_id, '_fp-related-post-two', $fp_related_post_two);

  $fp_related_post_three = isset($_POST['fp-related-post-three']) ? $_POST['fp-related-post-three'] : '';
  update_post_meta($post_id, '_fp-related-post-three', $fp_related_post_three);

  $fp_related_post_type = isset($_POST['fp-related-post-type']) ? $_POST['fp-related-post-type'] : '';
  update_post_meta($post_id, '_fp-related-post-type', $fp_related_post_type);

  $fp_related_tax = isset($_POST['fp-related-tax']) ? $_POST['fp-related-tax'] : '';
  update_post_meta($post_id, '_fp-related-tax', $fp_related_tax);

  $fp_testimonial = isset($_POST['fp-testimonial']) ? $_POST['fp-testimonial'] : '';
  update_post_meta($post_id, '_fp-testimonial', $fp_testimonial);

  $fp_cta_heading = isset($_POST['fp-cta-heading']) ? $_POST['fp-cta-heading'] : '';
  update_post_meta($post_id, '_fp-cta-heading', $fp_cta_heading);

  $fp_cta_text = isset($_POST['fp-cta-text']) ? $_POST['fp-cta-text'] : '';
  update_post_meta($post_id, '_fp-cta-text', $fp_cta_text);

  $fp_cta_btn_text = isset($_POST['fp-cta-btn-text']) ? $_POST['fp-cta-btn-text'] : '';
  update_post_meta($post_id, '_fp-cta-btn-text', $fp_cta_btn_text);

  $fp_cta_btn_link = isset($_POST['fp-cta-btn-link']) ? $_POST['fp-cta-btn-link'] : '';
  update_post_meta($post_id, '_fp-cta-btn-link', $fp_cta_btn_link);

  $fp_cta_bg_img = isset($_POST['fp-cta-bg-img']) ? $_POST['fp-cta-bg-img'] : '';
  update_post_meta($post_id, '_fp-cta-bg-img', $fp_cta_bg_img);
}
add_action('save_post', '_ws_save_fp_meta');
