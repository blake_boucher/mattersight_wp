<?php
// Personality labs meta fields
function _ws_pl_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'pl-nonce');
  $pl_top_form = get_post_meta(get_the_ID(), '_pl-top-form', true);
  $pl_featured_research = get_post_meta(get_the_ID(), '_pl-featured-research', true);
  $pl_research = get_post_meta(get_the_ID(), '_pl-research', true) ?: array();
  $pl_advisors = get_post_meta(get_the_ID(), '_pl-advisors', true) ?: array();
  $pl_cta_heading = get_post_meta(get_the_ID(), '_pl-cta-heading', true);
  $pl_cta_text = get_post_meta(get_the_ID(), '_pl-cta-text', true);
  $pl_cta_form = get_post_meta(get_the_ID(), '_pl-cta-form', true); ?>
  <div id="pl-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="pl-top-form">Banner Form</label>
          <textarea id="pl-top-form" name="pl-top-form"><?= $pl_top_form; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Research</legend>
            <ul>
              <li class="row">
                <div class="col-xs-12">
                  <label for="pl-featured-research">Featured Research</label>
                  <input id="pl-featured-research" class="post-selector" name="pl-featured-research" type="text" value="<?= $pl_featured_research; ?>" />
                </div>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="pl-research-1">Research 1</label>
                  <input id="pl-research-1" class="post-selector" name="pl-research[0]" type="text" value="<?= $pl_research[0]; ?>" />
                </div>
                <div class="col-xs-6">
                  <label for="pl-research-2">Research 2</label>
                  <input id="pl-research-2" class="post-selector" name="pl-research[1]" type="text" value="<?= $pl_research[1]; ?>" />
                </div>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="pl-research-3">Research 3</label>
                  <input id="pl-research-3" class="post-selector" name="pl-research[2]" type="text" value="<?= $pl_research[2]; ?>" />
                </div>
                <div class="col-xs-6">
                  <label for="pl-research-4">Research 4</label>
                  <input id="pl-research-4" class="post-selector" name="pl-research[3]" type="text" value="<?= $pl_research[3]; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <?php
        for ($i = 0; $i < 3; $i++) : ?>
          <div class="col-sm-4">
            <fieldset>
              <legend>Advisor <?= $i+1; ?></legend>
              <ul>
                <li>
                  <label for="pl-advisors-<?= $i; ?>-image">Image</label>
                  <div class="row">
                    <button class="button media-selector" target="#pl-advisors-<?= $i; ?>-image" size="standard">Select Image</button>
                    <input id="pl-advisors-<?= $i; ?>-image" name="pl-advisors[<?= $i; ?>][img]" class="flex-1" type="text" value="<?= isset($pl_advisors[$i]['img']) ? $pl_advisors[$i]['img'] : ''; ?>" />
                  </div>
                </li>
                <li>
                  <label for="pl-advisors-<?= $i; ?>-name">Name</label>
                  <input id="pl-advisors-<?= $i; ?>-name" name="pl-advisors[<?= $i; ?>][name]" type="text" value="<?= isset($pl_advisors[$i]['name']) ? $pl_advisors[$i]['name'] : ''; ?>" />
                </li>
                <li>
                  <label for="pl-advisors-<?= $i; ?>-name">Title</label>
                  <input id="pl-advisors-<?= $i; ?>-name" name="pl-advisors[<?= $i; ?>][title]" type="text" value="<?= isset($pl_advisors[$i]['title']) ? $pl_advisors[$i]['title'] : ''; ?>" />
                </li>
                <li>
                  <label for="pl-advisors-<?= $i; ?>-bio">Bio</label>
                  <textarea id="pl-advisors-<?= $i; ?>-bio" name="pl-advisors[<?= $i; ?>][bio]" class="text-editor"><?= isset($pl_advisors[$i]['bio']) ? $pl_advisors[$i]['bio'] : ''; ?></textarea>
                </li>
              </ul>
            </fieldset>
          </div>
        <?php
        endfor; ?>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="pl-cta-heading">CTA Heading</label>
                      <input id="pl-cta-heading" name="pl-cta-heading" type="text" value="<?= $pl_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="pl-cta-text">CTA Text</label>
                      <textarea id="pl-cta-text" class="text-editor" name="pl-cta-text"><?= $pl_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <label for="pl-cta-form">CTA Text</label>
                  <textarea id="pl-cta-form" name="pl-cta-form"><?= $pl_cta_form; ?></textarea>
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_pl_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/personality-labs.php') {
    add_meta_box('pl-meta-box', 'Gallery Template Options', '_ws_pl_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_pl_meta');

// Save meta values
function _ws_save_pl_meta($post_id) {
  if (!isset($_POST['pl-nonce']) || !wp_verify_nonce($_POST['pl-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $pl_top_form = isset($_POST['pl-top-form']) ? $_POST['pl-top-form'] : '';
  update_post_meta($post_id, '_pl-top-form', $pl_top_form);

  $pl_advisors = isset($_POST['pl-advisors']) ? $_POST['pl-advisors'] : '';
  update_post_meta($post_id, '_pl-advisors', $pl_advisors);

  $pl_featured_research = isset($_POST['pl-featured-research']) ? $_POST['pl-featured-research'] : '';
  update_post_meta($post_id, '_pl-featured-research', $pl_featured_research);

  $pl_research = isset($_POST['pl-research']) ? $_POST['pl-research'] : '';
  update_post_meta($post_id, '_pl-research', $pl_research);

  $pl_cta_heading = isset($_POST['pl-cta-heading']) ? $_POST['pl-cta-heading'] : '';
  update_post_meta($post_id, '_pl-cta-heading', $pl_cta_heading);

  $pl_cta_text = isset($_POST['pl-cta-text']) ? $_POST['pl-cta-text'] : '';
  update_post_meta($post_id, '_pl-cta-text', $pl_cta_text);

  $pl_cta_form = isset($_POST['pl-cta-form']) ? $_POST['pl-cta-form'] : '';
  update_post_meta($post_id, '_pl-cta-form', $pl_cta_form);
}
add_action('save_post', '_ws_save_pl_meta');
