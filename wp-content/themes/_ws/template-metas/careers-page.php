<?php
// Careers page meta fields
function _ws_careers_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'careers-nonce');
  $careers_personality_heading = get_post_meta(get_the_ID(), '_careers-personality-heading', true);
  $careers_personality_text = get_post_meta(get_the_ID(), '_careers-personality-text', true);
  $careers_personality_items = get_post_meta(get_the_ID(), '_careers-personality-items', true) ?: array();
  $careers_checkerboard_heading = get_post_meta(get_the_ID(), '_careers-checkerboard-heading', true);
  $careers_checkerboard_text_1 = get_post_meta(get_the_ID(), '_careers-checkerboard-text-1', true);
  $careers_checkerboard_img_1 = get_post_meta(get_the_ID(), '_careers-checkerboard-img-1', true);
  $careers_checkerboard_text_2 = get_post_meta(get_the_ID(), '_careers-checkerboard-text-2', true);
  $careers_checkerboard_img_2 = get_post_meta(get_the_ID(), '_careers-checkerboard-img-2', true);
  $careers_checkerboard_text_3 = get_post_meta(get_the_ID(), '_careers-checkerboard-text-3', true);
  $careers_checkerboard_img_3 = get_post_meta(get_the_ID(), '_careers-checkerboard-img-3', true);
  $careers_checkerboard_text_4 = get_post_meta(get_the_ID(), '_careers-checkerboard-text-4', true);
  $careers_checkerboard_img_4 = get_post_meta(get_the_ID(), '_careers-checkerboard-img-4', true);
  $careers_checkerboard_text_5 = get_post_meta(get_the_ID(), '_careers-checkerboard-text-5', true);
  $careers_img = get_post_meta(get_the_ID(), '_careers-img', true);
  $careers_testimonial = get_post_meta(get_the_ID(), '_careers-testimonial', true);
  $careers_job_heading = get_post_meta(get_the_ID(), '_careers-job-heading', true);
  $careers_job_1 = get_post_meta(get_the_ID(), '_careers-job-1', true);
  $careers_job_2 = get_post_meta(get_the_ID(), '_careers-job-2', true);
  $careers_job_3 = get_post_meta(get_the_ID(), '_careers-job-3', true); ?>
  <div id="careers-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Personalities Section</legend>
            <ul>
              <li>
                <label for="careers-personality-heading">Heading</label>
                <input id="careers-personality-heading" name="careers-personality-heading" type="text" value="<?= $careers_personality_heading; ?>" />
              </li>
              <li>
                <label for="careers-personality-text">Text</label>
                <textarea id="careers-personality-text" name="careers-personality-text" class="text-editor"><?= $careers_personality_text; ?></textarea>
              </li>
              <li>
                <label>Items</label>
                <ul class="sortable-container">
                  <?php
                  foreach ($careers_personality_items as $i=>$item) : ?>
                    <li class="sortable-item">
                      <div class="sortable-header">
                        <span class="dashicons dashicons-move sortable-handle"></span>
                        <span class="dashicons dashicons-trash sortable-delete"></span>
                      </div>
                      <ul class="sortable-content">
                        <li>
                          <label for="careers-personality-items-<?= $i; ?>-svg">Items</label>
                          <input id="careers-personality-items-<?= $i; ?>-svg" name="careers-personality-items[<?= $i; ?>][svg]" class="svg-selector" type="text" value="<?= $item['svg']; ?>" />
                        </li>
                        <li>
                          <label for="careers-personality-items-<?= $i; ?>-heading">Heading</label>
                          <input id="careers-personality-items-<?= $i; ?>-heading" name="careers-personality-items[<?= $i; ?>][heading]" type="text" value="<?= $item['heading']; ?>" />
                        </li>
                        <li>
                          <label for="careers-personality-items-<?= $i; ?>-text">Text</label>
                          <textarea id="careers-personality-items-<?= $i; ?>-text" name="careers-personality-items[<?= $i; ?>][text]"><?= $item['text']; ?></textarea>
                        </li>
                      </ul>
                    </li>
                  <?php
                  endforeach; ?>
                </ul>
                <button id="add-careers-personality-item" class="button">Add Item</button>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Checkerboard Section</legend>
            <ul>
              <li>
                <label for="careers-checkerboard-heading">Heading</label>
                <input id="careers-checkerboard-heading" name="careers-checkerboard-heading" type="text" value="<?= $careers_checkerboard_heading; ?>" />
              </li>
              <li>
                <label for="careers-checkerboard-text-1">Text 1</label>
                <textarea id="careers-checkerboard-text-1" name="careers-checkerboard-text-1" class="text-editor"><?= $careers_checkerboard_text_1; ?></textarea>
              </li>
              <li>
                <label for="careers-checkerboard-photo-1">Image 1</label>
                <div class="row">
                  <button class="button media-selector" target="#careers-checkerboard-img-1">Select Image</button>
                  <input id="careers-checkerboard-img-1" class="flex-1" name="careers-checkerboard-img-1" type="text" value="<?= $careers_checkerboard_img_1; ?>" />
                </div>
              </li>
              <li>
                <label for="careers-checkerboard-text-2">Text 2</label>
                <textarea id="careers-checkerboard-text-2" name="careers-checkerboard-text-2" class="text-editor"><?= $careers_checkerboard_text_2; ?></textarea>
              </li>
              <li>
                <label for="careers-checkerboard-img-2">Image 2</label>
                <div class="row">
                  <button class="button media-selector" target="#careers-checkerboard-img-2">Select Image</button>
                  <input id="careers-checkerboard-img-2" class="flex-1" name="careers-checkerboard-img-2" type="text" value="<?= $careers_checkerboard_img_2; ?>" />
                </div>
              </li>
              <li>
                <label for="careers-checkerboard-text-3">Text 3</label>
                <textarea id="careers-checkerboard-text-3" name="careers-checkerboard-text-3" class="text-editor"><?= $careers_checkerboard_text_3; ?></textarea>
              </li>
              <li>
                <label for="careers-checkerboard-img-3">Image 3</label>
                <div class="row">
                  <button class="button media-selector" target="#careers-checkerboard-img-3">Select Image</button>
                  <input id="careers-checkerboard-img-3" class="flex-1" name="careers-checkerboard-img-3" type="text" value="<?= $careers_checkerboard_img_3; ?>" />
                </div>
              </li>
              <li>
                <label for="careers-checkerboard-text-4">Text 4</label>
                <textarea id="careers-checkerboard-text-4" name="careers-checkerboard-text-4" class="text-editor"><?= $careers_checkerboard_text_4; ?></textarea>
              </li>
              <li>
                <label for="careers-checkerboard-img-4">Image 4</label>
                <div class="row">
                  <button class="button media-selector" target="#careers-checkerboard-img-4">Select Image</button>
                  <input id="careers-checkerboard-img-4" class="flex-1" name="careers-checkerboard-img-4" type="text" value="<?= $careers_checkerboard_img_4; ?>" />
                </div>
              </li>
              <li>
                <label for="careers-checkerboard-text-5">Text 5</label>
                <textarea id="careers-checkerboard-text-5" name="careers-checkerboard-text-5" class="text-editor"><?= $careers_checkerboard_text_5; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="careers-img">Image</label>
          <div class="row">
            <button class="button media-selector" target="#careers-img">Select Image</button>
            <input id="careers-img" class="flex-1" name="careers-img" type="text" value="<?= $careers_img; ?>" />
          </div>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="careers-testimonial">Testimonial</label>
          <input id="careers-testimonial" name="careers-testimonial" class="post-selector" post-type="testimonial" value="<?= $careers_testimonial; ?>">
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Featured Jobs</legend>
            <ul>
              <li class="row">
                <div class="col-xs-12">
                  <label for="careers-job-heading">Heading</label>
                  <input id="careers-job-heading" name="careers-job-heading" type="text" value="<?= $careers_job_heading; ?>">
                </div>
              </li>
              <li class="row">
                <div class="col-xs-4">
                  <label for="careers-job-1">Job 1</label>
                  <input id="careers-job-1" name="careers-job-1" class="post-selector" post-type="job" value="<?= $careers_job_1; ?>">
                </div>
                <div class="col-xs-4">
                  <label for="careers-job-2">Job 2</label>
                  <input id="careers-job-2" name="careers-job-2" class="post-selector" post-type="job" value="<?= $careers_job_2; ?>">
                </div>
                <div class="col-xs-4">
                  <label for="careers-job-3">Job 3</label>
                  <input id="careers-job-3" name="careers-job-3" class="post-selector" post-type="job" value="<?= $careers_job_3; ?>">
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_careers_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/careers-page.php') {
    add_meta_box('careers-meta-box', 'Careers Page Template Options', '_ws_careers_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_careers_meta');

// Save meta values
function _ws_save_careers_meta($post_id) {
  if (!isset($_POST['careers-nonce']) || !wp_verify_nonce($_POST['careers-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $careers_personality_heading = isset($_POST['careers-personality-heading']) ? $_POST['careers-personality-heading'] : '';
  update_post_meta($post_id, '_careers-personality-heading', $careers_personality_heading);

  $careers_personality_text = isset($_POST['careers-personality-text']) ? $_POST['careers-personality-text'] : '';
  update_post_meta($post_id, '_careers-personality-text', $careers_personality_text);

  $careers_personality_items = isset($_POST['careers-personality-items']) ? $_POST['careers-personality-items'] : '';
  update_post_meta($post_id, '_careers-personality-items', $careers_personality_items);

  $careers_checkerboard_heading = isset($_POST['careers-checkerboard-heading']) ? $_POST['careers-checkerboard-heading'] : '';
  update_post_meta($post_id, '_careers-checkerboard-heading', $careers_checkerboard_heading);

  $careers_checkerboard_text_1 = isset($_POST['careers-checkerboard-text-1']) ? $_POST['careers-checkerboard-text-1'] : '';
  update_post_meta($post_id, '_careers-checkerboard-text-1', $careers_checkerboard_text_1);

  $careers_checkerboard_img_1 = isset($_POST['careers-checkerboard-img-1']) ? $_POST['careers-checkerboard-img-1'] : '';
  update_post_meta($post_id, '_careers-checkerboard-img-1', $careers_checkerboard_img_1);

  $careers_checkerboard_text_2 = isset($_POST['careers-checkerboard-text-2']) ? $_POST['careers-checkerboard-text-2'] : '';
  update_post_meta($post_id, '_careers-checkerboard-text-2', $careers_checkerboard_text_2);

  $careers_checkerboard_img_2 = isset($_POST['careers-checkerboard-img-2']) ? $_POST['careers-checkerboard-img-2'] : '';
  update_post_meta($post_id, '_careers-checkerboard-img-2', $careers_checkerboard_img_2);

  $careers_checkerboard_text_3 = isset($_POST['careers-checkerboard-text-3']) ? $_POST['careers-checkerboard-text-3'] : '';
  update_post_meta($post_id, '_careers-checkerboard-text-3', $careers_checkerboard_text_3);

  $careers_checkerboard_img_3 = isset($_POST['careers-checkerboard-img-3']) ? $_POST['careers-checkerboard-img-3'] : '';
  update_post_meta($post_id, '_careers-checkerboard-img-3', $careers_checkerboard_img_3);

  $careers_checkerboard_text_4 = isset($_POST['careers-checkerboard-text-4']) ? $_POST['careers-checkerboard-text-4'] : '';
  update_post_meta($post_id, '_careers-checkerboard-text-4', $careers_checkerboard_text_4);

  $careers_checkerboard_img_4 = isset($_POST['careers-checkerboard-img-4']) ? $_POST['careers-checkerboard-img-4'] : '';
  update_post_meta($post_id, '_careers-checkerboard-img-4', $careers_checkerboard_img_4);

  $careers_checkerboard_text_5 = isset($_POST['careers-checkerboard-text-5']) ? $_POST['careers-checkerboard-text-5'] : '';
  update_post_meta($post_id, '_careers-checkerboard-text-5', $careers_checkerboard_text_5);

  $careers_img = isset($_POST['careers-img']) ? $_POST['careers-img'] : '';
  update_post_meta($post_id, '_careers-img', $careers_img);

  $careers_testimonial = isset($_POST['careers-testimonial']) ? $_POST['careers-testimonial'] : '';
  update_post_meta($post_id, '_careers-testimonial', $careers_testimonial);

  $careers_job_heading = isset($_POST['careers-job-heading']) ? $_POST['careers-job-heading'] : '';
  update_post_meta($post_id, '_careers-job-heading', $careers_job_heading);

  $careers_job_1 = isset($_POST['careers-job-1']) ? $_POST['careers-job-1'] : '';
  update_post_meta($post_id, '_careers-job-1', $careers_job_1);

  $careers_job_2 = isset($_POST['careers-job-2']) ? $_POST['careers-job-2'] : '';
  update_post_meta($post_id, '_careers-job-2', $careers_job_2);

  $careers_job_3 = isset($_POST['careers-job-3']) ? $_POST['careers-job-3'] : '';
  update_post_meta($post_id, '_careers-job-3', $careers_job_3);
}
add_action('save_post', '_ws_save_careers_meta');
