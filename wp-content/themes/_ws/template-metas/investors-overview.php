<?php
// Investors overview meta fields
function _ws_io_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'io-nonce');
  $io_files = get_post_meta(get_the_ID(), '_io-files', true) ?: array();
  $io_cta_heading = get_post_meta(get_the_ID(), '_io-cta-heading', true);
  $io_cta_text = get_post_meta(get_the_ID(), '_io-cta-text', true);
  $io_cta_form = get_post_meta(get_the_ID(), '_io-cta-form', true);
  $io_cta_bg_img = get_post_meta(get_the_ID(), '_io-cta-bg-img', true); ?>
  <div id="io-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Files</legend>
            <ul class="sortable-container">
              <?php
              foreach ($io_files as $i=>$file) : ?>
                <li class="sortable-item">
                  <div class="sortable-header">
                    <span class="dashicons dashicons-move sortable-handle"></span>
                    <span class="dashicons dashicons-trash sortable-delete"></span>
                  </div>
                  <ul class="sortable-content">
                    <li class="row">
                      <div class="col-xs-6">
                        <label for="io-files-<?= $i; ?>-text">Text</label>
                        <input id="io-files-<?= $i; ?>-text" name="io-files[<?= $i; ?>][text]" type="text" value="<?= $file['text']; ?>" />
                      </div>
                      <div class="col-xs-6">
                        <label for="io-files-<?= $i; ?>-link">Link</label>
                        <div class="row">
                          <button class="button media-selector" target="#io-files-<?= $i; ?>-link">Select File</button>
                          <input id="io-files-<?= $i; ?>-link" class="flex-1" name="io-files[<?= $i; ?>][link]" type="text" value="<?= $file['link']; ?>" />
                        </div>
                      </div>
                    </li>
                  </ul>
                </li>
              <?php
              endforeach; ?>
            </ul>
            <button id="add-io-file" class="button">Add File</button>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="io-cta-heading">Heading</label>
                      <input id="io-cta-heading" name="io-cta-heading" type="text" value="<?= $io_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="io-cta-text">Text</label>
                      <textarea id="io-cta-text" name="io-cta-text" class="text-editor"><?= $io_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="io-cta-form">Form (Card)</label>
                      <textarea id="io-cta-form" name="io-cta-form"><?= $io_cta_form; ?></textarea>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <label for="io-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#io-cta-bg-img">Select Image</button>
                  <input id="io-cta-bg-img" class="flex-1" name="io-cta-bg-img" type="text" value="<?= $io_cta_bg_img; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_io_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/investors-overview.php') {
    add_meta_box('io-meta-box', 'Investors Overview Template Options', '_ws_io_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_io_meta');

// Save meta values
function _ws_save_io_meta($post_id) {
  if (!isset($_POST['io-nonce']) || !wp_verify_nonce($_POST['io-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $io_files = isset($_POST['io-files']) ? $_POST['io-files'] : '';
  update_post_meta($post_id, '_io-files', $io_files);

  $io_cta_heading = isset($_POST['io-cta-heading']) ? $_POST['io-cta-heading'] : '';
  update_post_meta($post_id, '_io-cta-heading', $io_cta_heading);

  $io_cta_text = isset($_POST['io-cta-text']) ? $_POST['io-cta-text'] : '';
  update_post_meta($post_id, '_io-cta-text', $io_cta_text);

  $io_cta_form = isset($_POST['io-cta-form']) ? $_POST['io-cta-form'] : '';
  update_post_meta($post_id, '_io-cta-form', $io_cta_form);

  $io_cta_bg_img = isset($_POST['io-cta-bg-img']) ? $_POST['io-cta-bg-img'] : '';
  update_post_meta($post_id, '_io-cta-bg-img', $io_cta_bg_img);
}
add_action('save_post', '_ws_save_io_meta');
