<?php
// Calculator meta fields
function _ws_calculator_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'calculator-nonce');
  $calc_service_intro = get_post_meta(get_the_ID(), '_calc-service-intro', true);
  $calc_sales_intro = get_post_meta(get_the_ID(), '_calc-sales-intro', true);
  $calc_service_form_heading = get_post_meta(get_the_ID(), '_calc-service-form-heading', true);
  $calc_sales_form_heading = get_post_meta(get_the_ID(), '_calc-sales-form-heading', true);
  $calc_service_form_text = get_post_meta(get_the_ID(), '_calc-service-form-text', true);
  $calc_sales_form_text = get_post_meta(get_the_ID(), '_calc-sales-form-text', true);
  $calc_service_form = get_post_meta(get_the_ID(), '_calc-service-form', true);
  $calc_sales_form = get_post_meta(get_the_ID(), '_calc-sales-form', true);
  $calc_thank_you = get_post_meta(get_the_ID(), '_calc-thank-you', true);
  $calc_related_heading = get_post_meta(get_the_ID(), '_calc-related-heading', true);
  $calc_related_type = get_post_meta(get_the_ID(), '_calc-related-type', true);
  $calc_related_post_one = get_post_meta(get_the_ID(), '_calc-related-post-one', true);
  $calc_related_post_two = get_post_meta(get_the_ID(), '_calc-related-post-two', true);
  $calc_related_post_three = get_post_meta(get_the_ID(), '_calc-related-post-three', true);
  $calc_related_post_type = get_post_meta(get_the_ID(), '_calc-related-post-type', true);
  $calc_related_tax = get_post_meta(get_the_ID(), '_calc-related-tax', true); ?>
  <div id="calc-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-sm-6">
          <label for="calc-service-intro">Service Intro Text</label>
          <textarea id="calc-service-intro" name="calc-service-intro" class="text-editor"><?= $calc_service_intro; ?></textarea>
        </div>
        <div class="col-sm-6">
          <label for="calc-sales-intro">Sales Intro Text</label>
          <textarea id="calc-sales-intro" name="calc-sales-intro" class='text-editor'><?= $calc_sales_intro; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-sm-6">
          <label for="calc-service-form-heading">Service Form Heading</label>
          <textarea id="calc-service-form-heading" name="calc-service-form-heading"><?= $calc_service_form_heading; ?></textarea>
        </div>
        <div class="col-sm-6">
          <label for="calc-sales-form-heading">Sales Form Heading</label>
          <textarea id="calc-sales-form-heading" name="calc-sales-form-heading"><?= $calc_sales_form_heading; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-sm-6">
          <label for="calc-service-form-text">Service Form Text</label>
          <textarea id="calc-service-form-text" name="calc-service-form-text" class="text-editor"><?= $calc_service_form_text; ?></textarea>
        </div>
        <div class="col-sm-6">
          <label for="calc-sales-form-text">Sales Form Text</label>
          <textarea id="calc-sales-form-text" name="calc-sales-form-text" class="text-editor"><?= $calc_sales_form_text; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-sm-6">
          <label for="calc-service-form">Service Form</label>
          <textarea id="calc-service-form" name="calc-service-form"><?= $calc_service_form; ?></textarea>
        </div>
        <div class="col-sm-6">
          <label for="calc-sales-form">Sales Form</label>
          <textarea id="calc-sales-form" name="calc-sales-form"><?= $calc_sales_form; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <label for="calc-thank-you">Thank You Page</label>
          <textarea id="calc-thank-you" name="calc-thank-you" class="text-editor"><?= $calc_thank_you; ?></textarea>
        </div>
      </li>
      <li class="row pb-related">
        <div class="col-xs-12">
          <fieldset>
            <legend>Related Content</legend>
            <ul>
              <li>
                <label for="calc-related-heading">Heading</label>
                <input id="calc-related-heading" name="calc-related-heading" type="text" value="<?= $calc_related_heading; ?>" >
              </li>
              <li>
                <input id="calc-related-type-posts" name="calc-related-type" type="radio" value="posts" <?= $calc_related_type=='posts' ? 'checked' : ''; ?> />
                <label for="calc-related-type-posts">Specific Posts</label>
                <div class="row">
                  <div class="col-xs-4">
                    <input id="calc-related-post-one" name="calc-related-post-one" class="post-selector" type="text" value="<?= $calc_related_post_one; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="calc-related-post-two" name="calc-related-post-two" class="post-selector" type="text" value="<?= $calc_related_post_two; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="calc-related-post-three" name="calc-related-post-three" class="post-selector" type="text" value="<?= $calc_related_post_three; ?>" />
                  </div>
                </div>
              </li>
              <li>
                <input id="calc-related-type-post-type" name="calc-related-type" type="radio" value="post-type" <?= $calc_related_type=='post-type' ? 'checked' : ''; ?> />
                <label for="calc-related-type-post-type">Post Type</label>
                <div>
                  <select id="calc-related-post-type" name="calc-related-post-type">
                    <?php
                    $types = get_post_types(array('public'=>true));
                    foreach ($types as $type) {
                      $t = get_post_type_object($type);
                      echo '<option value="' . $t->name . '" ' . ($calc_related_post_type==$t->name ? 'selected' : '') . '>' . $t->labels->name . '</option>';
                    } ?>
                  </select>
                </div>
              </li>
              <li>
                <input id="calc-related-type-tax" name="calc-related-type" type="radio" value="tax" <?= $calc_related_type=='tax' ? 'checked' : ''; ?> />
                <label for="calc-related-type-tax">Taxonomy</label>
                <div>
                  <select id="calc-related-tax" name="calc-related-tax">
                    <?php
                    $terms = get_terms('related_content');
                    foreach ($terms as $term) {
                      echo '<option value="' . $term->slug . '"' . ($calc_related_tax==$term->slug ? 'selected' : '') . '>' . $term->name . ' (' . $term->count . ')</option>';
                    } ?>
                  </select>
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_calculator_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/calculator.php') {
    add_meta_box('calculator-meta-box', 'Calculator Template Options', '_ws_calculator_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_calculator_meta');

// Save meta values
function _ws_save_calculator_meta($post_id) {
  if (!isset($_POST['calculator-nonce']) || !wp_verify_nonce($_POST['calculator-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $calc_service_intro = isset($_POST['calc-service-intro']) ? $_POST['calc-service-intro'] : '';
  update_post_meta($post_id, '_calc-service-intro', $calc_service_intro);

  $calc_sales_intro = isset($_POST['calc-sales-intro']) ? $_POST['calc-sales-intro'] : '';
  update_post_meta($post_id, '_calc-sales-intro', $calc_sales_intro);

  $calc_service_form_heading = isset($_POST['calc-service-form-heading']) ? $_POST['calc-service-form-heading'] : '';
  update_post_meta($post_id, '_calc-service-form-heading', $calc_service_form_heading);

  $calc_sales_form_heading = isset($_POST['calc-sales-form-heading']) ? $_POST['calc-sales-form-heading'] : '';
  update_post_meta($post_id, '_calc-sales-form-heading', $calc_sales_form_heading);

  $calc_service_form_text = isset($_POST['calc-service-form-text']) ? $_POST['calc-service-form-text'] : '';
  update_post_meta($post_id, '_calc-service-form-text', $calc_service_form_text);

  $calc_sales_form_text = isset($_POST['calc-sales-form-text']) ? $_POST['calc-sales-form-text'] : '';
  update_post_meta($post_id, '_calc-sales-form-text', $calc_sales_form_text);

  $calc_service_form = isset($_POST['calc-service-form']) ? $_POST['calc-service-form'] : '';
  update_post_meta($post_id, '_calc-service-form', $calc_service_form);

  $calc_sales_form = isset($_POST['calc-sales-form']) ? $_POST['calc-sales-form'] : '';
  update_post_meta($post_id, '_calc-sales-form', $calc_sales_form);

  $calc_thank_you = isset($_POST['calc-thank-you']) ? $_POST['calc-thank-you'] : '';
  update_post_meta($post_id, '_calc-thank-you', $calc_thank_you);

  $calc_related_heading = isset($_POST['calc-related-heading']) ? $_POST['calc-related-heading'] : '';
  update_post_meta($post_id, '_calc-related-heading', $calc_related_heading);

  $calc_related_type = isset($_POST['calc-related-type']) ? $_POST['calc-related-type'] : '';
  update_post_meta($post_id, '_calc-related-type', $calc_related_type);

  $calc_related_post_one = isset($_POST['calc-related-post-one']) ? $_POST['calc-related-post-one'] : '';
  update_post_meta($post_id, '_calc-related-post-one', $calc_related_post_one);

  $calc_related_post_two = isset($_POST['calc-related-post-two']) ? $_POST['calc-related-post-two'] : '';
  update_post_meta($post_id, '_calc-related-post-two', $calc_related_post_two);

  $calc_related_post_three = isset($_POST['calc-related-post-three']) ? $_POST['calc-related-post-three'] : '';
  update_post_meta($post_id, '_calc-related-post-three', $calc_related_post_three);

  $calc_related_post_type = isset($_POST['calc-related-post-type']) ? $_POST['calc-related-post-type'] : '';
  update_post_meta($post_id, '_calc-related-post-type', $calc_related_post_type);

  $calc_related_tax = isset($_POST['calc-related-tax']) ? $_POST['calc-related-tax'] : '';
  update_post_meta($post_id, '_calc-related-tax', $calc_related_tax);
}
add_action('save_post', '_ws_save_calculator_meta');
