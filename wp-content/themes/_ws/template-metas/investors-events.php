<?php
// Investors generic meta fields
function _ws_ie_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'ie-nonce');
  $ie_cta_heading = get_post_meta(get_the_ID(), '_ie-cta-heading', true);
  $ie_cta_text = get_post_meta(get_the_ID(), '_ie-cta-text', true);
  $ie_cta_form = get_post_meta(get_the_ID(), '_ie-cta-form', true);
  $ie_cta_bg_img = get_post_meta(get_the_ID(), '_ie-cta-bg-img', true); ?>
  <div id="ie-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="ie-cta-heading">Heading</label>
                      <input id="ie-cta-heading" name="ie-cta-heading" type="text" value="<?= $ie_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="ie-cta-text">Text</label>
                      <textarea id="ie-cta-text" name="ie-cta-text" class="text-editor"><?= $ie_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="ie-cta-form">Form (Card)</label>
                      <textarea id="ie-cta-form" name="ie-cta-form"><?= $ie_cta_form; ?></textarea>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <label for="ie-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#ie-cta-bg-img">Select Image</button>
                  <input id="ie-cta-bg-img" class="flex-1" name="ie-cta-bg-img" type="text" value="<?= $ie_cta_bg_img; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_ie_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/investors-events.php') {
    add_meta_box('ie-meta-box', 'Investors Events Template Options', '_ws_ie_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_ie_meta');

// Save meta values
function _ws_save_ie_meta($post_id) {
  if (!isset($_POST['ie-nonce']) || !wp_verify_nonce($_POST['ie-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $ie_cta_heading = isset($_POST['ie-cta-heading']) ? $_POST['ie-cta-heading'] : '';
  update_post_meta($post_id, '_ie-cta-heading', $ie_cta_heading);

  $ie_cta_text = isset($_POST['ie-cta-text']) ? $_POST['ie-cta-text'] : '';
  update_post_meta($post_id, '_ie-cta-text', $ie_cta_text);

  $ie_cta_form = isset($_POST['ie-cta-form']) ? $_POST['ie-cta-form'] : '';
  update_post_meta($post_id, '_ie-cta-form', $ie_cta_form);

  $ie_cta_bg_img = isset($_POST['ie-cta-bg-img']) ? $_POST['ie-cta-bg-img'] : '';
  update_post_meta($post_id, '_ie-cta-bg-img', $ie_cta_bg_img);
}
add_action('save_post', '_ws_save_ie_meta');
