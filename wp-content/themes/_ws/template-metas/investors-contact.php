<?php
// Investors generic meta fields
function _ws_ic_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'ic-nonce');
  $ic_quote = get_post_meta(get_the_ID(), '_ic-quote', true);
  $ic_cta_heading = get_post_meta(get_the_ID(), '_ic-cta-heading', true);
  $ic_cta_text = get_post_meta(get_the_ID(), '_ic-cta-text', true);
  $ic_cta_form = get_post_meta(get_the_ID(), '_ic-cta-form', true);
  $ic_cta_bg_img = get_post_meta(get_the_ID(), '_ic-cta-bg-img', true); ?>
  <div id="ic-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <label for="ic-quote">Quote <small>(right column content)</small></label>
          <textarea id="ic-quote" name="ic-quote" class="text-editor"><?= $ic_quote; ?></textarea>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li class="row">
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="ic-cta-heading">Heading</label>
                      <input id="ic-cta-heading" name="ic-cta-heading" type="text" value="<?= $ic_cta_heading; ?>" />
                    </li>
                    <li>
                      <label for="ic-cta-text">Text</label>
                      <textarea id="ic-cta-text" name="ic-cta-text" class="text-editor"><?= $ic_cta_text; ?></textarea>
                    </li>
                  </ul>
                </div>
                <div class="col-xs-6">
                  <ul>
                    <li>
                      <label for="ic-cta-form">Form (Card)</label>
                      <textarea id="ic-cta-form" name="ic-cta-form"><?= $ic_cta_form; ?></textarea>
                    </li>
                  </ul>
                </div>
              </li>
              <li>
                <label for="ic-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#ic-cta-bg-img">Select Image</button>
                  <input id="ic-cta-bg-img" class="flex-1" name="ic-cta-bg-img" type="text" value="<?= $ic_cta_bg_img; ?>" />
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_ic_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/investors-contact.php') {
    add_meta_box('ic-meta-box', 'Investors Contact Template Options', '_ws_ic_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_ic_meta');

// Save meta values
function _ws_save_ic_meta($post_id) {
  if (!isset($_POST['ic-nonce']) || !wp_verify_nonce($_POST['ic-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $ic_quote = isset($_POST['ic-quote']) ? $_POST['ic-quote'] : '';
  update_post_meta($post_id, '_ic-quote', $ic_quote);

  $ic_cta_heading = isset($_POST['ic-cta-heading']) ? $_POST['ic-cta-heading'] : '';
  update_post_meta($post_id, '_ic-cta-heading', $ic_cta_heading);

  $ic_cta_text = isset($_POST['ic-cta-text']) ? $_POST['ic-cta-text'] : '';
  update_post_meta($post_id, '_ic-cta-text', $ic_cta_text);

  $ic_cta_form = isset($_POST['ic-cta-form']) ? $_POST['ic-cta-form'] : '';
  update_post_meta($post_id, '_ic-cta-form', $ic_cta_form);

  $ic_cta_bg_img = isset($_POST['ic-cta-bg-img']) ? $_POST['ic-cta-bg-img'] : '';
  update_post_meta($post_id, '_ic-cta-bg-img', $ic_cta_bg_img);
}
add_action('save_post', '_ws_save_ic_meta');
