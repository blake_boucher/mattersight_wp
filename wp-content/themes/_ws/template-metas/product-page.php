<?php
// Product page meta fields
function _ws_product_meta_fields() {
  wp_nonce_field(basename(__FILE__), 'product-nonce');
  $product_kpis = get_post_meta(get_the_ID(), '_product-kpis', true);
  $product_kpi_btn_text = get_post_meta(get_the_ID(), '_product-kpi-btn-text', true);
  $product_kpi_btn_link = get_post_meta(get_the_ID(), '_product-kpi-btn-link', true);
  $product_suites_heading = get_post_meta(get_the_ID(), '_product-suites-heading', true);
  $product_suites = get_post_meta(get_the_ID(), '_product-suites', true) ?: array();
  $product_privacy_policy = get_post_meta(get_the_ID(), '_product-privacy-policy', true);
  $product_cta_heading = get_post_meta(get_the_ID(), '_product-cta-heading', true);
  $product_cta_text = get_post_meta(get_the_ID(), '_product-cta-text', true);
  $product_cta_btn_text = get_post_meta(get_the_ID(), '_product-cta-btn-text', true);
  $product_cta_btn_link = get_post_meta(get_the_ID(), '_product-cta-btn-link', true);
  $product_cta_bg_img = get_post_meta(get_the_ID(), '_product-cta-bg-img', true);
  $product_related_heading = get_post_meta(get_the_ID(), '_product-related-heading', true);
  $product_related_type = get_post_meta(get_the_ID(), '_product-related-type', true);
  $product_related_post_one = get_post_meta(get_the_ID(), '_product-related-post-one', true);
  $product_related_post_two = get_post_meta(get_the_ID(), '_product-related-post-two', true);
  $product_related_post_three = get_post_meta(get_the_ID(), '_product-related-post-three', true);
  $product_related_post_type = get_post_meta(get_the_ID(), '_product-related-post-type', true);
  $product_related_tax = get_post_meta(get_the_ID(), '_product-related-tax', true); ?>
  <div id="product-meta-inside" class="custom-meta-inside">
    <ul>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 1</legend>
            <ul>
              <li>
                <label for="product-kpis-1-svg">SVG</label>
                <input id="product-kpis-1-svg" name="product-kpis[0][svg]" class="svg-selector" type="text" value="<?= isset($product_kpis[0]['svg']) ? $product_kpis[0]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="product-kpis-1-title">Title</label>
                <input id="product-kpis-1-title" name="product-kpis[0][title]" type="text" value="<?= isset($product_kpis[0]['title']) ? $product_kpis[0]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="product-kpis-1-text">Text</label>
                <textarea id="product-kpis-1-text" name="product-kpis[0][text]"><?= isset($product_kpis[0]['text']) ? $product_kpis[0]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 2</legend>
            <ul>
              <li>
                <label for="product-kpis-2-svg">SVG</label>
                <input id="product-kpis-2-svg" name="product-kpis[1][svg]" class="svg-selector" type="text" value="<?= isset($product_kpis[1]['svg']) ? $product_kpis[1]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="product-kpis-2-title">Title</label>
                <input id="product-kpis-2-title" name="product-kpis[1][title]" type="text" value="<?= isset($product_kpis[1]['title']) ? $product_kpis[1]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="product-kpis-2-text">Text</label>
                <textarea id="product-kpis-2-text" name="product-kpis[1][text]"><?= isset($product_kpis[1]['text']) ? $product_kpis[1]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>KPI 3</legend>
            <ul>
              <li>
                <label for="product-kpis-3-svg">SVG</label>
                <input id="product-kpis-3-svg" name="product-kpis[2][svg]" class="svg-selector" type="text" value="<?= isset($product_kpis[2]['svg']) ? $product_kpis[2]['svg'] : ''; ?>" />
              </li>
              <li>
                <label for="product-kpis-3-title">Title</label>
                <input id="product-kpis-3-title" name="product-kpis[2][title]" type="text" value="<?= isset($product_kpis[2]['title']) ? $product_kpis[2]['title'] : ''; ?>" />
              </li>
              <li>
                <label for="product-kpis-3-text">Text</label>
                <textarea id="product-kpis-3-text" name="product-kpis[2][text]"><?= isset($product_kpis[2]['text']) ? $product_kpis[2]['text'] : ''; ?></textarea>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-sm-6">
          <label for="product-kpi-btn-text">KPI Button Text</label>
          <input id="product-kpi-btn-text" name="product-kpi-btn-text" type="text" value="<?= $product_kpi_btn_text; ?>" />
        </div>
        <div class="col-sm-6">
          <label for="product-kpi-btn-link">KPI Button URL</label>
          <input id="product-kpi-btn-link" name="product-kpi-btn-link" type="text" value="<?= $product_kpi_btn_link; ?>" />
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>Product Suites</legend>
            <ul>
              <li>
                <label for="product-suites-heading">Heading</label>
                <input id="product-suites-heading" name="product-suites-heading" type="text" value="<?= $product_suites_heading; ?>" />
              </li>
            </ul>
            <ul class="sortable-container">
              <?php
              foreach($product_suites as $i=>$product_suite) : ?>
                <li class="sortable-item">
                  <div class="sortable-header">
                    <span class="dashicons dashicons-move sortable-handle"></span>
                    <span class="dashicons dashicons-trash sortable-delete"></span>
                  </div>
                  <ul class="sortable-content">
                    <li class="row">
                      <div class="col-xs-6">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-heading" name="product-suites[<?= $i; ?>][heading]" type="text" value="<?= $product_suite['heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-text" name="product-suites[<?= $i; ?>][text]"><?= $product_suite['text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                      <div class="col-xs-6">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-img">Image</label>
                            <div class="row">
                              <button class="button media-selector" target="#product-suites-<?= $i; ?>-img" size="large">Select Image</button>
                              <input id="product-suites-<?= $i; ?>-img" class="flex-1" name="product-suites[<?= $i; ?>][img]" type="text" value="<?= $product_suite['img']; ?>" />
                            </div>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="row">
                      <div class="col-xs-4">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-one-svg">SVG</label>
                            <input id="product-suites-<?= $i; ?>-kpi-one-svg" class="svg-selector" name="product-suites[<?= $i; ?>][kpi-one-svg]" type="text" value="<?= $product_suite['kpi-one-svg']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-one-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-kpi-one-heading" name="product-suites[<?= $i; ?>][kpi-one-heading]" type="text" value="<?= $product_suite['kpi-one-heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-one-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-kpi-one-text" name="product-suites[<?= $i; ?>][kpi-one-text]"><?= $product_suite['kpi-one-text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                      <div class="col-xs-4">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-two-svg">SVG</label>
                            <input id="product-suites-<?= $i; ?>-kpi-two-svg" class="svg-selector" name="product-suites[<?= $i; ?>][kpi-two-svg]" value="<?= $product_suite['kpi-two-svg']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-two-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-kpi-two-heading" name="product-suites[<?= $i; ?>][kpi-two-heading]" type="text" value="<?= $product_suite['kpi-two-heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-two-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-kpi-two-text" name="product-suites[<?= $i; ?>][kpi-two-text]"><?= $product_suite['kpi-two-text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                      <div class="col-xs-4">
                        <ul>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-three-svg">SVG</label>
                            <input id="product-suites-<?= $i; ?>-kpi-three-svg" class="svg-selector" name="product-suites[<?= $i; ?>][kpi-three-svg]" value="<?= $product_suite['kpi-three-svg']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-three-heading">Heading</label>
                            <input id="product-suites-<?= $i; ?>-kpi-three-heading" name="product-suites[<?= $i; ?>][kpi-three-heading]" type="text" value="<?= $product_suite['kpi-three-heading']; ?>" />
                          </li>
                          <li>
                            <label for="product-suites-<?= $i; ?>-kpi-three-text">Text</label>
                            <textarea id="product-suites-<?= $i; ?>-kpi-three-text" name="product-suites[<?= $i; ?>][kpi-three-text]"><?= $product_suite['kpi-three-text']; ?></textarea>
                          </li>
                        </ul>
                      </div>
                    </li>
                  </ul>
                </li>
              <?php
              endforeach; ?>
            </ul>
            <button id="add-product-suite" class="button">Add Product Suite</button>
            <ul>
              <li>
                <label for="product-privacy-policy">Privacy Policy URL</label>
                <input id="product-privacy-policy" name="product-privacy-policy" type="text" value="<?= $product_privacy_policy; ?>" />
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row">
        <div class="col-xs-12">
          <fieldset>
            <legend>CTA</legend>
            <ul>
              <li>
                <label for="product-cta-heading">Heading</label>
                <input id="product-cta-heading" name="product-cta-heading" type="text" value="<?= $product_cta_heading; ?>">
              </li>
              <li>
                <label for="product-cta-text">Text</label>
                <textarea id="product-cta-text" name="product-cta-text" class="text-editor"><?= $product_cta_text; ?></textarea>
              </li>
              <li class="row">
                <div class="col-xs-6">
                  <label for="product-cta-btn-text">Button Text</label>
                  <input id="product-cta-btn-text" name="product-cta-btn-text" type="text" value="<?= $product_cta_btn_text; ?>">
                </div>
                <div class="col-xs-6">
                  <label for="product-cta-btn-link">Button Link</label>
                  <input id="product-cta-btn-link" name="product-cta-btn-link" type="text" value="<?= $product_cta_btn_link; ?>">
                </div>
              </li>
              <li>
                <label for="product-cta-bg-img">Background Image</label>
                <div class="row">
                  <button class="button media-selector" target="#product-cta-bg-img">Select Image</button>
                  <input id="product-cta-bg-img" name="product-cta-bg-img" class="flex-1" type="text" value="<?= $product_cta_bg_img; ?>">
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
      <li class="row pb-related">
        <div class="col-xs-12">
          <fieldset>
            <legend>Related Content</legend>
            <ul>
              <li>
                <label for="product-related-heading">Heading</label>
                <input id="product-related-heading" name="product-related-heading" type="text" value="<?= $product_related_heading; ?>" >
              </li>
              <li>
                <input id="product-related-type-posts" name="product-related-type" type="radio" value="posts" <?= $product_related_type=='posts' ? 'checked' : ''; ?> />
                <label for="product-related-type-posts">Specific Posts</label>
                <div class="row">
                  <div class="col-xs-4">
                    <input id="product-related-post-one" name="product-related-post-one" class="post-selector" type="text" value="<?= $product_related_post_one; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="product-related-post-two" name="product-related-post-two" class="post-selector" type="text" value="<?= $product_related_post_two; ?>" />
                  </div>
                  <div class="col-xs-4">
                    <input id="product-related-post-three" name="product-related-post-three" class="post-selector" type="text" value="<?= $product_related_post_three; ?>" />
                  </div>
                </div>
              </li>
              <li>
                <input id="product-related-type-post-type" name="product-related-type" type="radio" value="post-type" <?= $product_related_type=='post-type' ? 'checked' : ''; ?> />
                <label for="product-related-type-post-type">Post Type</label>
                <div>
                  <select id="product-related-post-type" name="product-related-post-type">
                    <?php
                    $types = get_post_types(array('public'=>true));
                    foreach ($types as $type) {
                      $t = get_post_type_object($type);
                      echo '<option value="' . $t->name . '" ' . ($product_related_post_type==$t->name ? 'selected' : '') . '>' . $t->labels->name . '</option>';
                    } ?>
                  </select>
                </div>
              </li>
              <li>
                <input id="product-related-type-tax" name="product-related-type" type="radio" value="tax" <?= $product_related_type=='tax' ? 'checked' : ''; ?> />
                <label for="product-related-type-tax">Taxonomy</label>
                <div>
                  <select id="product-related-tax" name="product-related-tax">
                    <?php
                    $terms = get_terms('related_content');
                    foreach ($terms as $term) {
                      echo '<option value="' . $term->slug . '"' . ($product_related_tax==$term->slug ? 'selected' : '') . '>' . $term->name . ' (' . $term->count . ')</option>';
                    } ?>
                  </select>
                </div>
              </li>
            </ul>
          </fieldset>
        </div>
      </li>
    </ul>
  </div>
  <?php
}

// Create meta box
function _ws_product_meta() {
  global $post;
  if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/product-page.php') {
    add_meta_box('po-meta-box', 'Product Page Template Options', '_ws_product_meta_fields', 'page', 'normal', 'high');
  }
}
add_action('add_meta_boxes', '_ws_product_meta');

// Save meta values
function _ws_save_product_meta($post_id) {
  if (!isset($_POST['product-nonce']) || !wp_verify_nonce($_POST['product-nonce'], basename(__FILE__))) {
    return $post_id;
  }
  if (!current_user_can('edit_post', $post_id)) {
    return $post_id;
  }
  if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
    return $post_id;
  }

  $product_kpis = isset($_POST['product-kpis']) ? $_POST['product-kpis'] : '';
  update_post_meta($post_id, '_product-kpis', $product_kpis);

  $product_kpi_btn_text = isset($_POST['product-kpi-btn-text']) ? $_POST['product-kpi-btn-text'] : '';
  update_post_meta($post_id, '_product-kpi-btn-text', $product_kpi_btn_text);

  $product_kpi_btn_link = isset($_POST['product-kpi-btn-link']) ? $_POST['product-kpi-btn-link'] : '';
  update_post_meta($post_id, '_product-kpi-btn-link', $product_kpi_btn_link);

  $product_suites_heading = isset($_POST['product-suites-heading']) ? $_POST['product-suites-heading'] : '';
  update_post_meta($post_id, '_product-suites-heading', $product_suites_heading);

  $product_suites = isset($_POST['product-suites']) ? $_POST['product-suites'] : '';
  update_post_meta($post_id, '_product-suites', $product_suites);

  $product_privacy_policy = isset($_POST['product-privacy-policy']) ? $_POST['product-privacy-policy'] : '';
  update_post_meta($post_id, '_product-privacy-policy', $product_privacy_policy);

  $product_cta_heading = isset($_POST['product-cta-heading']) ? $_POST['product-cta-heading'] : '';
  update_post_meta($post_id, '_product-cta-heading', $product_cta_heading);

  $product_cta_text = isset($_POST['product-cta-text']) ? $_POST['product-cta-text'] : '';
  update_post_meta($post_id, '_product-cta-text', $product_cta_text);

  $product_cta_btn_text = isset($_POST['product-cta-btn-text']) ? $_POST['product-cta-btn-text'] : '';
  update_post_meta($post_id, '_product-cta-btn-text', $product_cta_btn_text);

  $product_cta_btn_link = isset($_POST['product-cta-btn-link']) ? $_POST['product-cta-btn-link'] : '';
  update_post_meta($post_id, '_product-cta-btn-link', $product_cta_btn_link);

  $product_cta_bg_img = isset($_POST['product-cta-bg-img']) ? $_POST['product-cta-bg-img'] : '';
  update_post_meta($post_id, '_product-cta-bg-img', $product_cta_bg_img);

  $product_related_heading = isset($_POST['product-related-heading']) ? $_POST['product-related-heading'] : '';
  update_post_meta($post_id, '_product-related-heading', $product_related_heading);

  $product_related_type = isset($_POST['product-related-type']) ? $_POST['product-related-type'] : '';
  update_post_meta($post_id, '_product-related-type', $product_related_type);

  $product_related_post_one = isset($_POST['product-related-post-one']) ? $_POST['product-related-post-one'] : '';
  update_post_meta($post_id, '_product-related-post-one', $product_related_post_one);

  $product_related_post_two = isset($_POST['product-related-post-two']) ? $_POST['product-related-post-two'] : '';
  update_post_meta($post_id, '_product-related-post-two', $product_related_post_two);

  $product_related_post_three = isset($_POST['product-related-post-three']) ? $_POST['product-related-post-three'] : '';
  update_post_meta($post_id, '_product-related-post-three', $product_related_post_three);

  $product_related_post_type = isset($_POST['product-related-post-type']) ? $_POST['product-related-post-type'] : '';
  update_post_meta($post_id, '_product-related-post-type', $product_related_post_type);

  $product_related_tax = isset($_POST['product-related-tax']) ? $_POST['product-related-tax'] : '';
  update_post_meta($post_id, '_product-related-tax', $product_related_tax);
}
add_action('save_post', '_ws_save_product_meta');
