<!DOCTYPE html>
<html <?= get_query_var('amp') ? '⚡ ' : ''; ?><?php language_attributes(); ?>>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
  <link rel="manifest" href="/manifest.json">
  <meta name="theme-color" content="#D64123">
  <!-- <meta http-equiv="Content-Security-Policy" content="script-src 'unsafe-inline' 'self' https://apis.google.com"> -->
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <link rel="preload" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" as="style" />
  <link rel="preload" href="/wp-content/themes/_ws/dist/css/wp/wp.min.css?ver=1.0" as="style" />
  <link rel="preload" href="/wp-content/themes/_ws/dist/js/wp/wp.min.js?ver=1.0" as="script" />

  <?php
  wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <?php
  if ($tag_manager_id = get_option('tag_manager_id')) : ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?= $tag_manager_id; ?>"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
  <?php
  endif; ?>
  <a class="screen-reader-text" href="#main">Skip to content</a>
  <header id="site-header" class="fixed">
    <div class="container-fluid row">
      <div class="logo-title">
        <a href="<?= esc_url(home_url('/')); ?>" rel="home">
          <svg viewBox="0 0 380 68.4" rel="img">
            <title>Mattersight</title>
            <path d="M85.4,18.9c-1.9-3.3-6.3-5.4-10.8-5.4-9.8,0-17.4,6.1-17.4,17.6s7.2,17.6,17.2,17.6c3.8,0,8.7-1.8,11-5.7l.4,4.9h7.8V14.3h-8ZM75.5,41.1c-5.5,0-9.9-3.6-9.9-10S70,21,75.5,21c6.5,0,9.8,5.1,9.8,10.1S82,41.1,75.5,41.1Z"/>
            <path d="M142.1,41.2c-2.7,0-4.4-1.6-4.4-4.8v-15H147V14.2h-9.3V4.7l-8.3.9v8.6H113V4.7l-8.3.9v8.6H98.4v7.2h6.3v15c0,8.4,4.7,12.5,12.1,12.3a18.49,18.49,0,0,0,6.9-1.4l-2.3-7.1a9.84,9.84,0,0,1-4,1c-2.7,0-4.4-1.6-4.4-4.8v-15h16.4v15c0,8.4,4.7,12.5,12.1,12.3a18.49,18.49,0,0,0,6.9-1.4l-2.3-7.1A9.84,9.84,0,0,1,142.1,41.2Z"/>
            <path d="M169.8,13.5C158.9,13.5,152,20.8,152,31c0,10.7,6.9,17.7,18.5,17.7,5.2,0,10.7-1.8,14.3-5.4L180,37.9c-2,2-6.3,3.2-9.3,3.2-5.9,0-9.5-2.8-10-6.9h26.2c.1-1.1.1-2.1.1-3.1C186.9,19.3,180.1,13.5,169.8,13.5Zm-9.1,13.8c1.3-4.4,4.8-6.5,9.4-6.5,4.8,0,8.2,2.1,8.7,6.5Z"/>
            <path d="M200.9,18.2l-.6-3.9h-7.7V47.9h8.3V30.2c0-6.3,4-8.5,8.4-8.5a9,9,0,0,1,6,2.2l3.7-7.2a13.41,13.41,0,0,0-8.7-3.1C207,13.5,203.3,14.1,200.9,18.2Z"/>
            <path d="M237.1,27.2c-4.2-.3-6.3-1.5-6.3-3.7s2.1-3.3,6.1-3.3a12.46,12.46,0,0,1,8.3,3l4.3-5.1a18.35,18.35,0,0,0-12.7-4.7c-6.1,0-14.2,2.5-14.2,10.4,0,7.6,7.6,9.9,13.9,10.4,4.8.3,6.7,1.6,6.7,3.8,0,2.5-2.9,3.5-6.1,3.4a18.77,18.77,0,0,1-11.6-4.7l-4.1,5.9c5.2,4.9,10.1,6,15.5,6,9.5,0,14.4-4.8,14.4-10.8C251.5,29.1,243.4,27.6,237.1,27.2Z"/>
            <ellipse cx="261.8" cy="4.8" rx="4.9" ry="4.8"/>
            <rect x="257.6" y="14.3" width="8.3" height="33.6"/>
            <path d="M303.6,10.9l-3.7,4.6c-2.3-1.6-6.1-2-8.7-2-9.4,0-18.4,5.7-18.4,17,0,10.6,7.3,16.9,18.4,16.9,6.3,0,9.5,2.5,9.5,6.6s-5,6.3-9.7,6.3c-5.2,0-9.9-2.5-9.9-7.4h-8.2c-.3,9.6,6.5,15.5,18.1,15.5,9,0,18.3-4.8,18.3-14.4,0-3.3-1.2-8.4-8.3-10.9,5.5-2.5,8.3-6.6,8.3-12.6,0-3.5-1-6.5-3.9-10l4.2-4.9ZM291.3,39.7c-5.4,0-10.1-3.4-10.1-9.3,0-6.3,4.6-9.3,10.1-9.3s9.9,3.5,9.9,9.3S296.7,39.7,291.3,39.7Z"/>
            <path d="M336.4,13.5a12.61,12.61,0,0,0-10.5,5V.2h-8.3V47.9h8.3V30.4c0-5.1,3.4-9.2,8.3-9.2,4.4,0,7.6,2.5,7.6,8.7v18h8.3v-18C350.1,19.9,345.9,13.5,336.4,13.5Z"/>
            <path d="M377.7,40.2a9.84,9.84,0,0,1-4,1c-2.7,0-4.4-1.6-4.4-4.8v-15h9.3V14.2h-9.3V4.7l-8.3.9v8.6h-6.3v7.2H361v15c0,8.4,4.7,12.5,12.1,12.3a18.49,18.49,0,0,0,6.9-1.4Z"/>
            <path d="M37.2,13.5a13.46,13.46,0,0,0-11.7,6.2,13.57,13.57,0,0,0-11.7-6.2C5.1,13.5,0,18.6,0,29.3V47.9H8.3V29.3c0-5,1.8-8,6.3-8s7.3,3.9,7.3,8.3V47.9H29V29.6c0-4.4,2.8-8.3,7.3-8.3s6.3,3,6.3,8V47.9h8.3V29.3C51,18.6,45.9,13.5,37.2,13.5Z"/>
          </svg>
        </a>
      </div>
      <input id="hamburger" type="checkbox" />
      <label for="hamburger" class="hamburger">
        <svg viewBox="0 0 100 100" rel="img">
          <title>Menu</title>
          <path class="closed" d="M10 22 L90 22 M10 50 L90 50 M10 78 L90 78" />
          <path class="opened" d="M20 20 L80 80 Z M80 20 L20 80 Z" />
        </svg>
      </label>
      <div class="menu-container">
        <nav>
          <?php
          class WPDocs_Walker_Nav_Menu extends Walker_Nav_Menu {
            public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
              if (isset($args->item_spacing) && 'discard' === $args->item_spacing) {
                $t = '';
                $n = '';
              } else {
                $t = "\t";
                $n = "\n";
              }
              $indent = ($depth) ? str_repeat($t, $depth) : '';
              $classes = empty($item->classes) ? array() : (array) $item->classes;
              $classes[] = 'menu-item-' . $item->ID;
              $args = apply_filters('nav_menu_item_args', $args, $item, $depth);
              $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
              $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';
              $id = apply_filters('nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args, $depth);
              $id = $id ? ' id="' . esc_attr($id) . '"' : '';
              $output .= $indent . '<li' . $id . $class_names .'>';
              $atts = array();
              $atts['title']  = ! empty($item->attr_title) ? $item->attr_title : '';
              $atts['target'] = ! empty($item->target)     ? $item->target     : '';
              $atts['rel']    = ! empty($item->xfn)        ? $item->xfn        : '';
              $atts['href']   = ! empty($item->url)        ? $item->url        : '';
              $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);
              $attributes = '';
              foreach ($atts as $attr => $value) {
                if (!empty($value)) {
                  $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                  $attributes .= ' ' . $attr . '="' . $value . '"';
                }
              }
              $title = apply_filters('the_title', $item->title, $item->ID);
              $title = apply_filters('nav_menu_item_title', $title, $item, $args, $depth);
              $item_output = $args->before;
              $item_output .= '<a'. $attributes .'>';
              $item_output .= $args->link_before . $title . $args->link_after;
              $item_output .= '</a>';
              $item_output .= do_shortcode('<input id="' . $item->object_id . '" type="checkbox" /><label class="dropdown" for="' . $item->object_id . '"><span class="screen-reader-text">Collapse/Expand</span>[svg id="arrow-down"]</label>');
              $item_output .= $args->after;
              $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
            }
          }
          wp_nav_menu(array(
            'theme_location' => 'header',
            'container' => 'div',
            'container_class' => 'header-menu header-left',
            'walker' => new WPDocs_Walker_Nav_Menu(), // Comment this line to remove mobile dropdowns
            'fallback_cb' => false,
            'item_spacing' => 'discard'
          ));
          wp_nav_menu(array(
            'theme_location' => 'header-right',
            'container' => 'div',
            'container_class' => 'header-menu header-right',
            'walker' => new WPDocs_Walker_Nav_Menu(), // Comment this line to remove mobile dropdowns
            'fallback_cb' => false,
            'item_spacing' => 'discard'
          )); ?>
          <div class="search-container">
            <button class="search-btn" aria-label="Search">
              <?= do_shortcode('[svg id="search"]'); ?>
            </button>
            <form class="search-form" action="/" method="get" role="search">
              <label class="screen-reader-text" for="s">Search</label>
              <input id="s" name="s" type="text" placeholder="search" />
            </form>
          </div>
        </nav>
      </div>
    </div>
  </header>
