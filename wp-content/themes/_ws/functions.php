<?php
date_default_timezone_set('America/New_York');
@ini_set('upload_max_filesize', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '60');

function _ws_setup() {
  // Make theme available for translation.
  load_theme_textdomain('_ws', get_template_directory() . '/languages');

  add_theme_support('custom-logo');
  add_theme_support('post-thumbnails');
  add_image_size('standard', 576, 576);
  add_theme_support('html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption'
  ));

  // Add excerpt support for pages
  add_post_type_support('page', 'excerpt');

  // Register nav locations
  register_nav_menus(array(
    'header' => esc_html__('Header', '_ws'),
    'header-right' => esc_html__('Header Right', '_ws'),
    'investors' => esc_html__('Investors', '_ws'),
    // 'footer' => esc_html__('Footer', '_ws')
  ));

  // Remove junk from head
  remove_action('wp_head', 'wp_generator'); // Wordpress version
  remove_action('wp_head', 'rsd_link'); // Really Simple Discovery
  remove_action('wp_head', 'wlwmanifest_link'); // Windows Live Writer
  remove_action('wp_head', 'print_emoji_detection_script', 7); // Emojis :(
  remove_action('wp_print_styles', 'print_emoji_styles'); // Emojis :(
  remove_action('wp_head', 'wp_shortlink_wp_head'); // Page shortlink
  remove_action('wp_head', 'start_post_rel_link'); // Navigation links
  remove_action('wp_head', 'parent_post_rel_link'); // Navigation links
  remove_action('wp_head', 'index_rel_link'); // Navigation links
  remove_action('wp_head', 'adjacent_posts_rel_link'); // Navigation links
  remove_action('wp_head', 'rest_output_link_wp_head'); // JSON
  remove_action('wp_head', 'wp_oembed_add_discovery_links'); // JSON
  remove_action('wp_head', 'rel_canonical'); // If there's more than one canonical, neither will work, so we remove the default one and use ours

  // Enable shortcodes in widgets
  add_filter('widget_text', 'do_shortcode');
}
add_action('after_setup_theme', '_ws_setup');

// Screen options defaults
function _ws_screen_options_defaults($hidden, $screen) {
  $hidden = array('revisionsdiv', 'postcustom', 'commentstatusdiv', 'commentsdiv', 'slugdiv');
  return $hidden;
}
add_filter('default_hidden_meta_boxes', '_ws_screen_options_defaults', 10, 2);

// Add custom image size
function _ws_custom_sizes($sizes) {
  return array_merge($sizes, array(
    'standard' => 'Standard'
  ));
}
add_filter('image_size_names_choose', '_ws_custom_sizes');

// Remove empty p tags
function _ws_remove_empty_p($content) {
  $content = force_balance_tags( $content );
  $content = preg_replace('/<p><\/p>/', '', $content); //empty
  $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content); //images
  return $content;
}
add_filter('the_content', '_ws_remove_empty_p', 20, 1);
remove_filter('the_content', 'wpautop');
add_filter('the_content', 'wpautop' , 12);

// Remove trackback/pingback support and tags from posts
function _ws_remove_post_support() {
  remove_post_type_support('post', 'trackbacks');
  unregister_taxonomy_for_object_type('post_tag', 'post');
}
add_action('init', '_ws_remove_post_support');

// Disable author archives
function _ws_disable_author_archives() {
  if (is_author()) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
  } else {
    redirect_canonical();
  }
}
remove_filter('template_redirect', 'redirect_canonical');
add_action('template_redirect', '_ws_disable_author_archives');

// Remove h1 and lower headlines from editor
function _ws_tiny_mce_formats($init) {
  $init['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre';
  return $init;
}
add_filter('tiny_mce_before_init', '_ws_tiny_mce_formats');

// Register scripts/styles
function _ws_register_scripts() {
  wp_register_style('font-css', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,700', array(), null);
  wp_register_style('wp-css', '/wp-content/themes/_ws/dist/css/wp/wp.min.css', array(), '1.0');
  wp_register_style('admin-css', get_template_directory_uri() . '/dist/css/admin/admin.min.css', array(), '1.0');
  wp_register_style('jquery-ui-css', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');

  wp_register_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', array('jquery'), false, true);
  wp_register_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=' . (get_option('google_maps') ?: 'AIzaSyB5dOtdhz53nCEusX4aU4yRKkOGns_Dsn8') . '&libraries=places&callback=initMap', array(), false, true);
  wp_register_script('google-charts', 'https://www.gstatic.com/charts/loader.js', array(), false, false);
  wp_register_script('amp', 'https://cdn.ampproject.org/v0.js', array(), null, false);
  wp_register_script('wp-js', '/wp-content/themes/_ws/dist/js/wp/wp.min.js', array(), '1.0', true);
  wp_register_script('calc-js', '/wp-content/themes/_ws/dist/js/wp/calc.min.js', array('google-charts'), '1.0', true);
  wp_register_script('admin-js', get_template_directory_uri() . '/dist/js/admin/admin.min.js', array('jquery', 'jquery-ui', 'wp-color-picker'), '1.0', true);
  wp_register_script('pagebuilder-js', get_template_directory_uri() . '/dist/js/admin/pagebuilder.min.js', array('jquery', 'jquery-ui', 'admin-js'), '1.0', true);
  wp_register_script('event-js', get_template_directory_uri() . '/dist/js/admin/event.min.js', array('jquery', 'jquery-ui'), '1.0', true);
  wp_register_script('options-js', get_template_directory_uri() . '/dist/js/admin/options.min.js', array('jquery'), '1.0', true);
}
add_action('wp_loaded', '_ws_register_scripts');

// Preload certain assets
function _ws_preload_assets($tag) {
  preg_match('/wp-css-css/', $tag, $matches);
  if ($matches) {
    $tag = preg_replace('/rel=\'stylesheet\'/', 'rel=\'preload\' as=\'style\'', $tag);
  }
  return $tag;
}
// add_filter('style_loader_tag', '_ws_preload_assets');

// Make frontend javascript defer
function _ws_script_attribute($tag, $handle) {
  if ($handle == 'wp-js') {
    return str_replace(' src', ' defer src', $tag);
  }
  if ($handle == 'amp' || $handle == 'amp-analytics') {
    return str_replace(' src', ' async src', $tag);
  }
  return $tag;
}
add_filter('script_loader_tag', '_ws_script_attribute', 10, 2);

// Enqueue frontend scripts/styles
function _ws_wp_enqueue() {
  wp_deregister_script('wp-embed'); // Remove embed script

  global $post;
  global $post_type;
  global $wp_query;
  wp_enqueue_style('font-css');
  wp_enqueue_style('wp-css');
  wp_localize_script('wp-js', 'locals', array(
    'ajax_url' => admin_url('admin-ajax.php'),
    'wp_query' => $wp_query
  ));
  if (get_query_var('amp')) {
    wp_enqueue_script('amp');
  }
  else {
    wp_enqueue_script('wp-js');
    if ($post && get_post_meta($post->ID, '_wp_page_template', true) == 'templates/calculator.php') {
      wp_enqueue_script('calc-js');
    }
  }
}
add_action('wp_enqueue_scripts', '_ws_wp_enqueue');

// Enqueue admin scripts/styles
function _ws_admin_enqueue($hook) {
  global $post_type;
  wp_enqueue_style('admin-css');
  wp_enqueue_style('jquery-ui-css');
  wp_enqueue_style('wp-color-picker');

  wp_enqueue_media();
  wp_enqueue_editor();
  wp_localize_script('admin-js', 'locals', array(
    'ajax_url' => admin_url('admin-ajax.php')
  ));
  wp_enqueue_script('admin-js');
  // Pages that need google maps api
  if ($post_type == 'event') {
    if ($hook == 'post-new.php' || $hook == 'post.php') {
      wp_enqueue_script('event-js');
    }
  }
  else if ($hook == 'settings_page_site') {
    wp_enqueue_script('options-js');
    wp_enqueue_script('google-maps', '', array('options-js'));
  }
  else if ($hook == 'settings_page_sitemap' || $hook == 'settings_page_bulk' || $hook == 'settings_page_redirects') {
    wp_enqueue_script('options-js');
  }
  // Pagebuilder script
  if ($post_type == 'page' && ($hook == 'post.php' || $hook == 'post-new.php')) {
    global $post;
    if (get_post_meta($post->ID, '_wp_page_template', true) == 'templates/pagebuilder.php') {
      add_filter('wp_default_editor', function() {
        return 'html';
      });
      wp_localize_script('pagebuilder-js', 'pbInfo', array(
        'ajax_url' => admin_url('admin-ajax.php')
      ));
      wp_enqueue_script('pagebuilder-js');
    }
  }
}
add_action('admin_enqueue_scripts', '_ws_admin_enqueue');

// Register widget areas
function _ws_register_widget_areas() {
  register_sidebar(array(
    'name' => 'Sidebar',
    'id' => 'sidebar',
    'description' => 'Widgets in this area will appear on pages with a left or right sidebar'
  ));
  register_sidebar(array(
    'name' => 'Footer',
    'id' => 'footer',
    'description' => 'Widgets in this area will appear in the footer'
  ));
}
add_action('widgets_init', '_ws_register_widget_areas');

// Set excerpt length
function _ws_excerpt_length($length) {
  return 35;
}
add_filter('excerpt_length', '_ws_excerpt_length', 999);

// Custom post types
foreach (glob(get_template_directory() . '/post-types/*.php') as $filename) {
  require_once $filename;
}

// Template fields
foreach (glob(get_template_directory() . '/template-metas/*.php') as $filename) {
  require_once $filename;
}

// Custom taxonomies
foreach (glob(get_template_directory() . '/taxonomies/*.php') as $filename) {
  require_once $filename;
}

// Miscellaneous files
foreach (glob(get_template_directory() . '/inc/*.php') as $filename) {
  require_once $filename;
}
